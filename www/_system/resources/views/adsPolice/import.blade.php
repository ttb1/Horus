<HTML lang="en">
    <HEAD>
        <TITLE>Branding Topica</TITLE>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <STYLE>
        </STYLE>
    </HEAD>
    <BODY>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 >Ads Police</h2>  
                </div>
            </div>
            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <strong>ERROR : </strong> {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif
                
            </div>
            <div class="row">
                <form action="{{ route('ads_police_import') }}" method="POST" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Import File</strong></div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group input-file" name="import">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary btn-choose" type="button">Choose</button>
                                            </span>
                                            <input type="text" class="form-control" placeholder='Choose a file...' />
                                            <span class="input-group-btn">
                                                     <button class="btn btn-warning btn-reset" type="button">Reset</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary"><strong>Import</strong></button> 
                            </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token()}}" >
                
                </form>
            </div>
            
            <script>
                function bs_input_file() {
                    $(".input-file").before(
                        function() {
                            if ( ! $(this).prev().hasClass('input-ghost') ) {
                                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                                element.attr("name",$(this).attr("name"));
                                element.change(function(){
                                        element.next(element).find('input').val((element.val()).split('\\').pop());
                                });
                                $(this).find("button.btn-choose").click(function(){
                                        element.click();
                                });
                                $(this).find("button.btn-reset").click(function(){
                                        element.val(null);
                                        $(this).parents(".input-file").find('input').val('');
                                });
                                $(this).find('input').css("cursor","pointer");
                                $(this).find('input').mousedown(function() {
                                        $(this).parents('.input-file').prev().click();
                                        return false;
                                });
                                return element;
                            }
                        });
                    }
                $(function() {
                    bs_input_file();
                });
            </script>
        </div>
    </BODY>
</HTML>