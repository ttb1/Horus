<HTML lang="en">
    <HEAD>
        <TITLE>Branding Topica</TITLE>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <STYLE>
        </STYLE>
    </HEAD>
    <BODY>
        <div class="container">
            <div class="row">
                <h2>Ads Police</h2>  
            </div>
            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <strong>ERROR : </strong> {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif
                
            </div>
            <div class="row">
                
                <h4> Branding key : {{ $adspolice->key_branding}}</h4>
                <h4> Marketing key : {{ $adspolice->key_marketing}}</h4>
         
                
                <h2>Detail</h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Field</th>
                            <th >Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($adspolice->customfield as $customfield)
                            <tr>
                            <th scope="row">{{$customfield->id}}</th>
                            <td>{{$customfield->customfield}}</td>
                            <td>{{$customfield->value}}</td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <h2>Comment</h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Comment</th>
                            <th >Anther</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($adspolice->comment as $comment)
                            <tr>
                            <th scope="row">{{$comment->id}}</th>
                            <td>{{$comment->comment}}</td>
                            <td>{{$comment->author_email}}</td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                
                <h2>Image</h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Branding Image</th>
                            <th >Workflow Name</th>
                            <th >Download</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($adspolice->attachment as $attachment)
                            <tr>
                            <th scope="row">{{$attachment->id}}</th>
                            <td><img src="{{ url($attachment->path)  }}"  style="max-height: 200px;max-width: 200px;"></td>
                            <td>{{$attachment->workflow_name}}</td>
                            <td>
                                <a href="/ads_police/download?path={{$attachment->path}}" ><i class="fas fa-download"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                
                
                
            </div>
        </div>
    </BODY>
</HTML>