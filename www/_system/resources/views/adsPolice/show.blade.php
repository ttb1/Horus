<HTML lang="en">
    <HEAD>
        <TITLE>Branding Topica</TITLE>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="{{ asset('/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
        <script src="{{ asset('js/bootstrap.min.js')}}"></script>
        <STYLE>
        </STYLE>
    </HEAD>
    <BODY>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 >Ads Police</h2>  
                </div>
            </div>
            <div class="row">
                <form action="{{ route('ads_police_show') }}" method="GET" >
                    <div class="col-xs-3">
                        <H5>From date</H5>
                        <input type="text" class="form-control date" data-provide="datepicker" name="from_date" value="{{$old_request->from_date}}" autocomplete="off">
                    </div>
                    <div class="col-xs-3">
                        <H5>To date</H5>
                        <input type="text" class="form-control date" data-provide="datepicker" name="to_date" value="{{$old_request->to_date}}" autocomplete="off">
                    </div>
                    <div class="col-xs-2">
                        <h5 style="color: transparent;">Fil</h5> 
                        <input class="btn btn-default" type="submit" value="Filter">
                    </div>
                </form>
            </div>
            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <strong>ERROR : </strong> {{ $error }}
                            @endforeach
                        </ul>
                    </div>
                @endif
                
            </div>
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Branding Key</th>
                            <th scope="col">Marketing Key</th>
                            <th scope="col">Approve Edit</th>
                            <th scope="col">Marketing Done</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($adspolice_list as $adspolice)
                            <tr>
                            <th scope="row">{{$adspolice->id}}</th>
                            <td>{{$adspolice->key_branding}}</td>
                            <td>{{$adspolice->key_marketing}}</td>
                            <td>{{$adspolice->created_at}}</td>
                            <td>
                                @if ($adspolice->created_at != $adspolice->updated_at)
                                    {{$adspolice->updated_at}}
                                @endif
                            </td>
                            <td><a href="/ads_police/show/{{$adspolice->id}}"><i class="fas fa-link"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row">
                <nav aria-label="Page navigation example" style="text-align: center;">
                    <ul class="pagination" style="margin-top: 10px;margin-bottom: 0px">
                        <li class="page-item @if( 1 == $adspolice_list->currentPage() ) disabled @endif"><a class="page-link" href="{{  request()->fullUrlWithQuery(["page"=>$adspolice_list->currentPage()-1]) }}">Previous</a></li>
                        @for ($i = 0; $i < $adspolice_list->lastPage() ; $i++)
                            <li class="page-item @if( ($i+1) == $adspolice_list->currentPage() ) active @endif"><a class="page-link" href="{{  request()->fullUrlWithQuery(["page"=>$i+1]) }}">{{ $i+1 }}</a></li>
                        @endfor
                        <li class="page-item @if( $adspolice_list->currentPage() == $adspolice_list->lastPage() ) disabled @endif"><a class="page-link" href="{{  request()->fullUrlWithQuery(["page"=>$adspolice_list->currentPage()+1]) }}">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <script type="text/javascript" src="{{ asset('/js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
        <script type="text/javascript" src="{{ asset('/js/locales/bootstrap-datetimepicker.th.js')}}" charset="UTF-8"></script>
        <script type="text/javascript">
           $('.date').datetimepicker({
                language:  'th',
                weekStart: 1,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: 'yyyy-mm-dd'
            }); 
        </script>
                
                
    </BODY>
</HTML>