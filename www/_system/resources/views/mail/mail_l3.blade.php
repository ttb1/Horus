<!DOCTYPE html>
<html lang="en">

<head>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@php $edit = $data->email_template_list; @endphp

<body>
    <div class="container">
        <hr>
        {{-- <h2>view mail form : {{ $edit->name }}</h2> --}}
        <hr>
        <form action="{{ url('mail_l3_send') }}" method="post" class="form form-horizontal">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $edit->id }}">
            <div class="form-group">
                <label for="email_to" class="col-sm-1 control-label">To</label>
                <div class="col-sm-6">
                <input type="email" name="email_to" id="email_to" class="form-control" value="{{$data->email->email}}">
                </div>
            </div>
            <div class="form-group">
                <label for="email_from" class="col-sm-1 control-label">Form</label>
                <div class="col-sm-6">
                    <input type="email" name="email_from" id="email_from" class="form-control" value="ntl.crm.topica@gmail.com">
                </div>
            </div>

            <div class="form-group">
                <label for="mailfrom" class="col-sm-1 control-label">mailfrom</label>
                <div class="col-sm-11">
                    <textarea id="summernote" name="html_text">
                        {{ $edit->html_text }}
                    </textarea>
                </div>

            </div>

            <div class="form-group">
                <span class="col-sm-1"></span>
                <div class="col-sm-6">
                    <input type="submit" class="btn btn-grey" value="send email">
                    <a href="{{ url('mail') }}">Back to list</a>
                </div>
            </div>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            $("#summernote").summernote({
                height: 400,
                callbacks: {
                    // onImageUpload: function (files, editor, welEditable) {
                    //     sendFile(files[0], this);
                    // }
                    onImageUpload: function (image) {
                        uploadImage(image[0]);
                    }
                }
            });
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function uploadImage(image) {
            var form_data = new FormData();
            form_data.append('image', image);
            $.ajax({
                data: form_data,
                type: "POST",
                url: '/img_mail',
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);
                    // $(el).summernote('editor.insertImage', data);
                    var image = $('<img>').attr('src', data);
                    $('#summernote').summernote("insertNode", image[0]);

                }
            });
        }
    </script>
</body>

</html>