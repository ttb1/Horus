<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">


<div class="container">

    <form action="{{url('testemaileditor')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">


        <div class="form-horizontal">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-6">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                </div>
            </div>
        </div>
        <br>
        <textarea id="summernote" name="mail">

        </textarea>
        <hr>
        <input type="submit" value="submit">

    </form>

</div>

<script>
    $(document).ready(function () {
        $("#summernote").summernote({
            placeholder: 'enter directions here...',
            height: 300,
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {

                    for (var i = files.length - 1; i >= 0; i--) {
                        sendFile(files[i], this);
                    }
                }
            }
        });
    });

    function sendFile(file, el) {
        var form_data = new FormData();
        form_data.append('file', file);
        $.ajax({
            data: form_data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{url('
            img_mail ')}}',
            cache: false,
            contentType: false,
            processData: false,
            success: function (url) {
                console.log(url);
                $(el).summernote('editor.insertImage', url);
            }
        })
    }
</script>