{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <!--Header Area -->
    @include('includes.header')
</head> --}}

@extends('layout')
@section('content')
@section('content')

    <div class="container">
        <hr>
        <h2>Mail list</h2>
        <hr>
        <a href="{{'mailadd'}}">Add New</a> |
        <a href="https://copita.createsend.com/templates/customize/13-the-blueprint-3#/editor" target="_blank">createsend</a>

        <table class="table">
            <thead>
                <td>id</td>
                <td>name</td>
                <td>description</td>
                {{-- <td>html</td> --}}
                <td>functiona</td>
            </thead>
            <tbody>
                @foreach ( $data->email_template_list as $list )
                <tr>
                    <td>#</td>
                    <td>{{$list->name }}</td>
                    <td>{{$list->description }}</td>
                    {{-- <td>{{$list->html_text }}</td> --}}
                    <td>
                    <a href="{{ 'mail_template_edit'.'/'.$list->id  }}" class="btn btn-app">edit</a>|
                    <a href="{{ 'mail_view'.'/'.$list->id  }}" class="btn btn-app">view</a>|
                    <a href="{{ 'mail_delete'.'/'.$list->id  }}" class="btn btn-app">delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endsection

