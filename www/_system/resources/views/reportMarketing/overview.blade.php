@extends('layout') @section('content')

<!-- ************************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!-- ************************************************************************************ -->
<!-- ************************************************************************************ -->


<div class="content" >
{{-- <div class="container-fluid"> --}}
    
        <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h3 class="title">Overview</h3>
                            <p class="category">Marketing Report Overview</p>
        
                        </div>
        
<div class ="content">
    <form action="" method="get" class="form form-inline">
        <div class="row">
            <div class="form-group">
                <label for="type" class="control-label">Type : </label>
                <select name="type" id="type" class="form-control">
                    @foreach( $data->type as $type ) @if( $data->curent_type == $type->id )
                    <option value="{{$type->id}}" selected>{{ $type->name }}</option>
                    @else
                    <option value="{{$type->id}}">{{ $type->name }}</option>
                    @endif @endforeach
                </select>
            </div>

            <div class="form-group ">
                <label for="start_date" class="control-label"> start date : </label>
                <input class="date form-control" type="text" name="start_date" id="" value="{{$data->start_date }}">
            </div>

            <div class="form-group ">
                <label for="end_date" class="control-label"> End date : </label>
                <input class="date form-control" type="text" name="end_date" id="" value="{{$data->end_date }}">
            </div>

            <div class="form-group">
                <input type="submit" id="" value="Search">
            </div>
        </div>
    </form>

    <hr>

    <div>
        <table class="table table-bordered">
            <thead style="background-color:  burlywood;">
                <th>Source / date</th>
                @foreach( $data->time_set->daystring as $item )
                <th>{{$item }}</th>
                @endforeach
            </thead>
            <tbody>
                @foreach( $data->source_data as $item )
                    <tr style="background-color: darkgray;">
                        <td>
                            {{$item->name }}
                        </td>
                        @foreach( $item->total as $total )
                        <td>{{$total->count_source}}</td> 
                        @endforeach
                    </tr>
                    <!-- this for loop data channels -->
                    @foreach(  $item->channel as $channel)
                    <tr>
                        <td>
                            {{$channel->name}}
                        </td>
                        @foreach($channel->data_per_date as $data_per_date )
                        <td>   {{$data_per_date->count_call_history}}</td> <!-- result -->
                        @endforeach
                    </tr>
                    @endforeach
                     <!-- this for loop data channels -->
                @endforeach
            </tbody>
        </table>
    </div>

</div>
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>

@endsection