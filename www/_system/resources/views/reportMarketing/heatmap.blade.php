@extends('layout') @section('content')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/heatmap.js"></script>
<!-- <script src="https://code.highcharts.com/modules/exporting.js"></script> -->
<!-- ************************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!-- ************************************************************************************ -->
<!-- ************************************************************************************ -->

<!-- <a href="{{'page'}}" target="_blank">Open in a new tab</a> -->
<!-- <h1>Report Marketing</h1> -->
<hr>
<div>
    <form action="" method="get" class="form form-inline">
        <div class="row">
            <div class="form-group">
                <label for="type" class="control-label">Type : </label>
                <select name="type" id="type" class="form-control">
                    @foreach( $data->type as $type ) @if( $data->curent_type == $type->id )
                    <option value="{{$type->id}}" selected>{{ $type->name }}</option>
                    @else
                    <option value="{{$type->id}}">{{ $type->name }}</option>
                    @endif @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="source" class="control-label"> Source : </label>
                <select name="source" id="source" class="form-control">
                    @foreach( $data->source_data as $source ) @if( $current_source->id == $source->id )
                    <option value="{{$source->id}}" selected>{{ $source->name }}</option>
                    @else
                    <option value="{{$source->id}}">{{ $source->name }}</option>
                    @endif @endforeach
                </select>
            </div>

            <div class="form-group ">
                <label for="start_date" class="control-label"> start date : </label>
                <input class="date form-control" type="text" name="start_date" id="" value="{{$data->start_date }}">
            </div>

            <div class="form-group ">
                <label for="end_date" class="control-label"> End date : </label>
                <input class="date form-control" type="text" name="end_date" id="" value="{{$data->end_date }}">
            </div>

            <div class="form-group">
                <input type="submit" id="" value="Search">
            </div>
        </div>

    </form>
    <hr>
</div>

@if($data->source_search)
<div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <strong> Total in {{ $current_source->name}}</strong>
            </tr>
            @foreach($data->time_set->daystring as $daystring)
            <th class="text-strong" style="background-color:  burlywood;"> {{ $daystring }} </th>
            @endforeach
        </thead>
        <tbody>
            <tr>
                @foreach($data->total as $total)
                <td> {{ $total->count_source }} </td>
                @endforeach
            </tr>
        </tbody>
    </table>
</div>

<?php
$height = 90+count($data->get_channel)*20;
$width = count($data->time_set->daystring)*75.7;
$width_max = count($data->time_set->daystring)*100;
//echo count($source_series->channel_name)."<br>";
//echo count($data->time_set->daystring)."<br>";
if(count($data->time_set->daystring) == 1){
    $width_max = count($data->time_set->daystring)*180;
}
if(count($data->get_channel) == 0){
    $height = 200;
}
?>
<div id="container" style="height: {{$height}}px; min-width:{{$width }}px ; max-width: {{$width_max }}px; "></div>

<script>
    //var arrayData = ['Alexander', 'Marie', 'Maximilian', 'Sophia', 'sdfd', 'Maria', 'Leon', 'Anna', 'Tim', 'Laura'];
    var arrayDataMember = <?php echo json_encode($data->get_channel); ?>;
    var days = <?php echo json_encode( $data->time_set->daystring ); ?>;
    // console.log(days);
    //console.log('arrayDataMember', arrayDataMember , dd);
    if (<?= printf($data->source_search); ?>) {
        Highcharts.chart('container', {
            chart: {
                type: 'heatmap',
        // Edit chart spacing

                // backgroundColor: {
                //     linearGradient: [100, 100, 500, 400],
                //     stops: [
                //         [0, 'rgb(255, 255, 255)'],
                //         [1, 'burlywood']
                //     ]
                // },
            },
            credits: {
                enabled: false
            },
            title: {
                // text: 'Sales Report Marketing'
                text: ''
            },
            subtitle: {
                text: 'Source by: {{$current_source->name}}'
            },
            xAxis: {
                categories: days, //['31/5', '1/6', '2/6', '3/6', '4/6', '5/6', '6/6', '7/6', '8/6', '9/6', '10/6','11/6', '12/6'],
                opposite: true,
            },
            yAxis: {
                categories: arrayDataMember, //['Alexander', 'Marie', 'Maximilian', 'Sophia', 'Total', 'Maria', 'Leon', 'Anna', 'Tim','Laura'],
                title: 'channel',
            },
            colorAxis: {
                min: 0,
                // minColor: '#ffffff',
                // maxColor: '#01579b',
                minColor: '#FFFFFF',
                maxColor: '#8eb9ff'
                //  maxColor: Highcharts.getOptions().colors[0]
            },
            legend: {
                enabled: false
                // align: 'top',
                // layout: 'vertical',
                //margin: 0,
                //verticalAlign: 'top',
                // y: 70,
               // symbolHeight: 280
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
                        this.point.value + '</b> kpi on <br><b>' + this.series.yAxis.categories[this.point
                            .y] +
                        '</b>';
                }
            },
            series: [{
                borderWidth: 2,
                data: <?php echo json_encode( $data->series_data )?>,
                //[ time(x) , channel(y) , kpi ]
                // ],
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    style: {
                        textOutline: false
                    }
                },
            }],

        }, function (chart) { // on complete
            @if(  count($data->series_data) == 0 )
            chart.renderer.text('No Data Available', 100, 100)
                .css({
                    color: '#4572A7',
                    fontSize: '16px',
                })
                .add();
            @endif
        });
    }
</script>
@endif

<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>

@endsection