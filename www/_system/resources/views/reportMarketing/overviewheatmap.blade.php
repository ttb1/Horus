@extends('layout') @section('content')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/heatmap.js"></script>
<!-- <script src="https://code.highcharts.com/modules/exporting.js"></script> -->
<!-- ************************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!-- ************************************************************************************ -->
<!-- ************************************************************************************ -->

<div>
    <form action="" method="get" class="form form-inline">
        <div class="row">
            <div class="form-group">
                <label for="type" class="control-label">Type : </label>
                <select name="type" id="type" class="form-control">
                    @foreach( $data->type as $type ) @if( $data->curent_type == $type->id )
                    <option value="{{$type->id}}" selected>{{ $type->name }}</option>
                    @else
                    <option value="{{$type->id}}">{{ $type->name }}</option>
                    @endif @endforeach
                </select>
            </div>

            <div class="form-group ">
                <label for="start_date" class="control-label"> start date : </label>
                <input class="date form-control" type="text" name="start_date" id="" value="{{$data->start_date }}">
            </div>

            <div class="form-group ">
                <label for="end_date" class="control-label"> End date : </label>
                <input class="date form-control" type="text" name="end_date" id="" value="{{$data->end_date }}">
            </div>
            <div class="form-group">
                <input type="submit" id="" value="Search">
            </div>
        </div>
    </form>
    <hr>
</div>


<div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <strong> Total all source </strong>
            </tr>
            <th  style="background-color:  burlywood;">source</th>
            @foreach($data->time_set->daystring as $daystring)
            <th class="text-strong" style="background-color:  burlywood;"> {{ $daystring }} </th>
            @endforeach
        </thead>
        <tbody>
            @foreach( $data->source_series as $source_total)
            <tr>
            <td> {{ $source_total->name }} </td>            
                @foreach($source_total->total as $total)
                <td> {{ $total->count_source }} </td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@foreach( $data->source_series as $source_series)

<?php
$height = 90+count($source_series->channel_name)*20;
//echo count($source_series->channel_name)."<br>";
if(count($source_series->channel_name) == 0){
    $height = 200;
}
?>

<div id="container{{ $source_series->name}}" style="height: {{$height}}px; min-width: 100%; max-width: 800px; margin: 0 auto"></div>
<hr>
<script>
    var arrayDataMember = <?php echo json_encode($source_series->channel_name); ?>;
    var days = <?php echo json_encode( $data->time_set->daystring ); ?>;
    Highcharts.chart('container{{ $source_series->name}}', {
        chart: {
            type: 'heatmap',
        },
        credits: {
            enabled: false
        },
        title: {
            text: '{{ $source_series->name}}'
        },
        xAxis: {
            categories: days, //['Alexander', 'Marie', 'Maximilian', 'Sophia', 'Lukas', 'Maria', 'Leon', 'Anna', 'Tim','Laura'],
            opposite: true,
        },
        yAxis: {
            categories: arrayDataMember, // ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'],
            title: null,
        },
        colorAxis: {
            min: 0,
            minColor: '#FFFFFF',
            maxColor: Highcharts.getOptions().colors[0]
        },
        legend: {
            // align: 'right',
            // layout: 'vertical',
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.xAxis.categories[this.point.x] + '</b> sold <br><b>' +
                    this.point.value + '</b> items on <br><b>' + this.series.yAxis.categories[this.point.y] +
                    '</b>';
            }
        },
        series: [{
            name: 'Sales per employee',
            borderWidth: 1,
            data: <?php echo json_encode( $source_series->data )?>,
            dataLabels: {
                enabled: true,
                color: '#000000'
            }
        }]
    }, function (chart) { // on complete
        @if(count($source_series->data) == 0)
        chart.renderer.text('No Data Available', 100, 100)
            .css({
                color: '#4572A7',
                fontSize: '16px',
            })
            .add();
        @endif
    });
</script>
@endforeach

<!-- script -->
<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
@endsection