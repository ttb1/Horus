@extends('layout') @section('content')

<!-- ************************************************************************ -->
<!-- ************************************************************************ -->
<!-- ************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<!-- ************************************************************************ -->
<!-- ************************************************************************ -->
<!-- ************************************************************************ -->

<div class="content" >




<!-- <hr> -->

<!-- 
<div class="container-fluid">
    <table class="table table-bordered">
        <thead>
            <tr class="text-strong">
                <th>Source</th>
                <th>Total</th>
                <th>detail</th>
            </tr>
        </thead>
        <tbody>
            @foreach( $data->result_source as $source_data )
            <tr>
                <td>{{ $source_data->source_name }}</td>
                <td>{{ $source_data->count_source }}</td>
                <td>detail</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div> -->


{{-- 
<div class="container-fluid">
    <table class="table table-bordered">
        <thead>
            <tr class="text-strong" style="background-color:  burlywood;">
                <th>channel</th>
                @foreach( $data->time_set->daystring as $item)
                <th>{{ $item }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <tr style="background-color: darkgray;">
            <td>Total</td>
                @foreach($data->channel_data->total as $total)
                <td>{{ $total->count_source}}</td>
                @endforeach
            </tr>
            @foreach( $data->channel_data->channel_source as $channel_source )
            <tr>
                <td>{{$channel_source->team}} </td>
                @foreach( $channel_source->channel as $item)
                <td>{{ $item->count_source }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>
</div> --}}



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h3 class="title">KPI</h3>
                    <p class="category">Key Performance Indicator</p>

                </div>

                <div class="content">
                        <form action="" method="get" class="form form-inline">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="type" class="control-label">Type : </label>
                                        <select name="type" id="type" class="form-control">
                                            @foreach( $data->type as $type ) @if( $data->curent_type == $type->id )
                                            <option value="{{$type->id}}" selected>{{ $type->name }}</option>
                                            <?= $type_view = $type->name; ?>
                                                @else
                                                <option value="{{$type->id}}">{{ $type->name }}</option>
                                                @endif @endforeach
                                        </select>
                                    </div>
                           
                                    <div class="form-group">
                                        <label for="source" class="control-label"> Source : </label>
                                        <select name="source" id="source" class="form-control">
                                            @foreach( $data->source_data as $source ) @if( $data->current_source->id == $source->id )
                                            <option value="{{$source->id}}" selected>{{ $source->name }}</option>
                                            @else
                                            <option value="{{$source->id}}">{{ $source->name }}</option>
                                            @endif @endforeach
                                        </select>
                                    </div>
                            
                                    <div class="form-group ">
                                        <label for="start_date" class="control-label"> Start : </label>
                                        <input class="date form-control" type="text" name="start_date" id="" value="{{$data->start_date }}">
                                    </div>
                            
                                    <div class="form-group ">
                                        <label for="end_date" class="control-label"> End : </label>
                                        <input class="date form-control" type="text" name="end_date" id="" value="{{$data->end_date }}">
                                    </div>
                            
                                    <div class="form-group">
                                        <input type="submit" class ="btn btn-crm"id="" value="Search">
                                    </div>
                                </div>
                            </form>
                        <br>
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr class="text-strong" style="background-color:  burlywood;">
                                        <th>Channel</th>
                                        @foreach( $data->time_set->daystring as $item)
                                        <th>{{ $item }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="background-color: darkgray;">
                                    <td>Total</td>
                                        @foreach($data->channel_data->total as $total)
                                        <td>{{ $total->count_source}}</td>
                                        @endforeach
                                    </tr>
                                    @foreach( $data->channel_data->channel_source as $channel_source )
                                    <tr>
                                        <td>{{$channel_source->team}} </td>
                                        @foreach( $channel_source->channel as $item)
                                        <td>{{ $item->count_source }}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>

@endsection

