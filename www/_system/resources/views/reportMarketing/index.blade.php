@extends('layout') @section('content')

<div class="content" style="position:unset; -index: -1;">
    <div class="main-header">
        <h2>Marketing Report </h2>
        <em>More Information Report</em>
    </div>
    <hr> {{--
    <div class="container-fluid">
        <a href="{{ 'reportmarketing/viewbysource' }}" target="_blank">@ marketingreport</a>
        <br>
        <a href="{{ 'reportmarketing/heatmap' }}" target="_blank">@ marketingreportheatmap</a>
        <br>
        <a href="{{ 'reportmarketing/overview' }}" target="_blank">@ marketingreportoverview</a>
    </div> --}}


    <div class="row">
        <div class="col-lg-3 col-sm-6 set-no-padding ">
            <a href="{{ 'reportmarketing/viewbysource' }}">
                <div class="card card-stats">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big text-center icon-warning">
                                    <i class="glyphicon glyphicon-flag" style="color:chocolate;"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                        <a href="{{ 'reportmarketing/viewbysource' }}">
                                    <p>KPI</p>
                                    <h6>
                                        <em>Key Performance Indicator</em>
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-refresh">
                                </i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-sm-6 set-no-padding">
             <a href="{{ 'reportmarketing/heatmap' }}">
            <div class="card card-stats">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big text-center icon-warning">
                                <i class="glyphicon glyphicon glyphicon-signal" style="color:#ED4C67;"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <a href="{{ 'reportmarketing/heatmap' }}">
                                    <p>Heat Map</p>
                                    <h6>
                                        <em>Heat Map Report</em>
                                    </h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-calendar-o">
                            </i> Last day <a href="{{'reportmarketing/overviewheatmap'}}">overviewheatmap</a>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>

        <div class="col-lg-3 col-sm-6 set-no-padding">
            <div class="card card-stats">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big text-center icon-warning">
                                <i class="glyphicon glyphicon-list-alt" style="color:#2d98da"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <a href="{{ 'reportmarketing/overview' }}">
                                    <p>Overview</p>
                                    <h6>
                                        <em>Overview Report</em>
                                    </h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-clock-o"></i> In the last hour</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 set-no-padding">
            <div class="card card-stats">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big text-center icon-warning">
                                <i class="glyphicon glyphicon-option-horizontal" style="color:green;"></i>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="numbers">
                                <a href="#" target="_blank">
                                    <p>Other</p>
                                    <h6>
                                        <em>Report</em>
                                    </h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <hr>
                        <div class="stats">
                            <i class="fa fa-clock-o"></i>Soon</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 set-no-padding">
            <div class="card">
                <div class="header">
                    <h5 class="title">Chart</h5>
                    <p class="category">24 Hours performance</p>
                </div>
                <div class="content">
                        <div class="ct-chart">
                            <div class="ct-chart ">
                            </div>
                         </div>
                    </div>
            </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="header">
                        <h5 class="title">Users Behavior</h5>
                        <p class="category">24 Hours performance</p>
                    </div>
                    <div class="content">
                            <div class="ct-chart">
                                <div class="ct-chart ">
                                </div>
                             </div>
                        </div>
                </div>
            </div>

    </div>
    @endsection