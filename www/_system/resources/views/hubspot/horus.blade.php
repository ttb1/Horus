<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Horus</title>

    <!-- Fonts -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> --}}

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

    <link href="/css/app.css" rel="stylesheet" type="text/css">

</head>

<script type="text/javascript">
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
</script>
<body>
    <div class="container">
        <br>
        <input type="hidden" name="phone" value="{{$phone}}" id="phone">
        <div id="horus">
        </div>
    </div>
    <script src="/js/app.js" type="text/javascript"></script>
</body>



</html>