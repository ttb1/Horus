<div>
    <label for="">Interview schedule </label>
    <table class="table">
        <thead>
            <th>date</th>
            <th>time</th>
            <th>teacher</th>
            <th>description</th>
            <th>description(cm)</th>
            <th>status</th>
            <th>function</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <input type="text"  name="date_schedule" id="date_schedule" value="{{ $today }}" class="form-control date_schedule" >
                </td>
                <td>
                    <select name="time_schedule" id="time_schedule" class="form-control">
                        @foreach($time_schedule as $time)
                        <option value="$time->id">{{ $time->time_period }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="teacher" id="teacher" class="form-control">
                        <option value="Native">Native</option>
                        <option value="Local">Local</option>
                    </select>
                </td>
                <td>
                    <input type="text" id="description" name="description" class="form-control ">
                </td>
                <td>
                    <input type="text" class="form-control">
                </td>
                <td>
                    <select name="" id="" class="form-control">
                        @foreach( $tester_status as $tester_status )
                        <option value="$tester_status->id">{{ $tester_status->name }}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <button class="btn-danger" type="button" onclick="bookingTester()">
                        <span class="glyphicon glyphicon-ok"></span>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $('#date_schedule').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
</script>

<script>
    function bookingTester(){
        alert('sdsd');
    }

</script>