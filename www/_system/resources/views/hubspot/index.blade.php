<!DOCTYPE html>
<html lang="en">

<head>
    <!--Header Area -->
    @include('includes.header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
</head>

<body>
    <div class="container">

        <div class="text-center">
            <h1>contact</h1>
        </div>

        <hr>

        <div class="row">
            <form action="#" method="post" class="form form-horizontal">
                <input type="hidden" id="call_id" value=""> {{-- this don't move --}}
                <input type="hidden" id="engagement_id" value=""> {{-- this don't move --}}
                <div class="form-group">
                    <label for="firstname" class="control-label col-sm-1">Firstname</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value=" {{ $data->properties->firstname->value !=  null ? $data->properties->firstname->value : $data->properties->name->value }}">
                    </div>
                    <label for="lastname" class="control-label col-sm-1">Lastname</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" value=" {{$data->properties->lastname->value}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tel" class="control-label col-sm-1">Tel</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="phone_number" value="0{{$data->properties->phone->value}}">
                    </div>
                    <label for="email" class="control-label col-sm-1">Email</label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" value="{{$data->properties->email->value}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="create_at" class="control-label col-sm-1">Create at</label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" value="{{$createdate}}">
                    </div>
                    <label for="last_activity" class="control-label col-sm-1">Lastactivity</label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="call_quality" class="control-label col-sm-1">Call_quality</label>
                    <div class="col-sm-5">
                        <select name="call_quality" id="call_quality" class="select2-drop form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <label for="c3_quality" class="control-label col-sm-1">c3_quality</label>
                    <div class="col-sm-5">
                        <select name="call_quality" id="call_quality" class="select2-drop form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="call" class="control-label col-sm-1">Owner</label>
                    <div class="col-sm-3">
                        <input type="text" id="phone_by" class="form-control" readonly value="{{$get_owner->email}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="call" class="control-label col-sm-1">station1</label>
                    <div class="col-sm-3">
                        <input type="text" id="phone_by" class="form-control" readonly value="{{$get_owner->station1}}">
                    </div>
                    <label for="call" class="control-label col-sm-1">station2</label>
                    <div class="col-sm-3">
                        <input type="text" id="phone_by" class="form-control" readonly value="{{$get_owner->station2}}">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="call" class="control-label col-sm-1">Call</label>
                    <div class="col-sm-7">
                        <input type="hidden" id="status_phone" value="1">
                        <input type="hidden" id="phone_by">
                        <button type="button" class="btn btn-success" id="left_phone" onclick="callBy(1)">Call by {{$get_owner->station1}}</button> |
                        <button type="button" class="btn btn-success" id="right_phone" onclick="callBy(2)">Call by {{$get_owner->station2}}</button>
                    </div>
                </div>
                <div class="form-group">
                    <label for="call" class="control-label col-sm-1">Status</label>
                    <div class="col-sm-7">
                        <button type="button" id="call_status" readonly class="btn btn-lighter"> No calling </button>
                        <span id="status_call_id"></span>
                        <button type="button" id="end_call_status" class="btn btn-danger" style="display: none" onclick="callDone()"> End Call </button>
                    </div>
                </div>
                <hr>
            </form>
        </div>

        {{-- @include('hubspot.interview'); --}}

    </div>
</body>

<script>
    function startCall(contact_id, mobile_phone, station_id) {
        $.ajax({
            method: "GET",
            url: "{{ url('startcall')}}",
            data: {
                contact_id: contact_id,
                mobile_phone: mobile_phone,
                station_id: station_id // user station
            },
            statusCode: {
                500: function () {
                    alert("error code :500");
                },
                404: function () {
                    alert("page not found /404");
                }
            }
        }).done(function (data) {
            var get = JSON.parse(data);
            document.getElementById('call_id').value = get.result.data.call_id;
            document.getElementById('status_call_id').textContent = get.result.data.call_id;
            document.getElementById('engagement_id').value = get.add_engagment.engagement.id;

            //console.log(get.add_engagment.engagement.id);
            //console.log(data);
        });
    }
</script>
<script>
    function callBy(key) {
        // alert('Call by 02-XXXXXX');
        let status = $("#status_phone").val();
        document.getElementById('phone_by').value = key;
        if (key == 1) {
            if (status != '2') {
                document.getElementById('status_phone').value = 2;
                var call_status = document.getElementById("call_status");
                call_status.textContent = "Calling...";
                document.getElementById('end_call_status').style.display = 'block';
                document.getElementById('right_phone').disabled = true;
                let contact_id = '{{ $data->vid }}'; //vid is contact_id
                let mobile_phone = document.getElementById('phone_number').value //'0{{$data->properties->phone->value}}';
                let station_id = '{{$get_owner->station1}}';
                startCall(contact_id, mobile_phone, station_id);
            } else {
                document.getElementById('status_phone').value = 1;
                var elem = document.getElementById("call_status");
                elem.textContent = "No calling";
                document.getElementById('end_call_status').style.display = 'none';
                document.getElementById('right_phone').disabled = false;
                // let station_id = '{{$get_owner->station1}}';
                // let engagement_id = engagement_id;
                let call_id = $("#call_id").val()
                let vid = '{{ $data->vid }}';
                let phone = document.getElementById('phone_number').value //'0{{$data->properties->phone->value}}';
                let station_id = '{{$get_owner->station1}}';
                let engagement_id = document.getElementById('engagement_id').value;

                //console.log('call_id',call_id);
                endCall(vid, phone, station_id, call_id, engagement_id);
            }
        } else if (key == 2) {
            if (status != '2') {
                document.getElementById('status_phone').value = 2;
                var call_status = document.getElementById("call_status");
                call_status.textContent = "Calling...";
                document.getElementById('end_call_status').style.display = 'block';
                document.getElementById('left_phone').disabled = true;
                let contact_id = '{{ $data->vid }}'; //vid is contact_id
                let mobile_phone = document.getElementById('phone_number').value //'0{{$data->properties->phone->value}}';
                let station_id = '{{$get_owner->station2}}';
                startCall(contact_id, mobile_phone, station_id);

            } else {
                document.getElementById('status_phone').value = 1;
                var elem = document.getElementById("call_status");
                elem.textContent = "No calling";
                document.getElementById('end_call_status').style.display = 'none';
                document.getElementById('left_phone').disabled = false;
                let call_id = $("#call_id").val()
                let vid = '{{ $data->vid }}';
                let phone = document.getElementById('phone_number').value //'0{{$data->properties->phone->value}}';
                let station_id = '{{$get_owner->station2}}';
                let engagement_id = document.getElementById('engagement_id').value;
                endCall(vid, phone, station_id, call_id, engagement_id);
            }
        }
    }

    function callDone() {
        let key = document.getElementById('phone_by').value;
        document.getElementById('status_call_id').textContent = "";
        callBy(key);
    }

    function endCall(contact_id, mobile_phone, station_id, call_id, engagement_id) {
        console.log(engagement_id);
        $.ajax({
            method: "GET",
            url: "{{ url('endCall')}}",
            data: {
                contact_id: contact_id,
                mobile_phone: mobile_phone,
                station_id: station_id, // user station
                call_id: call_id,
                engagement_id: engagement_id
            },
            statusCode: {
                500: function () {
                    alert("error /500");
                },
                404: function () {
                    alert("page not found /404");
                }
            }
        }).done(function (data) {
            console.log(data);
            // self.close();

        });
    }
</script>

</html>