<head>    
    <meta charset="utf-8" />
    <title>Topica CRM</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--============================================= Stylesheets ================================================-->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css')}}">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > --}}

    <!--============================================ Java Scripts ===============================================-->
    <script type='text/javascript' src="{{ url('js/Scripts/jquery.min.js')}}"></script>
    <script type='text/javascript' src="../js/bootstrap.min.js"></script> 
  
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

</head>

 