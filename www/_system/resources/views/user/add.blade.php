<html>

@extends('layout')

<body>
    @section('content')
    <!--- Content Text Area -->
    {{--
    <input type="button" class="btn btn-link lg" value="Back" onClick="history.go(-1);"> --}}
    <form action="{{ route('user_create') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="col-lg-6">
            <h2>Create User </h2>
            <hr>
            <div class="form-group">
                <label class="control-label ">Username</label>
                <input class="form-control" name="username" type="text" placeholder="Enter Username">
            </div>

            <div class="form-group">
                <label class="control-label ">Password</label>
                <input class="form-control" name="password" type="password" placeholder="Enter Password">
            </div>

            <div class="form-group">
                <label class="control-label">Firstname</label>
                <input class="form-control" name="firstname" type="text" placeholder="Enter Firstname">
            </div>

            <div class="form-group">
                <label class="control-label ">Lastname</label>
                <input class="form-control" name="lastname" type="text" placeholder="Enter Lastname">
            </div>

            <div class="form-group">
                <label class="control-label ">Role</label>
                <select class="form-control" name="role_id">
                    @foreach($roles as $role)
                    <option @if ($role->name == $default_add_role) selected="selected" @endif value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>

            </div>

            <div class="form-group">
                <label class="control-label ">Type</label>
                <select class="form-control" name="user_type">
                   <option  value="a">a</option>
                   <option  value="b">b</option>
                   <option  value="c">c</option>
                   <option  value="d">d</option>
                </select>
            </div>

            <div class="form-group">
                <label class="control-label ">email</label>
                <input class="form-control" name="email" type="email" value="">
            </div>
            <div class="form-group">
                <label class="control-label ">station_id 1</label>
                <input class="form-control" name="station_id[0]" type="text" value="0" placeholder="Enter ...">
            </div>
            <div class="form-group">
                <label class="control-label ">station_id 2</label>
                <input class="form-control" name="station_id[1]" type="text" value="0" placeholder="Enter ...">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div>
        </div>

    </form>
    @endsection

</body>

</html>