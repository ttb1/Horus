<html>
@extends('layout')

<body>

    @section('content')
    <!--- Content Text Area -->
    {{--
    <input type="button" class="btn btn-link lg" value="Back" onClick="history.go(-1);"> --}}
    <form action="{{ route('user_update') }}" method="POST">
        <input type="hidden" name="user_id" value="{{ $user->id }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{--
        <input type="hidden" name="password" value="{{ $user->password }}" id="password"> --}}
        <div class="col-lg-6">
            <h2>Edit User </h2>
            <hr>
            <div class="form-group">
      
            <div class="form-group">
                <label class="control-label ">Username</label>
                <input  class="form-control" name="username" type="text" value="{{ $user->username }}"placeholder="Enter ...">
            </div>

                <div class="form-group">
                    <label class="control-label ">Password</label>
                    <input class="form-control" name="password" placeholder="xxxxxxxxxx" readonly>
                </div>

                <div class="form-group">
                    <label class="control-label ">New-Password</label>
                    <input class="form-control" name="new_password" type="password" id="new_password" placeholder="Enter ...">
                </div>
                <div class="form-group">
                    <label class="control-label ">Firstname</label>
                    <input class="form-control" name="firstname" type="text" value="{{ $user->firstname }}" placeholder="Enter ...">
                </div>

                <div class="form-group">
                    <label class="control-label ">Lastname</label>
                    <input class="form-control" name="lastname" type="text" value="{{ $user->lastname }}" placeholder="Enter ...">
                </div>

                <div class="form-group">
                    <label class="control-label ">Role</label>
                    <select class="form-control" name="role_id">
                        @foreach($roles as $role)
                        <option @if ($role->id == $user->role_id) selected="selected" @endif value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label ">Type</label>
                    <select class="form-control" name="user_type">
                       <option @if ($user->type == 'a' ) selected="selected" @endif value="a">a</option>
                       <option @if ($user->type == 'b' ) selected="selected" @endif value="b">b</option>
                       <option @if ($user->type == 'c' ) selected="selected" @endif value="c">c</option>
                       <option @if ($user->type == 'd' ) selected="selected" @endif value="d">d</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label ">email</label>
                    <input class="form-control" name="email" type="email" value="{{ $user->email }}">
                </div>
                <div class="form-group">
                    <label class="control-label ">station_id 1</label>
                    <input class="form-control" name="station_id[0]" type="text" value="{{ $user->station_id[0] }}" placeholder="Enter ...">
                </div>
                <div class="form-group">
                    <label class="control-label ">station_id 2</label>
                    <input class="form-control" name="station_id[1]" type="text" value="{{ $user->station_id[1] }}" placeholder="Enter ...">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right">EDIT</button>
                </div>

            </div>
        </div>
    </form>
    @endsection

</body>

</html>
