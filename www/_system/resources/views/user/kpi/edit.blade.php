@extends('layout') @section('content')

<a href="{{ route('user_kpi')}}">
    <span class="glyphicon glyphicon-backward"></span> Back</a>

<h1>Edit</h1>
<hr>

<div class="col-sm-6">
    <form action="{{ route('update_kpi')}}" method="POST" class="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{  $kpi_group->id }}">

        <label for="name">Name</label> :
        <input type="text" name="name" value="{{$kpi_group->name}}">
        <br>

        <label for="description">Description</label> :
        <input type="text" name="description" value="{{ $kpi_group->description }}">
        <br>

        <label for="KPI">KPI</label> :
        <input type="text" name="kpi" value="{{$kpi_group->kpi}}">
        <br>

        <hr>

        <h2>user list </h2>
        <label for="user" class="control-label">Menber</label>

        <textarea name="apply_List" id="apply_List" class="form-control" readonly="readonly">@foreach($user_kpi_role as $key){{$key->user_name}},@endforeach</textarea>
        <hr>
        <button type="button" class="btn btn-sm" onclick="Userselect()">
            <span class="glyphicon glyphicon-transfer text-info"></span>
        </button>

        <label for="user" class="control-label">user</label>
        <select id="userselect" class="form-control" multiple="multiple">
            @foreach($users as $key)
            <option value="{{$key->name}}">{{$key->name}}</option>
            @endforeach
        </select>

        <hr>
        <input type="submit" value="Save">
    </form>
</div>
@endsection


<script>
    function Userselect() {
        var sel = $('#userselect').val();
        document.getElementById("apply_List").innerHTML = sel;
    }
    // function Userselect() {
    //     var sel = $('#userselect').val();
    //     var x = document.getElementById("apply_List");
    //     for (let i = 0; i < sel.length; i++) {
    //         var option = document.createElement("option");
    //         option.text = sel[i];
    //         x.add(option);
    //     }

    // }

    // function deleteoption() {
    //     var x = document.getElementById("apply_List");
    //     var seluser = $('#userselect').val();
    //     var u = document.getElementById("userselect");
    //     var sel = $('#apply_List').val();
    //     console.log(u);
    //     for (let i = 0; i < sel.length; i++) {
    //         x.remove(x.selectedIndex);
    //     }
    // }
</script>