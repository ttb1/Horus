
@extends('layout')

@section('content')

<h1>User KPI</h1>
<hr>
<div class="container-fluid" >
    <a href="{{ route('user_kpi_add')}}"><span class="glyphicon glyphicon-plus"></span> Add KPI</a>
</div>
<hr>

<table class="table table-table-responsive">
    <thead>
        <tr>
            <td>#</td>
            <td>Name</td>
            <td>Description</td>
            <td>KPI</td>
            <td>#</td>
        </tr>
    </thead>
    <tbody>
    <?php $n = 0; ?>
    @foreach($kpi_group as $item)
        <tr>
    <?php $n++ ; ?>
            <td>{{ $n }}</td>            
            <td>{{$item->name}}</td>            
            <td>{{$item->description}}</td>            
            <td>{{$item->kpi}}</td>            
            <td> 
            <a href="{{ route('kpi_edit',['id'=>$item->id]) }}">Edit</a> |
            <a href="{{ route('kpi_remove',['id'=>$item->id])}}" onclick="return confirm('Are you sure?')">Remove</a>            
            </td>            
        </tr>
    @endforeach 
    </tbody>
</table>
 

@endsection