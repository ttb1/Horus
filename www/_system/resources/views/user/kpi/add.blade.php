@extends('layout')

@section('content')

<h1>Add KPI</h1>
<a href="{{ route('user_kpi')}}"><span class="glyphicon glyphicon-backward"></span> Back</a>
<hr>

<div class="container-fluid" >

<div class="col-sm-6">
<form action="{{ route('create_kpi')}}" method="POST" class="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">    

        <div class="form-group">
            <label for="name" class="control-label">name</label>
            <input type="text" name="name" id="name" class="form-control">            
        </div>
        <div>
            <label for="description" class="control-label">description</label>
            <input type="text" name="description" id="description" class="form-control">
        </div>
        

        <div class="form-group">
            <label for="kpi" class="control-label">kpi</label>
            <input type="number" name="kpi" id="kpi" class="form-control">
        </div>
       
        <hr>

        <div class="form-group">
            <label for="user_id">User_Id :</label>
            <select id="userselect" class="form-control" multiple="multiple">
            @foreach($user as $key)
                <option value="{{$key->name}}">{{$key->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group" >
        <button type="button" class="btn btn-sm" onclick="Userselect()"><span class="glyphicon glyphicon-sort text-info"></span> </button>        
        </div>
            <div class="form-group">
                <label for="user"  class="control-label">user</label>
                <textarea name="apply_List" id="apply_List" class="form-control" readonly="readonly"></textarea>
            </div>
        <hr>
        <input type="submit" value="Submit">
    </form>
</div>
   

</div>

@endsection



<script>       
    function Userselect() {
    var sel = $('#userselect').val();
    document.getElementById("apply_List").innerHTML = sel;
    }
</script>