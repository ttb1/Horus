
 @extends('layout')
     @section('content')
            <!--- Content Text Area -->
            <h2>User Management</h2><hr>
            <form action="{{ route('user_add') }}" method="GET" >
            <button type="submit" class="btn btn-link" >Create User</button>
            </form> 
            <form action="" method="GET">
                <input type="hidden" name="page" value="{{ $current_page }}">
                <input type="hidden" name="total" value="{{ $current_total }}">
                Page
                <button type="submit" class="btn btn-link" name="page" value="1"><<</button>
                <button type="submit" class="btn btn-link" name="page" value="{{ $current_page-1 }}"><</button>
                @for ($i = 0; $i < $total_page; $i++)
                    <?php $index = $i+1; ?>
                    @if($index == $current_page)
                        <button type="submit" class="btn btn-link" name="page" value="{{$index}}">{{$index}}<-</button>
                    @else
                        <button type="submit" class="btn btn-link" name="page" value="{{$index}}">{{$index}}</button>
                    @endif
                @endfor
                <button type="submit" class="btn btn-link" name="page" value="{{ $current_page+1 }}">></button>
                <button type="submit" class="btn btn-link" name="page" value="{{ $total_page }}">>></button>
                <br>
                Amount
                @for ($i = 0; $i < count($user_amounts); $i++)
                    <button type="submit" class="btn btn-link" name="total" value="{{$user_amounts[$i]}}">{{$user_amounts[$i]}}</button>
                @endfor
                
            </form>
                  <table class="table table-striped">
                    <thead>
                        <tr class="bg-gold"  >
                            <th colspan="6" style="text-align:left;">User Management</th>
                        </tr>
                        <tr class="well">
                            <th class="col-sm-1" >#</th>
                            <th class="col-sm-2" >Username</th>
                            <th class="col-sm-3" >Firstname</th>
                            <th class="col-sm-3" >Lastname</th>
                            <th class="col-sm-1" >Edit User</th>
                            <th class="col-sm-1" >Remove User</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr >          
                                    <td>{{$user->id}} </td>
                                    <td>{{ $user->username}}</td>
                                    <td>{{ $user->firstname}}</td>
                                    <td>{{ $user->lastname}}</td>
                                    <td> 
                                        <form action="{{ route('user_edit', $user->id) }}" method="GET" >      
                                            <button type="submit" class="btn btn-link"><img src="/img/edit-16.png "></button> 
                                            </form>
                                    </td>
                                          <td> <form action="{{ route('user_remove') }}" method="POST" id="user_remove">
                                                 <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          
                                                 <button type="submit"class="btn btn-link" onclick="return confirm('Are you sure?')">
                                                      <span class="glyphicon glyphicon-trash" style="color:#dd5a43;"></span>
                                                </button>                    
                                          </form>
                                    </td>
                                </tr>
                        @endforeach
                     </tbody>
                    </table>

    @endsection
