@extends('layout') @section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">

<h2>Role Page</h2>
{{--
<a href="{{'../page'}}">
    <span class="glyphicon glyphicon-backward btn"></span>
</a> --}}
<hr>


<form action="" method="GET">
    @foreach($rols_data as $rols_data) @if($rols_data->id == $radio_filter)
    <label class="radio-inline">
        <input type="radio" name="optradio" value="{{$rols_data->id }}" onchange="filterproperty(this.value)" checked>{{ $rols_data->name }}</input>
    </label>
    @else
    <label class="radio-inline">
        <input type="radio" name="optradio" value="{{$rols_data->id }}" onchange="filterproperty(this.value)">{{ $rols_data->name }}</input>
    </label>
    @endif @endforeach
    <button type="submit" class="hidden " id="set">Search</button>
</form>

<form action="{{ route('update_role_page') }}" method="post">
    <!-- this form for page select -->
    <select multiple="multiple" class="form-control" id="page_select" name="page_select[]" style="height:40%;" onchange="pageSelect(this.value)">
        @foreach($default_page_list as $page) @if($page->select_on == 'on')
        <option value="{{ $page->page_id }}" selected>
            {{ $page->Page_main == 'true' ? "# $page->Page_name #" : "$page->Page_name" }}
        </option>
        @else
        <option value="{{ $page->page_id }}">
            {{ $page->Page_main == 'true' ? "# $page->Page_name #" : "$page->Page_name" }}
        </option>
        @endif @endforeach
    </select>
    <!-- end this form for page select -->

    <br> #default page list

    <!-- this form for default page list -->
    <select name="default_page" id="default_page_list" class="form-control">
        @foreach($default_page_list as $page) @if($page->page_id == $current_default_page->id)
        <option value="{{$current_default_page->id}}" selected>{{$current_default_page->name}}</option>
        @endif @if($page->select_on == 'on' && $page->page_id != $current_default_page->id)
        <option value="{{$page->page_id}}">{{ $page->Page_main == 'true' ? "# $page->Page_name #" : "$page->Page_name" }}</option>
        @endif @endforeach
    </select>
    <!-- end this form for default page list -->

    <hr>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="role_id" value="{{ $radio_filter }}">
    <div class="well-sm">
        <button class="btn btn-success">Confirm</button>
    </div>
</form>
<hr> @endsection



<script type="text/javascript" charset="utf-8">
    function filterproperty(val) {
        document.getElementById("set").click();
    }

    function defaultPageList() {

        var x = document.getElementById("default_page_list");
        x.options.length = 0;

        var pagedatalist = $("#page_select").val();
        $.ajax({
            type: 'GET',
            url: '/pageselect',
            data: {
                'page_data_list': pagedatalist
            },
            success: function (data) {
                var object = JSON.parse(data);
                for (let i = 0; i < object.data.length; i++) {
                   // console.log(object.data[i].name, '/', object.data[i].id, '/', object.data[i].root_id);
                    var option = document.createElement("option");
                    if (object.data[i].root_id == null) {
                        option.text = '# '+object.data[i].name +' #';
                        option.value = object.data[i].id;
                        if ( object.data[i].id = "<?php echo $current_default_page->id ?>") {
                            option.selected = true;                    
                        }
                    } else {
                        option.text = object.data[i].name;
                        option.value = object.data[i].id;
                        if ( object.data[i].id = "<?php echo $current_default_page->id ?>") {
                            option.selected = true;                    
                        }
                    }
                    x.add(option);
                }
            }
        });




        // var list_page = $('#page_select').val();
        // var e = document.getElementById("page_select");

        // var currentPage = "<?php echo $current_default_page->name ?>";
        // for (let i = 0; i < list_page.length; i++) {
        //     var option = document.createElement("option");
        //     var strText = e.options[i].text;
        //     if (currentPage == strText) {
        //         option.text = currentPage;
        //         option.value = "<?php echo $current_default_page->id ?>";
        //         option.selected = true;
        //         x.add(option);
        //     } else {
        //         option.text = strText;
        //         option.value = list_page[i];
        //         x.add(option);
        //     }
        // }
    }

    function pageSelect(e) {
        defaultPageList();
    }
</script>