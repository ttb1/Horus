<html>
@extends('layout')

<body>

    @section('content')
    <!--- Content Text Area -->
    
   
   <form action="{{ route('page_create') }}" method="POST" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

         <div class="col-lg-6"> 
            <h2>Create Page </h2><hr>

            <div class="form-group">
                <label class="control-label ">Name</label>
                <input  class="form-control"name="name" type="text"placeholder="Name ...">
            </div>

            <div class="form-group">
                <label class="control-label ">URL</label>
                <input  class="form-control" name="url" type="text"
                    placeholder="URL ...">
            </div>  
          
            <div class="form-group">
                <label class="control-label ">Root</label>
                <select   class="form-control" name="root_id">
                    @foreach($root_pages as $root_page)                
                        <option
                            value="{{ $root_page->id }}">{{ $root_page->name }}</option>
                    @endforeach
                </select>
            </div> 

             <div class="form-group">
                 <button type="submit" class="btn btn-primary  pull-right">Add</button>
                 <input type="button" class="btn btn-link pull-right" value="Cancle" onClick="history.go(-1);">
            </div>
        </div>        
       
    </form>
@endsection

    </body>
</html>

  
