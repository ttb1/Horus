
@extends('layout')
    @section('content')
    <!--- Content Text Area -->
    <div class="row">
        <div class="pull-left"> <h2>Page Management</h2></div>         
        <div class="pull-right"> 
            {{-- <a href="{{route('contact_add')}}" class="btn btn-primary"><span class="glyphicon glyphicon-plus-sign"></span> Add</a>  --}}
            <a href="{{ route('page_role')}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>Role page</a>
        </div>
    </div>
<hr>

    {{-- <h2>Page Management</h2><hr> --}}  
        <form action="{{ route('page_add') }}" method="GET" >
            <button type="submit" class="btn btn-link">Create Page</button>
        </form>        
        <table class="table table-hover table-striped">
                    <thead>
                        <tr class="bg-gold" >
                            <th colspan="5" style="text-align:left;">Page Management</th>
                        </tr>
                        <tr class="well">
                            <th class="col-sm-2">Page Name</th>
                            <th class="col-sm-2">URL</th>
                            <th class="col-sm-1">Root ID</th>
                            <th class="col-sm-1">Edit Page</th>
                            <th class="col-sm-1">Remove Page</th>
                        </tr>
                    </thead>

                    <tbody >
                    @foreach($pages as $page)
                            <tr>
                                <span style="float:left;">                    
                                    <td>{{ $page->name}} </td>
                                    <td>{{ $page->url}}</td>
                                    <td>{{ $page->root_id}}</td>
                                    <td> 
                                        <form action="{{ route('page_edit', $page->id) }}" method="GET" >                    
                                            <button type="submit" class="btn btn-link"><img src="/img/edit-16.png "></button> 
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{ route('page_remove') }}" method="POST" id="page_remove">
                                            <input type="hidden" name="page_id" value="{{ $page->id }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit"class="btn btn-link">
                                                <span class="glyphicon glyphicon-trash" style="color:#dd5a43;"></span>
                                          </button>                 
                                        </form>
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
        <!-- <div>
             @foreach($pages as $page)
                {{ $page->name." ~ ".$page->url." ~ ".$page->root_id }}
                <form action="{{ route('page_edit', $page->id) }}" method="GET" >                    
                   <button type="submit">Edit</button>
                </form>
                <form action="{{ route('page_remove') }}" method="POST" id="page_remove">
                    <input type="hidden" name="page_id" value="{{ $page->id }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="link-button">REMOVE</button>                    
                </form>
                <br>
            @endforeach 
        </div> -->
        @endsection

