<html>
@extends('layout')

<body>

    @section('content')
    <!----- Content Text Area ------>
          <!-- {{ $page }}
        {{ $root_pages }} -->
    
        <form action="{{ route('page_update') }}" method="POST" >
            <input type="hidden" name="page_id" value="{{ $page->id }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="col-lg-6"> 
                <h2>Edit Page </h2><hr>

                <div class="form-group">
                    <label class="control-label ">Name</label>
                    <input  class="form-control" name="name" type="text" value="{{ $page->name }}"
                            placeholder="Name ...">
                </div>

                <div class="form-group">
                    <label class="control-label ">URL</label>
                    <input class="form-control" name="url" type="text" value="{{ $page->url }}"
                        placeholder="URL ...">
                </div>

                <div class="form-group">
                <label class="control-label ">Root</label>
                <select class="form-control" name="root_id">
                @foreach($root_pages as $root_page)                
                    <option
                        
                        @if ($root_page->id == $page->root_id)
                            selected="selected"
                        @endif
                        
                        value="{{ $root_page->id }}">{{ $root_page->name }}</option>
                @endforeach
                </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-right">Edit</button>
                    <input type="button" class="btn btn-link pull-right" value="Cancle" onClick="history.go(-1);">
            </div>
            </div>       
        </form>
        @endsection
        
    </body>
</html>
