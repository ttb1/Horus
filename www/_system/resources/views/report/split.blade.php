@extends('layout')
@section('content')
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

{{-- Title Header --}}
<div class="row">
        <div class="pull-left">
            <h2> SPLIT Report </h2>
        </div>
    </div>
    
    <hr>
    <br>
    {{-- Input Search --}}
    <form class="form-inline"  action="" method="GET">
        <div class="form-group">
            <label for="exampleInputName2">Start Date</label>
            <input type="text"  class="date form-control" id="inlineFormInputName" name="left_date" 
                   placeholder="{{ $left_date }}" value="{{ $left_date }}">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail2">End Date</label>
            <input class="date form-control" type="text" name="right_date" id="inlineFormInputGroupUsername" 
           placeholder="{{ $right_date }}" value="{{ $right_date }}">
        </div>
        <button type="submit" class="btn btn-crm"><span class="glyphicon glyphicon-search"></span></button>
    </form>
    
    <script type="text/javascript">
        $('.date').datepicker({  
        format: 'yyyy-mm-dd'
        });  
    </script>
<br>
    <hr>

{{-- Test table freez column data --}}
<div class="zui-wrapper">
    <div class="zui-scroller">
        <table class="zui-table table-hover  table-striped">
            <thead class="zui-ymd">  
                <tr>                   
                    <th class="zui-sticky-col5-ymd" bgcolor="black">#</th>
                    @foreach($report->date as $date)                 
                        @for($i = 0 ; $i < 18 ; $i++)
                            <td  class="qttt-head-ymd">{{ $date->day_month }}</td>
                        @endfor
                        <td bgcolor="#FEE08A"></td>
                    @endforeach
                </tr>
                <tr>                  
                    <th class="zui-sticky-col5-ymd" >Year</th>
                    @foreach($report->date as $date)                 
                        @for($i = 0 ; $i < 18 ; $i++)
                            <td>{{ $date->year }}</td>
                        @endfor
                        <td bgcolor="#FEE08A"></td>
                    @endforeach
                </tr>
                <tr>                 
                    <th class="zui-sticky-col5-ymd" >Month</th>
                    @foreach($report->date as $date)                 
                        @for($i = 0 ; $i < 18 ; $i++)
                            <td>{{ $date->month }}</td>
                        @endfor
                        <td bgcolor="#FEE08A"></td>
                    @endforeach
                </tr>
                <tr>                    
                    <th class="zui-sticky-col5-ymd" >Day</th>
                    @foreach($report->date as $date)                 
                        @for($i = 0 ; $i < 18 ; $i++)
                             <td>{{ $date->day }}</td>
                        @endfor
                        <td bgcolor="#FEE08A"> </td>
                    @endforeach
                </tr>

                <thead>
                    <tr>
                        <th class="zui-sticky-col">#</th>
                        <th class="zui-sticky-col2"  >NAME</th>
                        <th class="zui-sticky-col3" >TEAM</th>
                        <th class="zui-sticky-col4" >KPI</th>
                        <th class="zui-sticky-col5">ACTUAL</th>
                        @foreach($report->date as $date)
                            <th colspan="2">REVENUE</th>
                            <th colspan="2">CTS </th>
                            <th colspan="2">Call</th>
                            <th colspan="2">OLD CTS not update </th>
                            <th colspan="2">NEW CTS not update </th>
                            <th colspan="2">L3 </th>
                            <th colspan="2">L6</th>
                            <th colspan="2">L6 Close </th>
                            <th colspan="1">L8 </th>
                            <th colspan="1">L1 </th>
                            <td bgcolor="#FEE08A">day space</td>
                        @endforeach
                    </tr>
                    <tr class="zui-table-sub">
                        <th class="zui-sticky-col" rowspan="2">-</th>
                        <th class="zui-sticky-col2" rowspan="2" >-</th>
                        <th class="zui-sticky-col3" rowspan="2">-</th>
                        <th class="zui-sticky-col4" rowspan="2">-</th>
                        <th class="zui-sticky-col5" rowspan="2">-</th>
                        @foreach($report->date as $date)
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">Time Coef..</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">KPI</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">Actual</th>
                            <th class="col-sm-1">Actual</th>
                            <td  bgcolor="#FEE08A"></td>
                            @endforeach
                        </tr>  
                </thead>
               
                <tbody>
                @foreach($report->users as $user_index => $user)
                    <tr>
                        <td class="zui-sticky-col">{{ $user->id }}</td>
                        <td class="zui-sticky-col2">{{ $user->name }}</td> 
                        <td class="zui-sticky-col3">{{ $user->team }}</td>
                        <td class="zui-sticky-col4">0.000</td>
                        <td class="zui-sticky-col5">{{ $user->actual_revenue }}</td>
                        @foreach($user->data as $data)
                            <td>N/A</td>
                            <td>{{ $data->revenue }}</td>
                            <td>CTS KPI</td>
                            <td>CTS data</td>
                            <td>Call KPI</td>
                            <td>Call data</td>
                            <td>OLD CTS KPI</td>
                            <td>OLD CTS data</td>
                            <td>NEW CTS KPI</td>
                            <td>NEW CTS data</td>
                            <td>{{ $data->KPI_l3 }}</td>
            
                                @if($data->l3 < $data->KPI_l3) 
                                    <td class="qttt-status-danger">{{ $data->l3 }}</td>
                                    @else
                                        <td class="qttt-status-success">{{ $data->l3 }}</td>
                                @endif
                                    
                                <td>{{ $data->KPI_l6 }}</td>
                                @if($data->l6 < $data->KPI_l6) 
                                    <td class="qttt-status-danger">{{ $data->l6 }}</td>
                                    @else
                                        <td class="qttt-status-success">{{ $data->l6 }}</td>
                                @endif
                            
                            <td>{{ $data->KPI_l6close }}</td>
                            <td>{{ $data->l6close }}</td>
                            <td>{{ $data->l8 }}</td>
                            <td>{{ $data->l1 }}</td>
                            <td bgcolor="#FEE08A"></td>
                        @endforeach
                    </tr> 
                @endforeach
            </tbody> 
        </table>
    </div>
</div>

@endsection





