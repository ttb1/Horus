@extends('layout')
@section('content')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

{{-- Title Header --}}
<div class="row">
    <div class="pull-left">
        <h2> QTTT Report 2 </h2>
    </div>
</div>
 <hr>
   {{-- Input Search --}}
   <form class="form-inline"  action="" method="GET">
        <label> Search :</label>
        <div class="form-group">
           <input type="text"  class="date form-control" 
               name="start_date" placeholder="Input Start-Date" >
       </div>
       <div class="form-group">
           <input class="date form-control" type="text" name="end_date" 
                placeholder="Input End-Date" v>
       </div>
       <button type="submit" class="btn btn-crm"><span class="glyphicon glyphicon-search"></span></button>
   </form>

    <script type="text/javascript">
       $('.date').datepicker({  
       format: 'yyyy-mm-dd'
       });  
   </script>       
    <br>
    <hr>

 
    <?php $index = 0; ?>
        @foreach($reports as $key => $report) 
            <label> Result Date :</label>
                {{ $key }}<br>
              
                    <table  class="table"> 
                            {{-- <thead>  
                                    <tr>
                                        <th colspan="4" bgcolor="gray"></th><td bgcolor="black"></td>
                                         @foreach($report as $date)                 
                                            @for($i = 0 ; $i < 18 ; $i++)
                                                <td  class="qttt-head-ymd">{{ $date->day_month }}</td>
                                            @endfor
                                            <td bgcolor="#FEE08A"></td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th colspan="4" bgcolor="gray"></th>
                                        <th  class="qttt-head-ymd">YEAR</th>
                                            @foreach($report as $date)                 
                                                @for($i = 0 ; $i < 18 ; $i++)
                                                    <td>{{ $date->year }}</td>
                                                @endfor
                                                <td  bgcolor="#FEE08A"></td>
                                            @endforeach
                                    </tr>
                                    <tr>
                                        <th colspan="4" bgcolor="gray"></th>
                                        <th  class="qttt-head-ymd">MONTH</th>
                                            @foreach($report as $date)                 
                                                @for($i = 0 ; $i < 18 ; $i++)
                                                    <td>{{ $date->month }}</td>
                                                @endfor
                                                <td  bgcolor="#FEE08A"></td>
                                            @endforeach
                                    </tr>
                                    <tr> 
                                        <th colspan="4" bgcolor="gray"></th>
                                        <th  class="qttt-head-ymd">DATE</th>
                                            @foreach($report as $date)                 
                                                @for($i = 0 ; $i < 18 ; $i++)
                                                    <td>{{ $date->day }}</td>
                                                @endfor
                                                <td  bgcolor="#FEE08A"></td>
                                            @endforeach
                                     </tr>
                                 </thead>
                         --}}
                        {{-- Table Data HEADER --}}
                        <thead class="qttt-head-data">   
                           <tr>   
                                <th rowspan="2">#</th>
                                <th rowspan="2">NAME</th>
                                <th rowspan="2">TEAM</th>
                                <th rowspan="2">KPI</th>
                                <th rowspan="2">Actual</th>
                                    
                                <th colspan="2">REVENUE</th>
                                <th colspan="2">CTS </th>
                                <th colspan="2">Call</th>
                                <th colspan="2">OLD CTS not update </th>
                                <th colspan="2">NEW CTS not update </th>
                                <th colspan="2">L3 </th>
                                <th colspan="2">L6</th>
                                <th colspan="2">L6 Close </th>
                                <th  colspan="1">L8 </th>
                                <th  colspan="1">L1 </th>
                                <td bgcolor="#FEE08A">day space</td>
                           </tr>
                           <tr class="zui-table-sub">
                               
                                {{-- Table Data SUB-HEADER --}}
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">Time Coef..</th>
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
        
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">KPI</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">Actual</th>
                                    <th class="col-sm-1">Actual</th>
                                    <td bgcolor="#FEE08A">day space</td>
                                </tr>
                            </thead>
                       
                            <tbody>
                                @foreach($report as $user)
                                    {{-- Table 1 --}}
                                        @if($index == 0)
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>TEAM</td>
                                                <td>KPI</td>
                                                <td>{{ $user->actual_revenue }}</td>
                                            
                                                        <td>{{ $user->revenue }}</td>
                                                        <td>Time</td>
                                                        <td>CTS KPI</td>
                                                        <td>CTS data</td>
                                                        <td>Call KPI</td>
                                                        <td>Call data</td>
                                                        <td>OLD CTS KPI</td>
                                                        <td>OLD CTS data</td>
                                                        <td>NEW CTS KPI</td>
                                                        <td>NEW CTS data</td>
                                                        <td>{{ $user->KPI_l3 }}</td>
                                                        
                                                            @if($user->l3 <  $user->KPI_l3) 
                                                                <td class="qttt-status-danger">{{ $user->l3 }}</td>
                                                            @else
                                                                <td class="qttt-status-success">{{ $user->l3 }}</td>
                                                            @endif
                                                            
                                                            <td>{{ $user->KPI_l6 }}</td>
                                                            @if($user->l6 < $user->KPI_l6) 
                                                                <td class="qttt-status-danger">{{ $user->l6 }}</td>
                                                            @else
                                                                <td class="qttt-status-success">{{ $user->l6 }}</td>
                                                            @endif
                                                        
                                                        <td>{{ $user->KPI_l6close }}</td>
                                                        <td>{{  $user->l6close }}</td>
                                                        <td>{{  $user->l8 }}</td>
                                                        <td>{{  $user->l1 }}</td>
                                                        <td bgcolor="#FEE08A"></td>
                                                        
                                                {{-- Table n --}}
                                                @else 
                                                        <td>{{ $user->id }}</td>
                                                        <td>{{ $user->name }}</td>
                                                        <td>TEAM</td>
                                                        <td>KPI</td>
                                                        <td>{{ $user->actual_revenue }}</td>
                                                        <td>{{ $user->revenue }}</td>
                                                        <td>Time</td>
                                                        <td>CTS KPI</td>
                                                        <td>CTS data</td>
                                                        <td>Call KPI</td>
                                                        <td>Call data</td>
                                                        <td>OLD CTS KPI</td>
                                                        <td>OLD CTS data</td>
                                                        <td>NEW CTS KPI</td>
                                                        <td>NEW CTS data</td>
                                                        <td>{{ $user->KPI_l3 }}</td>
                                                            @if($user->l3 <  $user->KPI_l3) 
                                                                <td class="qttt-status-danger">{{ $user->l3 }}</td>
                                                            @else
                                                                    <td class="qttt-status-success">{{ $user->l3 }}</td>
                                                            @endif
                                                        <td>{{ $user->KPI_l6 }}</td>
                                                                @if($user->l6 < $user->KPI_l6) 
                                                                    <td class="qttt-status-danger">{{ $user->l6 }}</td>
                                                                @else
                                                                    <td class="qttt-status-success">{{ $user->l6 }}</td>
                                                                @endif
                                                        <td>{{ $user->KPI_l6close }}</td>
                                                        <td>{{  $user->l6close }}</td>
                                                        <td>{{  $user->l8 }}</td>
                                                        <td>{{  $user->l1 }}</td>
                                                        <td bgcolor="#FEE08A"></td>                                               
                                                </tr>
                                            @endif
                                    @endforeach
                                </tbody>
</table>        
<?php $index++; ?>
@endforeach
@endsection


<script>
    $(document).ready(function() {
  $('tbody').scroll(function(e) { //detect a scroll event on the tbody
  	/*
    Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
    of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.    
    */
    $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
    $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
    $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
  });
});
</script>





