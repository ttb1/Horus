@extends('layout')
@section('content')
        
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
        
        <form action="" method="GET">
            <h1>Laravel Bootstrap Datepicker</h1>
            <input class="date form-control" type="text" name="start_date">
            <input class="date form-control" type="text" name="end_date">
            <button type="submit">Search</button>
        </form>
        
        <script type="text/javascript">
            $('.date').datepicker({  
               format: 'yyyy-mm-dd'
             });  
        </script>  
        
        {{-- <h2> Report index </h2><hr>
        
        <table class="table-bordered" style="width:100%">
            <tr>
                <!-- info -->
                <th>#</th>
                <th>NAME</th>
                <th>TEAM</th>
                <th>KPI</th>
                <th>Actual</th>
                
                <!-- daily data -->
                <th>REVENUE Actual</th>
                <th>TIME CO</th>
                <th>CTS KPI</th>
                <th>CTS Actual</th>
                <th>Call KPI</th>
                <th>Call Actual</th>
                <th>OLD CTS not update KPI</th>
                <th>OLD CTS not update Actual</th>
                <th>NEW CTS not update KPI</th>
                <th>NEW CTS not update Actual</th>
                <th>L3 KPI</th>
                <th>L3 Actual</th>
                <th>L6 KPI</th>
                <th>L6 Actual</th>
                <th>L6 Close KPI</th>
                <th>L6 Close Actual</th>
                <th>L8 Actual</th>
                <th>L1 Actual</th>
            </tr>
                @foreach($users as $user) 
                <tr>
                    <td>{{ $user->index }}</td>
                    <td>{{ $user->name }}</td>
                    <td>TEAM</td>
                    <td>0,A</td>
                    <td>{{ $user->revenue }}</td>
                    <td>0.0</td>
                    <td>0</td>
                    <td>80</td>
                    <td>data</td>
                    <td>120</td>
                    <td>data</td>
                    <td>30</td>
                    <td>data</td>
                    <td>30</td>
                    <td>data</td>
                    <td>9</td>
                    <td>{{ $user->l3 }}</td>
                    <td>4</td>
                    <td>{{ $user->l6 }}</td>
                    <td>3</td>
                    <td>{{ $user->l8 }}</td>
                    <td>data</td>
                    <td>{{ $user->l1 }}</td>
                </tr>
                @endforeach
            
       </table>
@endsection --}}


<div class="row">
        <div class="pull-left"><h2> Report Index </h2></div>      
</div>  
<hr>  

{{-- Table year month day (MOCK) --}}
<table class="table table-bordered table-hover ">
       <thead>                        
              <tr class="well">
                   <th></th>
                    <th style="text-align:right;">1/1</th>
                    <th style="text-align:right;" >1/1</th>
                    <th style="text-align:right;">1/1</th>
                    <th style="text-align:right;" >1/1</th>
                    <th style="text-align:right;" >1/1</th>
                    <th style="text-align:right;"  >1/1</th>
                    <th style="text-align:right;" >1/1</th>
                    <th style="text-align:right;">1/1</th>
                    <th style="text-align:right;" >1/1</th>
                    <th style="text-align:right;"  >1/1</th>
                    <th style="text-align:right;">1/1</th>
                    <th style="text-align:right;">1/1</th>
                </tr>
            </thead>

        <tbody>
          <tr>
                <th rowspan="1">Year</th>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
                <td>2018</td>
          </tr>
          <tr>
                <th rowspan="1">Month</th>                
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>               
          </tr>

          <tr>
            <th rowspan="1">Day</th>            
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
          </tr>
        </tbody>
      </table>
<br>

{{-- Table Data --}}
       <table class="table table-hover table-striped">   
            <thead>      
                <tr class="bg-gold" >
                    <th colspan="24" style="text-align:left;">QTTT Report</th>     
                </tr>  
                    <tr>
                        <th colspan="1">INDEX</th>
                        <th colspan="1">NAME</th>
                        <th colspan="1">Team</th>
                       
                        <th colspan="2">Revenue</th>
                        <th colspan="2">Revenue</th>
                        <th colspan="2">CTS was updated status</th>
                        <th colspan="2">Calling time (Minute)</th>
                        <th colspan="2">Old CTS was not updated status</th>
                        <th colspan="2">New CTS was not updated status</th>
                        <th colspan="2">L3</th>
                        <th colspan="2">L6</th>
                        <th colspan="2">L6 Close Deal</th>
                        <th colspan="1">L8</th>
                        <th colspan="1">L1</th>
                    </tr>
               
            
                <tr >
                    <th class="col-sm-1"></th>
                    <th class="col-sm-2"></th>
                    <th class="col-sm-1"></th>
                   
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">Time Coefficient</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>

                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">KPI</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">Actual</th>
                    <th class="col-sm-1">Actual</th>
                </tr>
            </thead>
          
            <tbody>
                    @foreach($users as $user)  
                <tr>
                        <td>{{ $user->index }}</td>
                        <td>{{ $user->name }}</td>
                        <td>TEAM</td>
                       
                        <td>0,A</td>
                        <td>{{ $user->revenue }}</td>
                        <td>0.0</td>
                        <td>0</td>
                        <td>80</td>
                        <td>data</td>
                        <td>120</td>
                        <td>data</td>
                        <td>30</td>
                        <td>data</td>
                        <td>30</td>
                        <td>data</td>
                        <td>9</td>
                        <td>{{ $user->l3 }}</td>
                        <td>4</td>
                        <td>{{ $user->l6 }}</td>
                        <td>3</td>
                        <td>{{ $user->l8 }}</td>
                        <td>data</td>
                        <td>{{ $user->l1 }}</td>
                </tr>
                @endforeach
            </tbody>
</table>
@endsection

