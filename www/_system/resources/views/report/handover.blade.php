@extends('layout')
@section('content')

<!-- ************************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!-- ************************************************************************************ -->

<div class="container-fluid">


    <form action="/report/handover" method="GET">
        <div class="row">
            <div class="form-group">
                <label for="start_date" class="control-label"> From : </label>
                <input class="date form-control" name="startDate" value="{{ $handover['startDate'] }}">
            </div>
            <div class="form-group">
                <label for="start_date" class="control-label"> To : </label>
                <input class="date form-control" name="endDate" value="{{ $handover['endDate'] }}">
            </div>
        </div>
        <input type="submit" class="btn" name="submit" value="filter"/>
    </form>
    <br>
    <table class="table" name="handover_table">
        <tr>
            <th>User ID</th>
            <th>Contact ID</th>
            <th>Team</th> 
            <th>ชื่อ</th>
            <th>ชื่อ</th>
            <th>เบอร์โทร</th> 
            <th>Contact Type</th>
            <th>อีเมล์</th> 
            <th>English Skill</th>
            <th>Level</th> 
            <th>อายุ</th>
            <th>link_sb100</th>
            <th>Personality</th> 
            <th>รายละเอียดการโอน</th>
            <th>คอร์สเรียนที่ซื้อ</th> 
            <th>ค่าเรียนโปรโมชั่น</th> 
            <th>ราคาโปรโมชั่น</th> 
            <th>ราคาปกติ</th> 
            <th>เหตุผลที่ได้โปรโมชั่น</th>
            <th>ช่องทางการชำระเงิน</th> 
            <th>ของแถมให้ผู้เรียน</th>
            <th>Type</th>
        </tr>
        @foreach ( $handover_data as $handover_value)
            <tr>
                <td>{{$handover_value->user_id}}</td>
                <td>{{$handover_value->contact_id}}</td>
                <td>{{$handover_value->user_sale_group_id}}</td> 
                <td>{{$handover_value->name_th}}</td>
                <td>{{$handover_value->name_en}}</td>
                <td>{{$handover_value->phone}}</td> 
                <td>{{$handover_value->contact_type_id}}</td>
                <td>{{$handover_value->email}}</td> 
                <td>{{$handover_value->english_skill_id}}</td>
                <td>{{$handover_value->level_type_id}}</td> 
                <td>{{$handover_value->age_type_id}}</td>
                <td>{{$handover_value->link_sb100}}</td>
                <td>{{$handover_value->personality}}</td> 
                <td>{{$handover_value->payment_detail}}</td>
                <td>{{$handover_value->total_learn_day_id}}</td> 
                <td>{{$handover_value->course_price}}</td> 
                <td>{{$handover_value->promotion_price}}</td> 
                <td>{{$handover_value->normal_price}}</td> 
                <td>{{$handover_value->promotion_type_id}}</td>
                <td>{{$handover_value->payment_method_id}}</td> 
                <td>{{$handover_value->free_gift_id}}</td>
                <td>{{$handover_value->payment_type_id}}</td>
            </tr>
        @endforeach

      <tr>

      </tr>
    </table>
</div>
<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });

    function opennewwindows(contact_id) {
        window.open("{{'contactstatus'}}" + "/" + contact_id + "/"+1, "_blank",
            "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1200%,height=600");
    }
    function opennewwindows2(contact_id) {
        window.open("{{'contactstatus'}}" + "/" + contact_id + "/"+2, "_blank",
            "toolbar=yes,scrollbars=yes,resizable=yes,top=40,left=300,width=1300%,height=900");
    }
</script>


@endsection