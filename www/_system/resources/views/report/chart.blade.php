@extends('layout')
@section('content')

<h2> AreaChart Example </h2>
<div id="pop_div"></div>
<?= $lava->render('AreaChart', 'Population', 'pop_div') ?>
<hr>
<br>
<h2> BarChart Example </h2>
<div id="poll_div"></div>
<?= $lava->render('BarChart', 'Votes', 'poll_div') ?>
<hr>
<br>

<h2>CalendarChart Example</h2>
<div id="sales_div"></div>
<?= $lava->render('CalendarChart', 'Sales', 'sales_div') ?>

<hr>
<br>

<h2>ColumnChart Example</h2>
<div id="perf_div"></div>
<?= $lava->render('ColumnChart', 'Finances', 'perf_div') ?>
<hr>
<br>

<h2>ComboChart Example</h2>
<div id="finances-div"></div>
<?= $lava->render('ComboChart', 'Finances', 'finances-div') ?>
<hr>
<br>

<h2>DonutChart Example</h2>
<div id="chart-div"></div>
<?= $lava->render('DonutChart', 'IMDB', 'chart-div') ?>

<hr>
<h2>GaugeChart Example</h2>
<div id="temps_div"></div>
<?= $lava->render('GaugeChart', 'Temps', 'temps_div') ?>

<hr>
<h2>PieChart Example </h2>
<div id="chart-div1"></div>
<?= $lava->render('PieChart', 'Reasons', 'chart-div1') ?>

<hr>
<h2>LineChart Example</h2>
<div id="temps_div1"></div>
<?= $lava->render('LineChart', 'Temps1', 'temps_div1') ?>

<hr>
<h2>ScatterChart Example</h2>
<div id="chart-div2"></div>
<?= $lava->render('ScatterChart', 'AgeWeight', 'chart-div2') ?>

@endsection