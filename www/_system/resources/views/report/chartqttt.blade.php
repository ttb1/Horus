 @extends('layout') @section('content')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

{{-- Input Search --}}
<div class="panel panel-primary">
    <br>
    <form class="form-inline" action="" method="GET">
        <div class="row">
            <div class="form-group">
                <label for="View by">View by </label>
                <select name="view_by" id="viewBy" onchange="myfunction()" class="form-control">
                    @foreach( $view_type as $view_type ) @if($data->curent_view_type == $view_type)
                    <option value="{{$view_type}}" selected>{{$view_type}}</option>
                    @else
                    <option value="{{$view_type}}">{{$view_type}}</option>
                    @endif @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="start_date">Start Date</label>
                <input type="text" class="date form-control" id="inlineFormInputName" name="start_date" placeholder="start date" value="{{ $data->start_date }}">
            </div>
            <div class="form-group">
                <label for="end_date">End Date</label>
                <input class="date form-control" type="text" name="end_date" id="inlineFormInputGroupUsername" placeholder="end date" value="{{ $data->end_date }}">
            </div>
            <div class="form-group">
                <label for="level_type">Level Type:</label>
                <select name="level_type" id="level_type" class="form-control">
                    @foreach($level as $level) @if( $data->curent_level == $level->id)
                    <option value="{{ $level->id }}" selected> {{$level->name }} </option>
                    @else
                    <option value="{{ $level->id }}"> {{$level->name }} </option>
                    @endif @endforeach
                </select>
            </div>

            @if( $data->curent_view_type == 'person' )
            <div class="form-group">
                <label for="user_view">User View:</label>
                <select name="user_view" id="user_view" class="form-control">
                    @foreach($user_view as $user) @if( $data->curent_user_view->id == $user->id)
                    <option value="{{ $user->id }}" selected> {{$user->name }} </option>
                    @else
                    <option value="{{ $user->id }}"> {{$user->name }} </option>
                    @endif @endforeach
                </select>
            </div>
            @endif @if( $data->curent_view_type == 'team_compare')
            <label for="team compare"> team compare </label>
            <select name="team_left" id="team_letf" class="form-control">
                <!-- <option value="no">all</option> -->
                @foreach($teme_data as $teme_l)
                @if($curent_team_left == $teme_l->id)
                <option value=" {{$teme_l->id}} " selected>{{$teme_l->name}}</option>
                @else
                <option value=" {{$teme_l->id}} ">{{$teme_l->name}}</option>
                @endif
                @endforeach
            </select>
            <select name="team_right" id="team_right" class="form-control">
                <!-- <option value="no">all</option> -->
                @foreach($teme_data as $teme_r)
                @if($curent_team_right == $teme_r->id)
                <option value=" {{$teme_r->id}} "selected >{{$teme_r->name}}</option>
                @else
                <option value=" {{$teme_r->id}} ">{{$teme_r->name}}</option>
                @endif
                @endforeach
            </select>
            @endif

            <button type="submit" class="btn btn-default" id="viewBySumit">
                <span class="glyphicon glyphicon-search"></span>
            </button>

        </div>
    </form>
    <br>
</div>
<hr>




<div id="finances-div" style="width: 90%; height: 600px;"></div>
<?= $lava->render('ComboChart', 'Finances', 'finances-div') ?>

    <script type="text/javascript">
        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    </script>
    @endsection