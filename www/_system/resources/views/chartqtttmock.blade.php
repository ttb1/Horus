@extends('layout')
 @section('content')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<div class="content" style="position:unset; -index: -1;">
        <div class="main-header" style="margin-left: 30px;">
                <h2>Marketing Report </h2>
                <em>Mockup Layout Information Report</em>
            </div>
        <hr>
      
        
    <div class="container-fluid">
            <div class="form-row">
                    <div class="col-lg-12">
                    <div class="form-group col-sm-3"> 
                        <label for="inputState">Type</label>
                      <select id="inputState" class="form-control">
                            <option selected>Facebook</option>
                                    <option>Google</option>
                                    <option>Social</option>

                          </select>
                    </div>
                    <div class="form-group col-sm-3">
                            <label for="inputState">Chanel</label>
                            <select id="inputState" class="form-control">
                                   <option selected>FA TET 01 (OVH2)</option>
                                    <option>FA TET 03</option>
                                    <option>FA TET 04</option>
                            </select>
                          </div>
                          <div class="form-group col-sm-2">
                                <label for="inputState">Year</label>
                                <select id="inputState" class="form-control">
                                  <option selected>2018</option>
                                  <option>2017</option>
                                  <option>2016</option>
                                </select>
                                
                              </div>
                    <div class="form-group col-sm-2">
                     <label for="inputState">From</label>
                      <select id="inputState" class="form-control">    
                        <option selected>January</option>
                        <option>February</option>
                        <option>March</option>
                      </select>
                      
                    </div>
                    <div class="form-group col-sm-2">
                            <label for="inputState">To</label>
                             <select id="inputState" class="form-control">    
                               <option >January</option>
                               <option selected>February</option>
                               <option>March</option>
                             </select>
                             
                           </div>
                    <div class="form-group col-sm-1">
                            
                        <div class ="btn btn-crm form-control">
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </div>
                  </div>
                </div>
            </div>  <br>

        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="card card-stats">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big text-center icon-warning">
                                <i class="glyphicon glyphicon-flag" style="color:chocolate"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Data 1</p>1
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <hr>
                            <div class="stats">
                                <i class="fa fa-refresh">
                                </i> Updated now
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="card card-stats">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big text-center icon-warning">
                                            <i class="glyphicon glyphicon glyphicon-signal" style="color:green"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        <p>Data 2</p>$1,345</div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-calendar-o">
                                            </i> Last day</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6">
                                <div class="card card-stats">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-xs-5">
                                                <div class="icon-big text-center icon-warning">
                                                        <i class="glyphicon glyphicon-glyphicon glyphicon-warning-sign" style="color:red"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-7">
                                                        <div class="numbers">
                                                            <p>Errors</p>23</div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <hr>
                                                        <div class="stats">
                                                            <i class="fa fa-clock-o"></i> In the last hour</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-sm-6">
                                                <div class="card card-stats">
                                                    <div class="content">
                                                        <div class="row">
                                                            <div class="col-xs-5">
                                                                <div class="icon-big text-center icon-warning">
                                                                        <i class="glyphicon glyphicon-glyphicon glyphicon-heart" style="color:#337ab7"></i>
                                                                      
                                                                 </div>
                                                            </div>
                                                            <div class="col-xs-7">
                                                                <div class="numbers">
                                                                    <p>Target</p>+45</div>
                                                                </div>
                                                            </div>
                                                            <div class="footer"><hr><div class="stats">
                                                                <i class="fa fa-refresh"></i> Updated now</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                                    </div>
                                                    <div class="row">
                                                         <div class="col-md-12"><div class="card">
                                                             <div class="header">
                                                                 <h5 class="title">PAMM RADAR MARKETING</h5>
                                                                 <p class="category">24 Hours performance</p>
                                                             </div>
                                                             <div class="content">
                                                                    <table id="visit-stat-table" class="table table-sorting table-striped table-hover datatable dataTable no-footer" role="grid" aria-describedby="visit-stat-table_info">
                                                                            <thead>
                                                                                <tr role="row">
                                                                                    <th class="sorting_asc" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Browser: activate to sort column descending" style="width: 198px;">Browser</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="Operating System: activate to sort column ascending" style="width: 232px;">Operating System</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="Visits: activate to sort column ascending" style="width: 96px;">Visits</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">New Visits</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">1</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">2</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">3</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">4</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">5</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">6</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">7</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">8</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">9</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">10</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">11</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">12</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">13</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">14</th>
                                                                                    <th class="sorting" tabindex="0" aria-controls="visit-stat-table" rowspan="1" colspan="1" aria-label="New Visits: activate to sort column ascending" style="width: 151px;">15</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>    
                                                                                
                                                                            <tr role="row" class="odd">
                                                                                    <td class="sorting_1">Chrome</td>
                                                                                    <td>Macintosh</td>
                                                                                    <td>360</td>
                                                                                    <td>82.78%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                    
                                                                                </tr><tr role="row" class="even">
                                                                                    <td class="sorting_1">Chrome</td>
                                                                                    <td>Windows</td>
                                                                                    <td>582</td>
                                                                                    <td>87.24%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="odd">
                                                                                    <td class="sorting_1">Chrome</td>
                                                                                    <td>Linux</td>
                                                                                    <td>172</td>
                                                                                    <td>45.21%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="even">
                                                                                    <td class="sorting_1">Chrome</td>
                                                                                    <td>iOS</td>
                                                                                    <td>86</td>
                                                                                    <td>35.23%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="odd">
                                                                                    <td class="sorting_1">Chrome</td>
                                                                                    <td>iOS</td>
                                                                                    <td>45</td>
                                                                                    <td>23.21%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="even">
                                                                                    <td class="sorting_1">Firefox</td>
                                                                                    <td>Windows</td>
                                                                                    <td>280</td>
                                                                                    <td>63.12%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="odd">
                                                                                    <td class="sorting_1">Firefox</td>
                                                                                    <td>Android</td>
                                                                                    <td>236</td>
                                                                                    <td>58.02%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="even">
                                                                                    <td class="sorting_1">Firefox</td>
                                                                                    <td>Windows</td>
                                                                                    <td>67</td>
                                                                                    <td>27.11%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="odd">
                                                                                    <td class="sorting_1">Internet Explorer</td>
                                                                                    <td>Windows</td>
                                                                                    <td>145</td>
                                                                                    <td>45.23%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr><tr role="row" class="even">
                                                                                    <td class="sorting_1">Opera</td>
                                                                                    <td>Windows</td>
                                                                                    <td>328</td>
                                                                                    <td>67.12%</td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr></tbody>
                                                                        </table>
                                                             </div>
                                                         </div>
                                                         </div>
                                                         <div class="col-md-8"><div class="card">
                                                                <div class="header">
                                                                    <h5 class="title">Chart</h5>
                                                                    <p class="category">24 Hours performance</p>
                                                                </div>
                                                                <div class="content">
                                                                        <div class="ct-chart">
                                                                            <div class="ct-chart ">
                                                                            </div>
                                                                         </div>
                                                                    </div>
                                                            </div>
                                                            </div>
                                                            <div class="col-md-4"><div class="card">
                                                                    <div class="header">
                                                                        <h5 class="title">Users Behavior</h5>
                                                                        <p class="category">24 Hours performance</p>
                                                                    </div>
                                                                    <div class="content">
                                                                            <div class="ct-chart">
                                                                                <div class="ct-chart ">
                                                                                </div>
                                                                             </div>
                                                                        </div>
                                                                </div>
                                                                </div>
                                                    </div>
    </div>
</div>

                                                                             
@endsection

<style>
 /* --------------    */
    .main-header h2 {
    display: inline-block;
    vertical-align: middle;
    border-right: 1px solid #ccc;
    margin: 0;
    padding-right: 10px;
    margin-right: 10px;
    
}
.page-content {
    margin-top: 0;
    margin-bottom: 0;
    margin: 0;
    padding: 8px 50px 24px;
    padding-top: 5rem;
     /* ADD CHART QTTT MOCK */
     background-color: #c7c7c717;  
}
 /* --------------    */
    .card {
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);
          box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), 0 0 0 1px rgba(63, 63, 68, 0.1);
  background-color: #f7f7f733;
  margin-bottom: 30px; }
  .card .image {
    width: 100%;
    overflow: hidden;
    height: 260px;
    border-radius: 4px 4px 0 0;
    position: relative;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d; }
    .card .image img {
      width: 100%; }
  .card .filter {
    position: absolute;
    z-index: 2;
    background-color: rgba(0, 0, 0, 0.68);
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    text-align: center;
    opacity: 0;
    filter: alpha(opacity=0); }
    .card .filter .btn {
      position: relative;
      top: 50%;
      -webkit-transform: translateY(-50%);
      -ms-transform: translateY(-50%);
      transform: translateY(-50%); }
  .card:hover .filter {
    opacity: 1;
    filter: alpha(opacity=100); }
  .card .btn-hover {
    opacity: 0;
    filter: alpha(opacity=0); }
  .card:hover .btn-hover {
    opacity: 1;
    filter: alpha(opacity=100); }
  .card .content {
    padding: 15px 15px 10px 15px;
    background-color:white; }
    
  .card .header {
    /* padding: 0 10px; */
    /* height: 35px; */
    border-bottom-width: 1px;
    border-bottom-style: solid;
    border-bottom-color: lightgray;
    background-color: #eeeeee;  
    padding: 5px 15px 5px; 
    font-size: 13px;
  }
  .card .category,
  .card label {
    font-size: 12px;
    font-weight: 400;
    color: #9A9A9A;
    margin-bottom: 0px; }
    .card .category i,
    .card label i {
      font-size: 16px; }
  .card label {
    font-size: 12px;
    margin-bottom: 5px;
    text-transform: uppercase; }
  .card .title {
    margin: 0;
    color: #333333;
    font-weight: 300; }
  .card .avatar {
    width: 30px;
    height: 30px;
    overflow: hidden;
    border-radius: 50%;
    margin-right: 5px; }
  .card .description {
    font-size: 14px;
    color: #333; }
  .card .footer {
    padding: 0;
    background-color: transparent;
    line-height: 30px; }
    .card .footer .legend {
      padding: 5px 0; }
    .card .footer hr {
      margin-top: 5px;
      margin-bottom: 5px; }
  .card .stats {
    color: #a9a9a9; }
  .card .footer div {
    display: inline-block; }
  .card .author {
    font-size: 12px;
    font-weight: 600;
    text-transform: uppercase; }
  .card .author i {
    font-size: 14px; }
  .card h6 {
    font-size: 12px;
    margin: 0; }
  .card.card-separator:after {
    height: 100%;
    right: -15px;
    top: 0;
    width: 1px;
    background-color: #DDDDDD;
    content: "";
    position: absolute; }
  .card .ct-chart {
    margin: 30px 0 30px;
    height: 245px; }
  .card .table tbody td:first-child,
  .card .table thead th:first-child {
    padding-left: 15px; }
  .card .table tbody td:last-child,
  .card .table thead th:last-child {
    padding-right: 15px; }
  .card .alert {
    border-radius: 4px;
    position: relative; }
    .card .alert.alert-with-icon {
      padding-left: 65px; }

.card-user .image {
  height: 110px; }

.card-user .image-plain {
  height: 0;
  margin-top: 110px; }

.card-user .author {
  text-align: center;
  text-transform: none;
  margin-top: -70px; }

.card-user .avatar {
  width: 124px;
  height: 124px;
  border: 5px solid #FFFFFF;
  position: relative;
  margin-bottom: 15px; }
  .card-user .avatar.border-gray {
    border-color: #EEEEEE; }

.card-user .title {
  line-height: 24px; }

.card-user .content {
  min-height: 240px; }

.card-user .footer,
.card-price .footer {
  padding: 5px 15px 10px; }

.card-user hr,
.card-price hr {
  margin: 5px 15px; }

.card-plain {
  background-color: transparent;
  -webkit-box-shadow: none;
          box-shadow: none;
  border-radius: 0; }
  .card-plain .image {
    border-radius: 4px; }

.card-stats .icon-big {
  font-size: 3em;
  min-height: 64px; }
  .card-stats .icon-big i {
    font-weight: bold;
    line-height: 59px; }

.card-stats .numbers {
  font-size: 2em;
  text-align: right; }
  .card-stats .numbers p {
    margin: 0; }


</style>