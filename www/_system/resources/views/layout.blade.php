<!doctype html>
<html>
{{-- @include('includes.head')  --}}
<head>
      <!--Header Area -->     
        @include('includes.header')    
</head>


@yield('sidebar')
<body style="min-height: 75rem; padding-top: 5.2rem; background-color:rgba(199, 198, 198, 0.12);">
 <!-- Header --> 
  <nav class="navbar  navbar-light navbar-gold">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                     <span class="icon-bar"></span>                        
                 </button>
                <a class="navbar-brand" href="#"> TOPICA CRM </a>   
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <strong>{{ auth()->user()->username }}</strong>
                            <span class="glyphicon glyphicon-cog"></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">
                                <span class="glyphicon glyphicon-user"></span> Change Password</a>
                            </li>
                            
                            <li>  
                                <a href="#">
                                    <form action="{{ route('auth_logout') }}" method="GET" >
                                        <span class="glyphicon glyphicon-log-out"></span>
                                        <button type="submit" class="btn btn-link">Logout</button>
                                    </form>
                                </a>
                        
                            </li>
                        </ul>
                    </li>  
                </ul>
            </div>
        </div>
    </nav>


   <div class="container-fluid ">
       <div class="row content">

         <!--  Sidebar -->
            <div class="sidenav">                
                <?php 
                $menu_data = json_decode($menu_data);
                $url = url()->full();
                $manuArray = explode("/", $url);
                ?>
                
                @if(!empty($menu_data) > 0)
                  <!---Main Manu -->
                    @foreach($menu_data->main as $main_menu)            
                    <a  data-toggle="collapse" data-target="#<?php echo $main_menu->id ?>" href="#">
                    <strong> {{ $main_menu->name }}</strong>
                    <span class="glyphicon glyphicon-menu-down pull-right manu-down"  aria-hidden="true"></span>            
                    </a>
                      <!---SubManu -->
                         <!-- ################################# Left menu - current page   ############################################ -->
                                    <?php
                                        $nameArr = array();
                                        $rootArr = array();
                                    foreach ($main_menu->sub as $index => $sub_menu) {
                                    
                                        array_push( $nameArr,$sub_menu->name);
                                        array_push( $rootArr,$sub_menu->root_id);
                                        
                                    }
                                    $validaateNamrArr = in_array( ucfirst($manuArray[3]),$nameArr );
                                    $validaateRootArr = in_array( $main_menu->id ,$rootArr );
                                    //dd($validaateRootArr);                      
                                    ?>
                         <!-- ##################################  Left menu - current page   #################################### -->
                      
                      @if($validaateRootArr && $validaateNamrArr) 
                        <div id="{{ $main_menu->id }}" class="">
                      @else
                        <div id="{{ $main_menu->id }}" class="collapse">
                      @endif
                        <!-- remove main link (act only as a folder name)
                        <a href="/{{ $main_menu->url }}"> 
                            <span class="glyphicon glyphicon-triangle-right" style="font-size: 10px; margin-left:20px;" aria-hidden="true"></span> {{ $main_menu->name }}
                        </a>
                        -->
                          
                        @foreach($main_menu->sub as $index => $sub_menu)  

                            @if($sub_menu->name == ucfirst($manuArray[3]))
                            
                               <a  href="/{{ $sub_menu->url }}" class="active-page"> <span class="glyphicon glyphicon-triangle-right" style="font-size: 10px; margin-left:20px;" aria-hidden="true"></span> {{ $sub_menu->name }}</a>                
                               
                            @else
                               
                               <a  href="/{{ $sub_menu->url }}"> <span class="glyphicon glyphicon-triangle-right" style="font-size: 10px; margin-left:20px;" aria-hidden="true"></span> {{ $sub_menu->name }}</a>                
                    
                            @endif
                        @endforeach
                    </div>
                    @endforeach                    
                @else
                    <!--<script>window.location = "/";</script>-->
                    NOT AUTH -> redirect to ROOT
                @endif
                
            </div>
         
           <!-- Main Content Area-->
           <div class="main-content">
                     <!-- Page Breadcrumb Area -->
                    <div class="breadcrumbs" id="breadcrumbs">
                        <script type="text/javascript">
                            try {
                                ce.settings.check('breadcrumbs', 'fixed');
                                } catch (e) { }
                        </script>
                          <ul class="breadcrumb">
                                <li>
                                    <span class="glyphicon glyphicon-home"></span> 
                                            &ensp; <a href="/">Home</a>
                                </li>
                                    <?php 
                                    $url = url()->full();

                                    $navArray = explode("/", $url);
                                    $link_url = '';
                                        for ($i = 3; $i < count($navArray); $i++) {
                                        
                                                    
                                                //remove numeric ex, edit / 1
                                            if (!is_numeric($navArray[$i])) {

                                                $link_url .=  '/'. $navArray[$i];
                                                    
                                            }
                                        ?>                
                                            <li>            
                                                <a href="{{ $link_url }}">{{ ucfirst($navArray[$i])}}</a>
                                            </li>
                                                        
                                     <?php }  ?>
                             </ul>
                    </div>
                    @include('includes.head') 
                    
                    <div class="page-content">    
                            <br>
                        @yield('content')  
                        
                    </div>
                 </div>
           </div>

        </div>
    </div>
</body>
</html>