@extends('layout')
@section('content')

<form action="{{ route('user_sale_group') }}" method="POST">
    <select name="selectGroup1" onchange="myFunction()" >
        <option value="0" @if (!strcmp($selectGroup1 , 0)) selected="true"   @endif >
            Not own
        </option>
        @foreach ( $sale_groups as $sale_group)
            <option value="{{$sale_group->id}}" @if (!strcmp($selectGroup1 , $sale_group->id)) selected="true"   @endif >
                {{$sale_group->name}}
            </option>
        @endforeach
    </select>
    <br>
    <select name="multiselectgroup1[]" multiple>
        @foreach ( $user_in_group_1 as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
        @endforeach  
    </select>
    <br>
    <input type="submit" name="sumbit" value="Up" >
    <input type="submit" name="sumbit" value="Down" >
    <br>
    <select name="selectGroup2"  onchange="myFunction()" >
        <option value="0" @if (!strcmp($selectGroup2 , 0)) selected="true"   @endif >
            Not own
        </option>
        @foreach ( $sale_groups as $sale_group)
            <option value="{{$sale_group->id}}" @if (!strcmp($selectGroup2 , $sale_group->id)) selected="true"   @endif >
                {{$sale_group->name}}
            </option>
        @endforeach
    </select>
    <br>
    <select name="multiselectgroup2[]" multiple>
        @foreach ( $user_in_group_2 as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>
        @endforeach  
    </select>
    <br>
    <input type="hidden" value="{{ csrf_token() }}" name="_token">
    <input type="submit" name="sumbit" value="update_group" id="update_group" style="display:none;">
    
</form>

<script type="text/javascript">
function myFunction() {
   document.getElementById("update_group").click();
}
</script>
@endsection