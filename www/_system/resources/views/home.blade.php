<!doctype html>
<html>  
     @include('includes.head')
<head>
    <title>Login Page - CRM Administration</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    
    <!--========================== Style Sheet =============================-->

    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{asset('css/Style.css')}}">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!--========================== Style Sheet =============================-->
     
</head>

<body>
    <form action="{{ route('auth_login') }}" method="POST" autocomplete="off" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">        
            <div class="limiter">
		<div class="container-login100">
                    <div class="wrap-login100 p-l-35 p-r-35 p-t-45 p-b-30">
                        <form class="login100-form validate-form">
                            <span class="login100-form-head p-b-25">    
                                TOPICA CRM TL100
                            </span>
                            <span class="login100-form-title p-b-15">Login Information</span>
                                <div class="wrap-input100 validate-input" >
                                    <input class="input100" type="text" name="username" placeholder="Username">
                                    <span class="focus-input100-1"></span>
                                    <span class="focus-input100-2"></span>
				</div>

				<div class="wrap-input100 rs1 validate-input" >
                                    <input class="input100 p-b-25" type="password" name="password" placeholder="Password">
					<span class="focus-input100-1"></span>
					<span class="focus-input100-2"></span>
				</div>

				<div class="container-login100-form-btn p-t-20" >
                                    <button class="login100-form-btn" type="submit">
                                        Sign In
                                    </button>     
                                </div>  
                          
                             <div class="text-center p-t-30 p-b-4">
				<span class="txt1">
                                    <footer>
                                        @include('includes.footer')      
                                    </footer>	
				</span>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
    </body>
</html>      


<style>
    .alert {
     padding: 0; 
     margin-bottom:0; 
     border: 0 ; 
     border-radius: 0; 
}
</style>
