<html>
@extends('layout')

<body>
    @section('content')
    <!--- Content Text Area -->
    {{--
    <input type="button" class="btn btn-link lg" value="Back" onClick="history.go(-1);"> --}}
    <form action="{{ route('contact_create') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="col-lg-6">
            <h2>Create Contact</h2>
            <hr>
            <div class="form-group">
                <label class="control-label ">Firstname</label>
                <input class="form-control" name="firstname" type="text" placeholder="Firstname ...">
            </div>

            <div class="form-group">
                <label class="control-label ">Lastname</label>
                <input class="form-control" name="lastname" type="text" placeholder="Lastname ...">
            </div>
            {{--
            <div class="form-group">
                <label class="control-label ">Email</label>
                <input class="form-control" name="email" type="text" placeholder="Email ...">
            </div> --}}
            <div class="form-group">
                <label class="control-label ">Email 1</label>
                <input class="form-control" name="email[]" type="text" placeholder="Email ...">
            </div>
            <div class="form-group">
                <label class="control-label ">Email 2</label>
                <input class="form-control" name="email[]" type="text" placeholder="Email ...">
            </div>


            <div class="form-group">
                <label class="control-label ">Phone 1</label>
                <input class="form-control" name="phone[]" type="text" placeholder="Phone ...">
            </div>
            <div class="form-group">
                <label class="control-label ">Phone 2</label>
                <input class="form-control" name="phone[]" type="text" placeholder="Phone ...">
            </div>
            <div class="form-group">
                <label class="control-label ">Phone 3</label>
                <input class="form-control" name="phone[]" type="text" placeholder="Phone ...">
            </div>


            <div class="form-group">
                <label class="control-label ">Channel</label>
                <select class="form-control" name="channel_id">
                    <option value="o">select one</option>
                    @foreach($channels as $channel)
                    <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label ">Contact Source</label>
                <select class="form-control" name="source_id">
                    <option value="o">select one</option>
                    @foreach($sources as $source)
                    <option value="{{ $source->id }}">{{ $source->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary  pull-right">SAVE</button>
            </div>
        </div>
    </form>

    @endsection

</body>

</html>