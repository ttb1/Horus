<html>
@extends('layout')

<body>
    @section('content') {{--
    <input type="button" class="btn btn-link lg" value="Back" onClick="history.go(-1);"> --}}
    <form action="{{ route('contact_update') }}" method="POST">
        <input type="hidden" name="contact_id" value="{{ $contact->id }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!--- Content Text Area -->
        <div class="col-lg-6">
            <h2> Edit Contact</h2>
            <hr>
            <div class="form-group">
                <label class="control-label ">Firstname</label>
                <input class="form-control" name="firstname" type="text" value="{{ $contact->firstname }}" placeholder="Firstname ...">
            </div>

            <div class="form-group">
                <label class="control-label ">Lastname</label>
                <input class="form-control" name="lastname" type="text" value="{{ $contact->lastname }}" placeholder="Lastname ...">
            </div>

            <div class="form-group">
                <label class="control-label ">Email 1</label>
                @if(!empty($contact->email[0]) )
                <input class="form-control" name="email[]" type="text" value="{{ $contact->email[0]->email }}" placeholder="Email ..."> @else
                <input class="form-control" name="email[]" type="text" value="" placeholder="Email ..."> @endif
            </div>

            <div class="form-group">
                <label class="control-label ">Email 2</label>
                @if(!empty($contact->email[1]) )
                <input class="form-control" name="email[]" type="text" value="{{ $contact->email[1]->email }}" placeholder="Email ..."> @else
                <input class="form-control" name="email[]" type="text" value="" placeholder="Email ..."> @endif
            </div>

            {{-- <div class="form-group">
                <label class="control-label ">Email 3</label>
                @if(!empty($contact->email[2]) )
                <input class="form-control" name="email[]" type="text" value="{{ $contact->email[2]->email }}" placeholder="Email ..."> @else
                <input class="form-control" name="email[]" type="text" value="" placeholder="Email ..."> @endif
            </div> --}}

            <div class="form-group">
                <label class="control-label ">Phone</label>
                @if(!empty($contact->phone[0]) )
                <input class="form-control" name="phone[]" type="text" value="{{ $contact->phone[0]->phone_number }}" placeholder="Phone ..."> @else
                <input class="form-control" name="phone[]" type="text" value="" placeholder="Phone ..."> @endif
            </div>
            <div class="form-group">
                <label class="control-label ">Phone</label>
                @if(!empty($contact->phone[1]) )
                <input class="form-control" name="phone[]" type="text" value="{{ $contact->phone[1]->phone_number }}" placeholder="Phone ..."> @else
                <input class="form-control" name="phone[]" type="text" value="" placeholder="Phone ..."> @endif
            </div>
            <div class="form-group">
                <label class="control-label ">Phone</label>
                @if(!empty($contact->phone[2]) )
                <input class="form-control" name="phone[]" type="text" value="{{ $contact->phone[2]->phone_number }}" placeholder="Phone ..."> @else
                <input class="form-control" name="phone[]" type="text" value="" placeholder="Phone ..."> @endif
            </div>

            <div class="form-group">
                <label class="control-label ">Channel</label>
                <select class="form-control" name="channel_id">
                    @foreach($channels as $channel)
                    <option @if ($channel->id == $contact->channel_id) selected="selected" @endif value="{{ $channel->id }}">{{ $channel->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="control-label ">Contact Source</label>
                <select class="form-control" name="source_id">
                    @foreach($sources as $source)
                    <option @if ($source->id == $contact->source_id) selected="selected" @endif value="{{ $source->id }}">{{ $source->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary  pull-right">SAVE</button>
            </div>

        </div>
    </form>
    @endsection
</body>

</html>