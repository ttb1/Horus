
    @extends('layout')   
    @section('content')

    <!--- Content Text Area -->  
    <div class="row">
        <div class="pull-left">
            <h2 >Contact Management</h2>  
        </div> 
    </div>
    <hr>

    <nav aria-label="Page navigation example">
        <ul class="pagination"> 
            <li class="page-item"><a class="page-link" href="{{route('contact_add')}}">Create</a></li>
            <li class="page-item"><a class="page-link" href="{{ route('contact_transfer') }}">Tranfer</a></li>
        </ul>
    </nav>

    <div class="form-group ">
        <form class="form-inline" action="{{ route('contact_import') }}" method="POST" enctype="multipart/form-data">
            <div class="panel panel-default col-sm-4" style="background-color:whitesmoke; position:unset;">
                <div class="panel-heading"><strong>Import File</strong></div>
                    <div class="panel-body">
                        <input type="hidden"name="_token" value="{{ csrf_token()}}" class="sr-only">
                        <input  type="file"  id="import" name="import">
                    </div>
                    <div class="panel-body">
                        <button type="submit" class="btn btn-crm pull-right"><strong>Import</strong></button> 
                    </div>
            </div>
        </form>
    </div>


    <table class="table-h">   
        <thead>
            <tr class="bg-gold" >
                <th colspan="9" style="text-align:left;">Contact Management</th>   
            </tr>
            <tr>
                <th class="col-sm-1">#</th>
                    <th class="col-sm-3"> Firstname</th>
                    <th class="col-sm-3"> Lastname</th>
                    <th class="col-sm-1">Phone</th>
                    <th class="col-sm-3">Email</th>
                    <th class="col-sm-1">Channel Id</th>
                    <th class="col-sm-1">Source Id</th>
                    <th class="col-sm-1">Edit</th>
                    <th class="col-sm-1">Delete</th>
                </tr>
            </thead>
        </table>

<table class=" table table-hover table-striped">          
         <tbody class="tablescroll">
                    @foreach($contacts as $contact)
                        <tr>
                            <?php
                                $phone = '';
                                if(count($contact->phone) > 0) {
                                    $phone = $contact->phone->first()->phone_number;
                                }
                                
                                $email = '';
                                if(count($contact->email) > 0) {
                                    $email = $contact->email->first()->email;
                                }
                            ?>
                            <td class="col-sm-1">{{$contact->id}}</td>
                            <td class="col-sm-3">{{$contact->firstname}}</td>
                            <td class="col-sm-3">{{$contact->lastname}}</td>
                            <td class="col-sm-1">{{$phone}}</td>
                            <td class="col-sm-2">{{$email}}</td>
                            <td class="col-sm-1">{{$contact->channel_id}}</td>
                            <td class="col-sm-1">{{$contact->source_id}}</td>
                            <td class="col-sm-1"> 
                                <form action="{{ route('contact_edit', $contact->id) }}" method="GET" >                    
                                    <button type="submit" class="btn btn-link"><img src="/img/edit-16.png "></button> 
                                </form>
                            </td>
                            <td class="col-sm-1"> 
                                <form action="{{ route('contact_remove') }}" method="POST" id="contact_remove">
                                    <input type="hidden" name="contact_id" value="{{ $contact->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit"class="btn btn-link" onclick="return confirm('Are you sure?')">
                                        <span class="glyphicon glyphicon-trash" style="color:#dd5a43;"></span>
                                    </button>  
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
</table>
</div>

       <!-- <div>
            @foreach($contacts as $contact)
                {{ $contact->id." ~ ".$contact->firstname." ".$contact->lastname." ~ ".$contact->telephone
                            ." ~ ".$contact->email."  ~~~~ ".$contact->channel_id." / ".$contact->source_id
                }}
                <form action="{{ route('contact_edit', $contact->id) }}" method="GET" >                    
                   <button type="submit">Edit</button>
                </form>
                <form action="{{ route('contact_remove') }}" method="POST" id="contact_remove">
                    <input type="hidden" name="contact_id" value="{{ $contact->id }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="link-button">REMOVE</button>                    
                </form>
                <br>
            @endforeach 
        </div> -->
       
        @endsection
           


