
@extends('layout')       
    @section('content')
        <!--- Content Text Area -->
    
    <h2>Transfer Contact</h2><hr>
    <div class="col-lg-6"> 
            <div class ="form-group">                
                <form action="" method="GET">
                    <div class="panel panel-default" style="background-color:whitesmoke;">
                        <div class="panel-heading"><strong> Contact Type</strong></div>
                        <div class="panel-body">
                            <select class="form-control" id="contact_type" name="contact_type">
                                @foreach($contact_types as $index => $contact_type)                
                                <option
                                        @if($current_contact_id == $index)
                                            selected="selected"
                                        @endif
                                        value="{{ $index }}">{{ $contact_type }}</option>                                
                                @endforeach
                            </select><br>
                            <button type="submit" class="btn btn-crm pull-right" > 
                                    <strong>Search</strong></button>  
                        </div>
                    </div>
                </form>
            </div>

            <div class ="form-group">     
                <form action="{{ route('contact_transfer_to_user') }}" method="POST">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color:#dbac69;"><strong>User List</strong></div>
                            <div class="panel-body">
                                <select class="form-control" name="user_id">
                                    @foreach($users as $user)                
                                        <option value="{{ $user->id }}">{{ $user->firstname.' '. $user->lastname }}</option>
                                    @endforeach
                                </select><hr>
                                <label class="control-label">Contact list</label><br>
                            </div>
                        
                            <div class="panel-body-md">  
                           
                                
                                @foreach($contacts as $contact)                
                                    <input type="checkbox" name="contacts[]" value="{{ $contact->id }}" > {{ $contact->firstname }}<br>
                                @endforeach
                            </div>
                        </div>
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <button type="submit" class="btn btn-primary pull-right">Transfer</button>     
                </form>
            </div> 
            <br>
            <br>
</div>
@endsection
