<html>
@extends('layout')

    <body>
        
        @section('content')
        <!--- Content Text Area -->
        <h1>Call History</h1><hr>
        
        <form action="" method="GET">
            <label>User</label>
            <select name="user_id">
                @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
            <br>

            <label>Contact</label>
            <select name="contact_id">
                @foreach($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->firstname." ".$contact->lastname }}</option>
                @endforeach
            </select>
            <br>
        </form>
        
        @foreach($call_histories as $call_history)
            <?php $contact_name = $call_history->contact->firstname." ".$call_history->contact->lastname?>
            {{ "user: ".$call_history->user->name. " contact: ".$contact_name }}<br>
        @endforeach
        
        
        
        <form action="{{ route('call_history_add') }}" method="GET" >
            <button type="submit">Add</button>
        </form>
        @endsection
    </body>
</html>

