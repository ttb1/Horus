<html>
@extends('layout')

    <body>

        @section('content')
        <!--- Content Text Area -->
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" charset="utf-8">
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        function setCallHistoryLevel(level_id) {            
            $.ajax({
               type:'POST',
               url:'/ajaxPOST',
               data:{
                   'level_id' : level_id
               },        
               success:function(call_level){
                    $('#call_quality_level_id').empty();
                    for(var i = 0; i < call_level.call_quality_level.length; i++) {
                        //TODO json string -> collection                        
                        var call_quality = call_level.call_quality_level[i]['call_quality'];                        
                        $('#call_quality_level_id').append($('<option>').text(call_quality.name).attr('value', call_quality.id));
                    }
               }
            });            
        }
        
        /*
        function setCallHistoryLevel(level_id) {            
            $.ajax({
               type:'GET',
               url:'/ajaxGET',
               data:{
                   'level_id' : level_id
               },        
               success:function(data){
                  console.log(data);
                  document.getElementById("response_txt").innerHTML = data;
               }
            });            
        }
        */
        </script>
        <p>Suggestions: <span id="response_txt"></span></p> 
        
        <h1>Add Call History</h1><hr>
        <form action="{{ route('call_history_create') }}" method="POST">
            <label>User</label>
            <select name="user_id">
                @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
            <br>
            
            <label>Contact</label>
            <select name="contact_id">
                @foreach($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->firstname." ".$contact->lastname }}</option>
                @endforeach
            </select>
            <br>
            
            <label>Call Status</label>
            <select name="status_call_id">
                @foreach($status_calls as $status_call)
                <option value="{{ $status_call->id }}">{{ $status_call->name }}</option>
                @endforeach
            </select>
            <input name="call_status_comment"></input>
            <br>
            
            <label>Service Status</label>
            <select name="status_service_id">
                @foreach($status_services as $status_service)
                <option value="{{ $status_service->id }}">{{ $status_service->name }}</option>
                @endforeach
            </select>
            <input name="service_status_comment"></input>
            <br>
            
            <label>Level</label>
            <select id="level" name="level_id" onchange="setCallHistoryLevel(this.value)">
                @foreach($levels as $level)
                <option @if( $level->id  == $level_id)
                            selected="selected"
                        @endif
                    value="{{ $level->id }}">{{ $level->name }}</option>
                @endforeach
            </select>
            <br>
            
            <label>Call Quality (By Level)</label>
            <select id="call_quality_level_id" name="call_quality_level_id">
                @foreach($call_level->call_quality_level as $call_quality_level) {
                    <option value="{{ $call_quality_level->call_quality->id }}">{{ $call_quality_level->call_quality->name }}</option>
                @endforeach
            </select>
            <input name="call_quality_text"></input>
            <br>
            
            <label>Course (for L6)</label>
            <select name="course_id">
                @foreach($courses as $course)
                <option value="{{ $course->id }}">{{ $course->name }}</option>
                @endforeach
            </select>
            <br>
            
            <button type="submit">Add</button>
            <input type="hidden"name="_token" value="{{ csrf_token()}}" class="sr-only">
            
        </form>
        
        @endsection
    </body>
</html>

