<div class="panel panel-primary">
    <div class="panel-heading" style="font-size: 16px;">
       <strong> Update Contact Status </strong>
    </div>
    <div class="panel-body">
            <div class="row">
                <div class="col-md-2"><strong>Time call back</strong></div>
                <div class="col-md-4">
                    <input type="datetime-local" name="update_callback_datetime" class="form-control" value={{ $datetime_now->format('Y-m-d\TH:i T') }}>
                </div>
                <div class="col-md-2"><strong>Call quility</strong></div>
                <div class="col-md-4">
                    <select class="form-control" name="update_call_quility_select">
                        <option value="1">A1</option>
                        <option value="2">A2</option>
                        <option value="3">A3</option>
                        <option value="4">A4</option>
                        <option value="5">A5</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><strong>Status call</strong></div>
                <div class="col-md-4">
                    <select class="form-control" name="status_call">

                        @foreach ( $update_contact_status->status_call as $status_list )
                            <option value="1">{{$status_list->name}}</option>                            
                        @endforeach
                        {{-- <option value="2">B2</option>
                        <option value="3">B3</option>
                        <option value="4">B4</option>
                        <option value="5">B5</option> --}}
                    </select>
                </div>
                <div class="col-md-2"><strong>Service status</strong></div>
                <div class="col-md-4">
                    <select class="form-control" name="update_service_status_select">
                        <option value="1">C1</option>
                        <option value="2">C2</option>
                        <option value="3">C3</option>
                        <option value="4">C4</option>
                        <option value="5">C5</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2"><strong>Comment</strong></div>
                <div class="col-md-10">
                    <textarea class="form-control" rows="3" name="comment"></textarea>
                </div>
            </div>
         
            
        
    </div>
</div>


