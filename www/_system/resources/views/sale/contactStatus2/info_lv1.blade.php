<!-- ************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

<?php 
    $contact = $info_lv1->contact;
    if(count($contact->email) == 0) {
        $contact->email = [''];
    }
    if(count($contact->phone) == 0) {
        $contact->phone = [''];
    }
?>
<div class="panel panel-primary">
    <div class="panel-heading" style="font-size: 16px;">
            <strong>{{ $info_lv1->blade_name }}</strong>
           
        {{-- <strong>{{ $info_lv1->blade_name }} </strong> --}}
    </div>
    <div class="panel-body">
        <form action="{{ route('l1update')}}" class="form form-inline" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-xs-1">
                    <label for="Fullname" class="label-control">Fullname</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="fullname" id="fullname" value=" {{ $info_lv1->contact->firstname }} {{ $info_lv1->contact->lastname }}">
                </div>
                <div class="col-xs-1">
                    <label for="Gender">Gender</label>
                </div>
                <div class="col-xs-3">
                    <!-- <input type="text" name="gender" id="gender" readonly> -->
                    <select name="gender" id="gender" class="form-control">
                        <!-- <option value="select">select</option> -->
                        <option value="man">man</option>
                        <option value="female">female</option>
                        <option value="other">other</option>
                    </select>
                </div>
                <div class="col-xs-1">
                    <label for="Birth">Birth</label>
                </div>
                <div class="col-xs-3">
                    <input type="date" class="form-control" name="birth" id="birth">
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="Tel">Tel.</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" onclick="callAsterisk('{{ $contact_id }}' )" style="background:#bedbf5;" type="button"></button>
                        </span>
                        <input type="text" class="form-control" name="telephone[]" id="telephone" value="{{ $info_lv1->contact->phone[0] }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#bedbf5;" type="button"></button>
                        </span>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Tel2">Tel.2</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#bedbf5;" type="button"></button>
                        </span>
                        <input type="text" class="form-control" name="telephone[]" id="tel2" value="891249458">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#bedbf5;" type="button"></button>
                        </span>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Tel3">Tel.3</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#bedbf5;" type="button"></button>
                        </span>
                        <input type="text" class="form-control" name="telephone[]" id="tel3" value="891249458">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#bedbf5;" type="button"></button>
                        </span>

                    </div>
                </div>
            </div>

            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="Email">Email.</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-envelope" id="email_addon"></span>
                        <input type="text" class="form-control " aria-describedby="email_addon" name="email[]" id="email" value="{{ $info_lv1->contact->email[0] }}"
                            readonly>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Email 2">Email 2</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-envelope" id="email2_addon"></span>
                        <input type="text" class="form-control" aria-describedby="email2_addon" name="email[]" id="email">
                    </div>
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="contact_id ">ContactID.</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="contact_id" id="contact_id" value="{{ $info_lv1->contact->contact_id }}" readonly>
                </div>
                <div class="col-xs-1">
                    <label for="line_id ">LineID.</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="line_id" id="line_id" value="testline">
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="Contact_quality">Contact quality.</label>
                </div>
                <div class="col-xs-3">
                    <textarea class="form-control" id="Contact_quality" rows="2" cols=" 100%" name="contact_quality"></textarea>
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="tester_quality">Tester quality.</label>
                </div>
                <div class="col-xs-3">
                    <textarea class="form-control" id="tester_quality" name="tester_quality" rows="2" cols=" 100%"></textarea>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-1"> </div>
                <button class="btn-lg btn-link" type="button" onclick=" window.open('http://google.com','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">

                    <span class="glyphicon glyphicon-envelope"></span> Send introduction email.
                </button>

                <button class="pull-right btn btn-success" style="margin-right: 5%;"><strong>Save L1</strong></button>

            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
<script>

function callAsterisk(call_id) {

    $.ajax({
            method: "GET",
            url: "{{ url('call/asterisk_call')}}",
            data: {
                call_id : call_id ,
            },
            statusCode: {
                500: function () {
                    alert("error /500");
                 
                },
                404: function () {
                    alert("page not found /404");
                   
                }
            }
        }).done(function (data) {
            console.log(data);
        });
}
</script>