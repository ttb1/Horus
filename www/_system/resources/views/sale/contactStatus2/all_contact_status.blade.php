<div class="panel panel-default">
    <div class="panel-heading">
       <strong>All contact status ( Total phone call: {{ $all_contact_status->count() }} ,  Total calling time :  {{ $all_contact_status->sum('duration') }} )</strong> 
    </div>
    <div class="panel-body" >
            <table  class="table table-striped">
                <tr>
                    <th></th>
                    <th>เวลาโทร</th>
                    <th>สถานะ</th>
                    <th>สถานะการโทร</th>
                    <th>เลเวล</th>
                    <th>เวลาโทรกลับ</th>
                    <th>เนื้อหา</th>
                    <th>บันทึก</th>
                </tr>
                @foreach ($all_contact_status as $key => $user)
                    <tr>
                        <td></td>
                        <td>{{ $user['start_time'] }}</td>
                        <td>{{ $user['status_call_id'] }}</td>
                        <td>{{ $user['status_service_id'] }}</td>
                        <td>{{ $user['level_id'] }}</td>
                        <td>{{ $user['appointed_at'] }}</td>
                        <td>{{ $user['comment'] }}</td>
                        <td></td>
                    </tr>
                @endforeach
            </table> 
    </div>
</div>