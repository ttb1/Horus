@extends('sale.contactStatus2.layout')
@section('content')


{{-- ----------------------New design ------------------------ --}}
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Include the above in your HEAD tag -->

<div class="container">
	<div class="row">
            {{-- <div class="hidden">
                    uuid : {{ $uuid }} contactid : {{ $contact_id }} contact_status_id = {{ $contact_status_render->id }}
                </div> --}}
		<section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Level 1">
                            <span class="round-tab">
                                {{-- <i class="glyphicon glyphicon-folder-open"></i> --}}
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" >
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Level 2">
                            <span class="round-tab">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                                {{-- <i class="glyphicon glyphicon-pencil"></i> --}}
                            </span>
                        </a>
                    </li>
                    <li role="presentation" >
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Level 3-6">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </a>
                    </li>
                    <li role="presentation">
                            <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Level 7-8">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-usd"></i>
                                </span>
                            </a>
                        </li>
    

                    <li role="presentation">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                    <i class="glyphicon glyphicon-flag"></i>
                                {{-- <i class="glyphicon glyphicon-ok"></i> --}}
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        @include('sale.contactStatus2.info_lv1')
                        <hr>
                        @include('sale.contactStatus2.all_contact_status')   
                    </div>

                    <div class="tab-pane" role="tabpanel" id="step2">  
                        @include('sale.contactStatus2.info_lv2')
                        @include('sale.contactStatus2.update_contact_status')
                        <input type="hidden" value="{{ csrf_token() }}" name="_token">
                        {{-- <input type="hidden" value={{ $uuid }} name="uuid"> --}}
                        <input type="hidden" value={{ $contact_id }} name="contact_id">
                        <div style="text-align: right;">
                            <input type="submit" name="sumbit" class="btn btn-success">
                        </div>
                    </form>
                    <hr>
                        @include('sale.contactStatus2.all_contact_status')   
                    </div>

                    <div class="tab-pane" role="tabpanel" id="step3">
                        @include('sale.contactStatus2.info_lv3_to_6')
                        @include('sale.contactStatus2.update_contact_status')
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            {{-- <input type="hidden" value={{ $uuid }} name="uuid"> --}}
                            <input type="hidden" value={{ $contact_id }} name="contact_id">
                            <div style="text-align: right;">
                                    <input type="submit" name="sumbit" class="btn btn-success">
                            </div>
                        </form>
                        <hr>
                        @include('sale.contactStatus2.all_contact_status') 
                    </div>

                    <div class="tab-pane" role="tabpanel" id="step4">
                            @include('sale.contactStatus2.info_lv7_to_8')
                            @include('sale.contactStatus2.update_contact_status')
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                {{-- <input type="hidden" value={{ $uuid }} name="uuid"> --}}
                                <input type="hidden" value={{ $contact_id }} name="contact_id">
                                <div style="text-align: right; ">
                                        <input type="submit" name="sumbit" class="btn btn-success">
                                </div>
                            </form>
                            <hr>
                            @include('sale.contactStatus2.all_contact_status') 
                    </div>

                    <div class="tab-pane" role="tabpanel" id="complete">
                            @include('sale.contactStatus2.estimate')
                            @include('sale.contactStatus2.update_contact_status')
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            {{-- <input type="hidden" value={{ $uuid }} name="uuid"> --}}
                            <input type="hidden" value={{ $contact_id }} name="contact_id">
                            <div style="text-align: right; ">
                                    <input type="submit" name="sumbit" class="btn btn-success">
                            </div>
                        </form>
                        <hr>
                        @include('sale.contactStatus2.all_contact_status') 
                    </div>

                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>

<script>
    $(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
</script>

{{-- --------------------- Old Design ------------------------ --}}
{{-- <div class="container-fluid">
    <br>
        <div>
            uuid : {{ $uuid }} contactid : {{ $contact_id }} contact_status_id = {{ $contact_status_render->id }}
        </div>
    <br>
    <div class="panel with-nav-tabs panel-default">
            <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1default" data-toggle="tab">L1</a></li>
                        <li><a href="#tab2default" data-toggle="tab">L2</a></li>
                        <li><a href="#tab3default" data-toggle="tab">L3-L6</a></li>
                        <li><a href="#tab4default" data-toggle="tab">L7-L8</a></li>
                        <li><a href="#tab5default" data-toggle="tab">Estimate</a></li>
                    </ul>
            </div>
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1default"> @include('sale.contactStatus2.info_lv1')</div>
                    <div class="tab-pane fade" id="tab2default">@include('sale.contactStatus2.info_lv2')</div>
                    <div class="tab-pane fade" id="tab3default">@include('sale.contactStatus2.info_lv3_to_6')</div>
                    <div class="tab-pane fade" id="tab4default"> @include('sale.contactStatus2.info_lv7_to_8')</div>
                    <div class="tab-pane fade" id="tab5default">@include('sale.contactStatus2.estimate')</div>
                    {{-- <div class="tab-pane fade" id="tab6default">@include('sale.contactStatus2.contact_status')</div>
                </div>
            </div>
        </div>
    </div> 
    @include('sale.contactStatus2.update_contact_status')
        <input type="hidden" value="{{ csrf_token() }}" name="_token">
        <input type="hidden" value={{ $uuid }} name="uuid">
        <input type="hidden" value={{ $contact_id }} name="contact_id">
        <div style="text-align: right; margin-right: 15px;">
            <input type="submit" name="sumbit" class="btn btn-default">
        </div>
    </form>
    @include('sale.contactStatus2.all_contact_status')   
</div> --}}

{{-- -----------------------Style New Design------------------- --}}
<style>
    .wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        /* margin: 10px auto; */
        margin-bottom: 0;
        border-bottom-color: #e0e0e0;
    }

    .wizard > div.wizard-inner {
        position: relative;
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 3px solid #4CAF50;
    
}
.wizard li.active span.round-tab i{
    color: #4CAF50;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 20%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color:#428bca;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
    padding-top: 0px;
}

.wizard h3 {
    margin-top: 0;
}

@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}
</style>




@endsection