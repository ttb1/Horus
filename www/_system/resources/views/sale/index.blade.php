@extends('layout') @section('content')
<!-- ************************************************************************************ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<!-- ************************************************************************************ -->


<div class="container-fluid">
    <h2> Schedule</h2>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th rowspan="3">Amount of work</th>
                <th rowspan="2">Quantity to call</th>
                <th colspan="2">Called number</th>
                <th rowspan="2">Number not called</th>
                <th rowspan="2">Amounts existed earlier</th>
            </tr>
            <tr>
                <th>In the calendar</th>
                <th>Out of calendar</th>
            </tr>
            <tr>
                <th>{{$data->user_dashboard_data->report[0] }}</th>
                <th>{{$data->user_dashboard_data->report[1] }}</th>
                <th>{{$data->user_dashboard_data->report[2] }}</th>
                <th>{{$data->user_dashboard_data->report[3] }}</th>
                <th>{{$data->user_dashboard_data->report[4] }}</th>
            </tr>
        </thead>
    </table>
    <hr>

    <div class="container-fluid">

        <form action="" method="get" class="form form-inline" name="search">
            <div class="row">
                <div class="form-group ">
                    <label for="start_date" class="control-label"> From : </label>
                    <input class="date form-control" name="from_date" value="{{$data->from_date->toDateString()}}">
                </div>
                <div class="form-group ">
                    <label for="start_date" class="control-label"> To : </label>
                    <input class="date form-control" name="to_date" value="{{$data->to_date->toDateString() }}">
                </div>

                <div class="form-group ">
                    <strong>Condition : </strong>

                    <input type="radio" class="form-control" name="condition" value="All" {{ $data->current_condition === "All" ? "checked":"" }} > All

                    <input type="radio" class="form-control" name="condition" value="New" {{ $data->current_condition === "New" ? "checked":"" }} > New

                    <input type="radio" class="form-control" id="Apointment" name="condition" value="Apointment" {{ $data->current_condition === "Apointment" ? "checked":"" }} > Apointment
                </div>
            </div>
            <div class="row">
                <div id="check" class="form-group" style="display: none;">
                    @foreach($data->levels as $level_item)
                    <input type="checkbox" name="level_search[]" value="{{$level_item->id}}" {{ in_array($level_item->id , $data->last_level_search )? "checked":"" }}> {{$level_item->name}} 
                    @endforeach
                </div>
            </div>

            <button type="submit">Contact Search</button>
        </form>

    </div>
    <hr>


    <div class="container-fluid">
        <div class="col-sm-1">
            <strong>Level :</strong>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-1">All(0)</div>
            @foreach($data->levels as $key => $level)
            <form action="" method="get">
                <div class="col-sm-1">{{ $level->name }} (
                    <input type="hidden" name="level" value="{{ $level->id }}">
                    <button class="btn-link" type="submit">{{ $data->user_dashboard_data->levels[$level->id] }}</button> )
                </div>
            </form>
            @endforeach
        </div>
    </div>
    <hr>

    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-heading" style="background:#dbac69;">Contact List</div>
            <div class="panel-body">

                <div class="container-fluid">

                    <table class="table">
                        <thead>
                            <tr>
                                <th rowspan="2">no</th>
                                <th colspan="4">Contact Information</th>
                                <th colspan="3">Calling Time</th>
                                <th colspan="2">Caring Status</th>
                                <th rowspan="2">Caring Status</th>
                                <th rowspan="2">Total Calling Time</th>
                                <th rowspan="2">Function</th>
                            </tr>
                            <tr>
                                <th>Full Name</th>
                                <th>Level</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Date</th>
                                <th>Morning/Afternoon</th>
                                <th>Time</th>
                                <th>Content(latest call)</th>
                                <th>Date Call</th>

                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($data->user_contacts as $contact_list)
                            <tr>
                                <td>#</td>
                                <td>{{ $contact_list->fullname }}</td>
                                <td>{{ $contact_list->level }}</td>
                                @if(count($contact_list->email) != 0 )
                                <td>{{ $contact_list->email[0] }}</td>
                                @else
                                <td></td>
                                @endif @if(count($contact_list->phone) != 0 )
                                <td>{{ $contact_list->phone[0] }}</td>
                                @else
                                <td></td>
                                @endif
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $contact_list->last_call }}</td>
                                <td></td>
                                <td>{{ $contact_list->total_call }}</td>
                                <td>
                                    <button onclick="opennewwindows( {{ $contact_list->contact_id }} )">View</button>
                                    <button onclick="opennewwindows2( {{ $contact_list->contact_id }} )">View2</button>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                    {{-- {{ $data->contact_list->links() }} --}}

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'yyyy-mm-dd'
    });

    $(window).bind("load", function () {
        var current_condition = "<?php echo $data->current_condition; ?>";
        if (current_condition == "Apointment") {
            document.getElementById("Apointment").click();
        }
    });

    function opennewwindows(contact_id) {
        window.open("{{'contactstatus'}}" + "/" + contact_id + "/" + 1, "_blank",
            "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1200%,height=600");
    }

    function opennewwindows2(contact_id) {
        window.open("{{'contactstatus'}}" + "/" + contact_id + "/" + 2, "_blank",
            "toolbar=yes,scrollbars=yes,resizable=yes,top=40,left=300,width=1300%,height=900");
    }
</script>
<script>
    var rad = document.search.condition;
    var prev = null;
    for (var i = 0; i < rad.length; i++) {
        rad[i].onclick = function () {
            // (prev) ? console.log(prev.value): null;
            // if (this !== prev) {
            prev = this;
            if (this.value == "Apointment") {
                // console.log('s', this.value)
                document.getElementById('check').style.display = 'block';
            } else {
                document.getElementById('check').style.display = 'none';
            }
            // }
        };
    }
</script>
@endsection