<!doctype html>
<html>
{{-- @include('includes.head') --}}

<head>
    @include('includes.header')
</head>

<body>
    <div class="container">
        @yield('content')
    </div>
</body>

</html>