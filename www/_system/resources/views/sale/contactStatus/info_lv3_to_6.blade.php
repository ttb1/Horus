<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
        <strong>{{ $info_lv3_to_6->blade_name }} </strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <input type="checkbox" name="l3_appointment_checkbox" id="l3_appointment_checkbox" @if ( $contact_status_render->l3_appointment_checkbox == 'on' ) checked @endif>
                <label for="appointment_time"></label>Appointment (L3A-L3C)
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control" name="l3_appointment_text" id="l3_appointment_text" value="{{ $contact_status_render->l3_appointment_text }}">
            </div>
            <div class="col-sm-2">
                <select name="l3_appointment_select" id="l3_appointment_select" class="form-control">
                    <option value="cash" @if ( $contact_status_render->l3_appointment_select == 'cash' ) selected @endif>Cash</option>
                    <option value="other" @if ( $contact_status_render->l3_appointment_select == 'other' ) selected @endif>Other</option>
                </select>
            </div>
            <div class="col-sm-5">
                <div class="col-sm-3">
                    <label for="appointment_time"></label>Description
                </div>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="l3_appointment_description_text" id="l3_appointment_description_text" value="{{ $contact_status_render->l3_appointment_description_text }}">
                </div>
            </div>
        </div>

        <div style=" padding-top: 10px;"></div>


        <!-- this is  appointment_interview section-->
        <div class="row">
            <div class="col-xs-12">
                <input type="checkbox" name="l3_appointment_interview_checkbox" id="l3_appointment_interview_checkbox" @if ( $contact_status_render->l3_appointment_interview_checkbox == 'on' ) checked @endif onclick="appointment(this)" ;>
                <label for="appointment_interview"></label>Appointment interview

                <button class="btn-link" style="color: black;" type="button" onclick="window.open('http://testerdev.topicanative.asia/view_schedule_sale','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                    <span class="glyphicon glyphicon-search "> </span>
                    Book Tester schedule
                </button>
                <button class="btn-link" style="color: black;" type="button" onclick="window.open('http://testerdev.topicanative.asia/view_schedule_sale','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                    <span class="glyphicon glyphicon-list "> </span>
                    Open recorder interview
                </button>

                <!-- <div id='appointment_interview_stage' style="display: none;"> -->
                @if ( $contact_status_render->l3_appointment_interview_checkbox == 'on' )

                <div id='appointment_interview_stage' style="display: block;">
                    @else
                    <div id='appointment_interview_stage' style="display: none;">
                        @endif
                        <table class="table">
                            <thead>

                                <th>date</th>
                                <th>time</th>
                                <th>teacher</th>
                                <th>description</th>
                                <th>description(cm)</th>
                                <th>status</th>
                                <th>function</th>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="text" class="date" name="date_schedule" id="date_schedule" value="{{ $info_lv3_to_6->today }}">
                                    </td>
                                    <td>
                                        <select name="time_schedule" id="time_schedule">
                                            @foreach($info_lv3_to_6->time as $time)
                                            <option value="$time->id">{{ $time->time_period }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select name="teacher" id="teacher">
                                            <option value="Native">Native</option>
                                            <option value="Local">Local</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="description" name="description">
                                    </td>
                                    <td>
                                        <input type="text">
                                    </td>
                                    <td>
                                        <select name="" id="">
                                            @foreach( $info_lv3_to_6->tester_status as $tester_status )
                                            <option value="$tester_status->id">{{ $tester_status->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <button class="btn-danger" type="button" onclick="bookingTester()">
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="loader" style="display: none;" id="loader"></span>

                        <span id="mesageBooking" class="alert-success"></span>

                    </div>
                </div>
            </div>

            <!-- this is  appointment_interview section end-->

            {{-- <div style=" padding-top: 7px;"></div> --}}

            <div class="row">
                <div class="col-xs-12">
                    <label for="test_English_Topica" onclick="testEnglishTopica()">
                        <input type="checkbox" name="l4_l5_information_test_checkbox" id="l4_l5_information_test_checkbox" @if ( $contact_status_render->l4_l5_information_test_checkbox == 'on' ) checked @endif>
                    </label>Get information test English Topica
                    <div id='test_english_topica' style="display: none;">
                        <table class="table">
                            <thead>
                                <th>วันที่เกรดบัญชี</th>
                                <th>บัญชี Topica</th>
                                <th>รหัสผ่าน Topica</th>
                                <th>สถานะ</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span id="test_english_topica_date"></span>
                                    </td>
                                    <td>
                                        <span id="test_english_topica_username"></span>
                                    </td>
                                    <td>
                                        <span id="test_english_topica_password"></span>
                                    </td>
                                    <td>
                                        <span id="test_english_topica_status"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button type="button" onclick="window.open('{{ url('mail_l3'.'/'.$contact_id )}}','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')"
                        class="btn-lg btn-link">
                        <span class="glyphicon glyphicon-envelope"></span> Tutorial Topica test .
                    </button>
                    <a href="{{ url('contactstatus/dummy/'.$contact_id.'/1'.'/'.$version) }}">dummy test</a>
                </div>
            </div>
            {{-- <div style=" padding-top: 7px;"></div> --}}
            <div class="row">
                <div class="col-xs-12">
                    <input type="checkbox" name="l4_l5_information_casec_checkbox" id="l4_l5_information_casec_checkbox" disabled @if ( $contact_status_render->l4_l5_information_casec_checkbox == 'on' ) checked @endif>
                    <label for="get_information_casec"></label>Get information Casec
                    <button class="btn-lg btn-link" onclick="window.open('{{'mail'}}','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                        <span class="glyphicon glyphicon-envelope"></span> Tutorial Casec test .
                    </button>
                    <a href="{{ url('contactstatus/dummy/'.$contact_id.'/2'.'/'.$version) }}">dummy Casec </a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-3">
                    <input type="checkbox" name="l4_l5_score_interview_checkbox" id="l4_l5_score_interview_checkbox" @if ( $contact_status_render->l4_l5_score_interview_checkbox == 'on' ) checked @endif>
                    <label for="score_test_casec"></label> have score Casec
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>
            <div class="row">
                <div class="col-xs-3">
                    <input type="checkbox" name="l4_l5_score_test_checkbox" id="l4_l5_score_test_checkbox" @if ( $contact_status_render->l4_l5_score_test_checkbox == 'on' ) checked @endif>
                    <label for="score_test_topica"></label> have score test topica
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>
            <div class="row">
                <div class="col-xs-3">
                    <input type="checkbox" name="l4_l5_score_interview_checkbox" id="l4_l5_score_interview_checkbox" @if ( $contact_status_render->l4_l5_score_interview_checkbox == 'on' ) checked @endif>
                    <label for="score_interview"></label> have score interview
                </div>
            </div>
            <hr>
            <!-- *** this for L6 ***-->
            <div class="row">
                <div class="col-xs-3">

                    <label for="Order_SB100">
                        <input type="checkbox" name="l6_Order_SB100_checkbox" id="l6_Order_SB100_checkbox" @if ( $contact_status_render->l6_Order_SB100_checkbox == 'on' ) checked @endif> Order SB100</label>
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-3">
                    <label for="Order_SB100_System">
                        <input type="checkbox" name="l6_Order_SB100_System_checkbox" id="l6_Order_SB100_System_checkbox" @if ( $contact_status_render->l6_Order_SB100_System_checkbox == 'on' ) checked @endif> SB100 (English Topica System) </label>
                </div>
            </div>
        </div>
    </div>

    <script>
        function testEnglishTopica() {
            var appointment_interview = document.getElementById('l4_l5_information_test_checkbox').checked;
            if (appointment_interview) {
                alert('รับข้อมูล รหัสผ่าน ของลูกค้า');
                $.ajax({
                    method: "GET",
                    url: "{{ url('genaratetestuser')}}",
                    data: {
                        contactID: '<?php echo $contact_id ; ?>',
                    },
                    statusCode: {
                        500: function () {
                            alert("error /500");
                            loader.style.display = 'none';
                        },
                        404: function () {
                            alert("page not found /404");
                            loader.style.display = 'none';
                        }
                    }
                }).done(function (data) {
                    //alert(data.contactID );
                    //console.log(data);
                    var date = document.getElementById('test_english_topica_date');
                    date.innerHTML = data.appointed_at;
                    var username = document.getElementById('test_english_topica_username');
                    username.innerHTML = data.username;
                    var password = document.getElementById('test_english_topica_password');
                    password.innerHTML = data.password;
                    var status = document.getElementById('test_english_topica_status');
                    status.innerHTML = data.status;
                    document.getElementById('test_english_topica').style.display = 'block';
                });

            } else
                document.getElementById('test_english_topica').style.display = 'none';
        }
    </script>
    <script>
        function appointment() {
            var appointment_interview = document.getElementById('l3_appointment_interview_checkbox').checked;
            //console.log(document.getElementById('l3_appointment_interview_checkbox').checked);
            if (appointment_interview)
                document.getElementById('appointment_interview_stage').style.display = 'block';
            else
                document.getElementById('appointment_interview_stage').style.display = 'none';
        }

        function bookingTester() {
            var loader = document.getElementById('loader');
            loader.style.display = 'block';


            var time_schedule = document.getElementById('time_schedule').value;
            var date_schedule = document.getElementById('date_schedule').value;
            var teacher = document.getElementById('teacher').value;
            var fullname = document.getElementById('fullname').value;
            var contactID = '<?php echo $contact_id ; ?>'
            var description = document.getElementById('description').value;
            var element = document.getElementById("mesageBooking");
            element.classList.remove("alert-danger");
            element.classList.remove("alert-success");
            // console.log(date_schedule);


            $.ajax({
                method: "GET",
                url: "{{ url('bookingScheduleAdd')}}",
                data: {
                    fullname: fullname,
                    contactID: contactID,
                    description: description,
                    time_schedule: time_schedule,
                    date_schedule: date_schedule,
                    teacher: teacher
                },
                statusCode: {
                    500: function () {
                        alert("error /500");
                        loader.style.display = 'none';
                    },
                    404: function () {
                        alert("page not found /404");
                        loader.style.display = 'none';
                    }
                }
            }).done(function (data) {
                //alert("Data Saved: " + msg);
                //  console.log(data);
                var getData = JSON.parse(data);
                // console.log(getData.data);
                // console.log(getData.response.msg);
                if (getData.response.status == 1) {
                    var element = document.getElementById("mesageBooking");
                    element.classList.add("alert-danger");
                } else {
                    var element = document.getElementById("mesageBooking");
                    element.classList.add("alert-success");
                }
                document.getElementById("mesageBooking").innerHTML = getData.response.msg;
                loader.style.display = 'none';
            });
        }
    </script>

    <style>
        .loader {
            border: 6px solid #f3f3f3;
            border-radius: 100%;
            border-top: 6px solid #3498db;
            border-bottom: 6px solid #3498db;
            width: 30px;
            height: 30px;
            -webkit-animation: spin 1s linear infinite;
            /* Safari */
            animation: spin 1s linear infinite;
        }

        /* Safari */

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>