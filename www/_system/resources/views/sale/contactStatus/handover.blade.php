
<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
        L7-L8 (handover)
    </div>
    <div class="panel-body">
<!--        {{ $handover->name_th }}-->
        
        <div class="row">
            <div class="col-md-2">
                <input type="checkbox" name="send_handover">
            </div>
            <div class="col-md-5">
                <label class="label-control">
                    ส่ง handover
                </label>
            </div>
            
            
        </div>
        <div class="row">
            
           
            <div class="col-md-2">
                <label class="label-control">
                    Your Team
                </label>
            </div>
            <div class="col-md-4">
                <select name="user_sale_group_id" class="form-control">
                    <option value="bkk1">bkk1</option>
                    <option value="bkk2">bkk2</option>
                    <option value="bkk3" selected="true">bkk3</option>
                    <option value="bkk1">bkk4</option>
                </select>
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    Link sb100
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="link_sb100" class="form-control" >
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    ชื่อ (ไทย)
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="name_th" class="form-control" value="{{ $handover->name_th }}" >
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    ชื่อ (อังกฤษ)
                </label>
            </div>
             <div class="col-md-4">
                 <input type="text" name="name_en" class="form-control" value="{{ $handover->name_en }}">
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    เบอร์โทร
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="phone" class="form-control" value="{{ $handover->phone }}">
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    อีเมล์
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="email" class="form-control" value="{{ $handover->email }}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    Contact Type
                </label>
            </div>
            <div class="col-md-4">
                <select name="contact_type_id" class="form-control">
                    @foreach ($info_lv7_to_8['contact_type'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    Contact ID
                </label>
            </div>
             <div class="col-md-4">
                <input type="text" name="contact_id" class="form-control" value="{{ $handover->contact_id }}">
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    English Skill
                </label>
            </div>
            <div class="col-md-4">
                <select name="english_skill_id" class="form-control">
                    @foreach ($info_lv7_to_8['english_skill'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    Level
                </label>
            </div>
            <div class="col-md-4">
                <select name="level_type_id" class="form-control">
                    @foreach ($info_lv7_to_8['level'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    อายุ
                </label>
            </div>
            <div class="col-md-4">
                <select name="age_type_id" class="form-control">
                     @foreach ($info_lv7_to_8['age'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    Personality
                </label>
            </div>
             <div class="col-md-4">
                <input type="text" name="personality" class="form-control" >
            </div>
        </div>
        
<br><br>
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    รายละเอียดการโอน
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="payment_detail" class="form-control" >
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    คอร์สเรียนที่ซื้อ
                </label>
            </div>
             <div class="col-md-4">
                <select name="total_learn_day_id" class="form-control">
                    @foreach ($info_lv7_to_8['course'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    ค่าเรียนโปรโมชั่น
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="course_price" class="form-control" >
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    ราคาโปรโมชั่น
                </label>
            </div>
             <div class="col-md-4">
                <input type="text" name="promotion_price" class="form-control" >
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    ราคาปกติ
                </label>
            </div>
            <div class="col-md-4">
                <input type="text" name="normal_price" class="form-control" >
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    เหตุผลที่ได้โปรโมชั่น
                </label>
            </div>
             <div class="col-md-4">
                <select name="promotion_type_id" class="form-control">
                    @foreach ($info_lv7_to_8['promotion_type'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    ช่องทางการชำระเงิน
                </label>
            </div>
            <div class="col-md-4">
                <select name="payment_method_id" class="form-control">
                    @foreach ($info_lv7_to_8['payment_method'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label class="label-control">
                    ของแถมให้ผู้เรียน
                </label>
            </div>
             <div class="col-md-4">
                 <select name="free_gift_id" class="form-control">
                    @foreach ($info_lv7_to_8['free_gift'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-2">
                <label class="label-control">
                    Payment Type
                </label>
            </div>
            <div class="col-md-4">
                 <select name="payment_type_id" class="form-control">
                    @foreach ($info_lv7_to_8['payment_type'] as $value)
                        <option value="{{ $value->value }}">{{ $value->value }}</option>
                    @endforeach
                </select>
            </div>
           
        </div>
    
    </div>
</div>