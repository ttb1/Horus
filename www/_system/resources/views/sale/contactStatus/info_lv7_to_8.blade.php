<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
        L7-L8
    </div>
    <div class="panel-body">
            <input type="checkbox" name="l7_sb100_checkbox" id="l7_sb100_checkbox" @if ( $contact_status_render->l7_sb100_checkbox == 'on' ) checked @endif>
            <label for="sb100"> get SB100 </label>

            <button class="btn-link" onclick=" window.open('http://google.com','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                <span class="glyphicon glyphicon-envelope "> </span>
                Send email recommend program (v2B)
            </button>

            <hr>

            <div class="row">
                <div class="col-sm-2">
                    <label for="transfer_money">transfer money</label>
                </div>
                <div class="col-sm-8">
                    <div class="form-inline">
                        <div class="row">
                            <div class="form-group">
                                <label for="Price" class="label-control">Price</label>
                                <select name="l7_price_select" id="l7_price_select" class="form-control">
                                    <option value="12000" @if ( $contact_status_render->l7_price_select == '12000' ) selected @endif>12000</option>
                                    <option value="23000" @if ( $contact_status_render->l7_price_select == '23000' ) selected @endif>23000</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <label for="transfer_by" class="label-control">transfer by</label>
                                <select name="l7_transfer_select" id="l7_transfer_select" class="form-control">
                                    <option value="full" @if ( $contact_status_render->l7_transfer_select == 'full' ) selected @endif>full</option>
                                    <option value="half" @if ( $contact_status_render->l7_transfer_select == 'half' ) selected @endif>half</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="close" class="label-control">close</label>
                                <input type="text" class="form-control" name="l7_close_text" id="l7_close_text" value="{{ $contact_status_render->l7_close_text }}">
                            </div>
                            <div class="form-group">
                                <label for="remainder" class="label-control">remainder</label>
                                <input type="text" class="form-control" name="l7_remainder_text" id="l7_remainder_text" value="{{ $contact_status_render->l7_remainder_text }}">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <label for="Note" class="label-control">note</label>
                                <textarea class="form-control" rows="3" name="l7_note_text" id="l7_note_text" >{{ $contact_status_render->l7_note_text }}</textarea>
                            </div>
                        </div>
                    </div>
                    <hr>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <label for="transfer_by" class="label-control">Sale</label>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <select name="l7_transfer_by" id="l7_transfer_by" class="form-control">
                                <option value="full" @if ( $contact_status_render->l7_transfer_by == 'full' ) selected @endif>Native</option>
                                <option value="half" @if ( $contact_status_render->l7_transfer_by == 'half' ) selected @endif>Tenup</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>