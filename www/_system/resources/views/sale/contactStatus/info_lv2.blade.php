<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
        <strong> {{ $info_lv2->blade_name }} </strong>
    </div>
    <div class="panel-body">
        <div class="col-sm-4">
            <input type="checkbox" name="l2_checkbox" id="l2_checkbox" @if ( $contact_status_render->l2_checkbox == 'on' ) checked @endif > Customer want to learn English?
        </div>

        <!--//Todo chenge url to mail module -->
        <div class="col-sm-4">
            <button type="button" class="btn-link" onclick=" window.open('{{ url('mail_l2'.'/'.$contact_id.'/'.'A' )}}','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                <span class="glyphicon glyphicon-envelope "> </span>
                Send email recommend program (v2A)
            </button>
        </div>
        <div class="col-sm-4">
            <button class="btn-link" type="button"  onclick=" window.open('{{ url('mail_l2'.'/'.$contact_id.'/'.'B' )}}','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">
                <span class="glyphicon glyphicon-envelope "> </span>
                Send email recommend program (v2B)
            </button>
        </div>
    </div>
</div>