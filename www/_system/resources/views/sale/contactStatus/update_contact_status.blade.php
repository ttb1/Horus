<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
       <strong> Update Contact Status</strong>
    </div>
    <div class="panel-body">

        
            <div class="row">
                <div class="col-md-2">Time call back</div>
                <div class="col-md-4">
                    <input type="datetime-local" name="update_callback_datetime" class="form-control" value={{ $datetime_now->format('Y-m-d\TH:i T') }}>
                </div>
                <div class="col-md-2">Call quility</div>
                <div class="col-md-4">
                    <select class="form-control" name="update_call_quility_select">
                        <option value="1">A1</option>
                        <option value="2">A2</option>
                        <option value="3">A3</option>
                        <option value="4">A4</option>
                        <option value="5">A5</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">Status call</div>
                <div class="col-md-4">
                    <select class="form-control" name="status_call">
                        @foreach ( $update_contact_status->status_call as $status_list )
                            <option value="{{$status_list->next_level_id}}">{{$status_list->name}}</option>                            
                        @endforeach
                        {{-- <option value="2">B2</option>
                        <option value="3">B3</option>
                        <option value="4">B4</option>
                        <option value="5">B5</option> --}}
                    </select>
                </div>
                <div class="col-md-2">Service status</div>
                <div class="col-md-4">
                    <select class="form-control" name="update_service_status_select">
                        <option value="1">C1</option>
                        <option value="2">C2</option>
                        <option value="3">C3</option>
                        <option value="4">C4</option>
                        <option value="5">C5</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">Comment</div>
                <div class="col-md-10">
                    <textarea class="form-control" rows="3" name="comment"></textarea>
                </div>
            </div>
           
    </div>
</div>


