@extends('sale.contactStatus.layout')
@section('content')

<div class="container-fluid">
    <br>
    
    
    
    @include('sale.contactStatus.info_lv1')

    <form action="{{ route('updateContactStatus') }}" method="POST">
        
        @include('sale.contactStatus.info_lv2')

        @include('sale.contactStatus.info_lv3_to_6')

    <!--        @include('sale.contactStatus.info_lv7_to_8') -->
        
        @include('sale.contactStatus.handover')

        @include('sale.contactStatus.update_contact_status')    
        
        

        <input type="hidden" value="{{ csrf_token() }}" name="_token">
        <input type="hidden" value={{ $contact_id }} name="contact_id">
        <div style="text-align: right; margin-right: 15px;">
            <input type="submit" name="sumbit" class="btn btn-default">
        </div>

    </form> 

 @include('sale.contactStatus.all_contact_status')   
   

</div>

@endsection