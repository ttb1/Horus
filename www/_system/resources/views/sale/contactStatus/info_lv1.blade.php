<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<?php 
    $contact = $info_lv1->contact;
    if(count($contact->email) == 0) {
        $contact->email = ['','','' ];
    }
    if(count($contact->email) < 2) {
        $contact->email = [$contact->email[0] ,'','' ];
    }
    if(count($contact->phone) == 0) {
        $contact->phone = ['','',''];
    }
    if(count($contact->phone) < 2 ) {
        $contact->phone = [$contact->phone[0],'',''];
    }
    if(count($contact->phone) < 3 ) {
        $contact->phone = [$contact->phone[0],$contact->phone[1],''];
    }
    // dd($info_lv1->contact);
?>
<div class="panel panel-default">
    <div class="panel-heading" style="background:#dbac69;">
        <strong>{{ $info_lv1->blade_name }} </strong>
    </div>
    <div class="panel-body">
        <form action="{{ route('l1update')}}" class="form" method="POST">
            <input type="hidden" name="version" value="{{$version}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-1">
                    <label for="Fullname" class="label-control">Fullname</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="fullname" id="fullname" value="{{ $info_lv1->contact->firstname }} {{ $info_lv1->contact->lastname }}">
                </div>

                <div class="col-xs-1">
                    <label for="Gender">Gender </label>
                </div>
                <div class="col-xs-3">
                    <!-- <input type="text" name="gender" id="gender" readonly> -->
                    <select name="gender" id="gender" class="form-control">
                        <!-- <option value="select">select</option> -->
                        @if ($info_lv1->contact->gender == 'man')
                        <option value="man" selected>man</option>
                        @else
                        <option value="man">man</option>
                        @endif @if ($info_lv1->contact->gender == 'female')
                        <option value="female" selected>female</option>
                        @else
                        <option value="female">female</option>
                        @endif @if ($info_lv1->contact->gender == 'other')
                        <option value="other" selected>other</option>
                        @else
                        <option value="other">other</option>
                        @endif

                    </select>
                </div>
                <div class="col-xs-1">
                    <label for="Birth">Birth</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" data-provide="datepicker" name="birth" id="birth" value="{{ $info_lv1->contact->birthdate }}"
                        autocomplete="off">
                </div>
            </div>


            {{--
            <div style=" padding-top: 7px;"></div> --}}

            <div class="row">
                <div class="col-xs-1">
                    <label for="Tel">Tel.</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" {{-- onclick="callAsterisk('{{ $contact_id.'.'.$uuid }}' )"
                                --}} onclick="callByphone1('{{ $info_lv1->user_station[0]->station_id }}')" style="background:#dbac69;"
                                type="button"></button>
                        </span>
                        <input type="text" class="form-control" name="phone[]" id="phone1" value="{{ $info_lv1->contact->phone[0] }}" readonly>
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#dbac69;" type="button" onclick="callByphone1('{{ $info_lv1->user_station[1]->station_id }}')"></button>
                        </span>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Tel2">Tel.2</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#dbac69;" type="button" onclick="callByphone2('{{ $info_lv1->user_station[0]->station_id }}')"></button>
                        </span>
                        <input type="text" class="form-control" name="phone[]" id="phone2" value="{{ $info_lv1->contact->phone[1] }}">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#dbac69;" type="button" onclick="callByphone2('{{ $info_lv1->user_station[1]->station_id }}')"></button>
                        </span>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Tel3">Tel.3</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#dbac69;" type="button" onclick="callByphone3('{{ $info_lv1->user_station[0]->station_id }}')">
                            </button>
                        </span>
                        <input type="text" class="form-control" name="phone[]" id="phone3" value="{{ $info_lv1->contact->phone[2] }}">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-earphone" style="background:#dbac69;" type="button" onclick="callByphone3('{{ $info_lv1->user_station[1]->station_id }}')">
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="Email">Email.</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-envelope" id="email_addon"></span>
                        <input type="text" class="form-control " aria-describedby="email_addon" name="email[]" id="email" value="{{ $info_lv1->contact->email[0] }}"
                            readonly>
                    </div>
                </div>
                <div class="col-xs-1">
                    <label for="Email 2">Email 2</label>
                </div>
                <div class="col-xs-3">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-envelope" id="email2_addon"></span>
                        <input type="text" class="form-control" aria-describedby="email2_addon" name="email[]" id="email" value="{{ $info_lv1->contact->email[1] }}">
                    </div>
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="contact_id ">ContactID.</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="contact_id" id="contact_id" value="{{ $info_lv1->contact->contact_id }}" readonly>
                </div>
                <div class="col-xs-1">
                    <label for="line_id ">LineID.</label>
                </div>
                <div class="col-xs-3">
                    <input type="text" class="form-control" name="line_id" id="line_id" value="{{ $info_lv1->contact->line_id }}">
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="Contact_quality">Contact quality.</label>
                </div>
                <div class="col-xs-7">
                    <textarea class="form-control" id="Contact_quality" rows="2" cols=" 100%" name="contact_quality">{{ $info_lv1->contact->contact_quality }}</textarea>
                </div>
            </div>
            <div style=" padding-top: 7px;"></div>

            <div class="row">
                <div class="col-xs-1">
                    <label for="tester_quality">Tester quality.</label>
                </div>
                <div class="col-xs-7">
                    <textarea class="form-control" id="tester_quality" name="tester_quality" rows="2" cols=" 100%">{{ $info_lv1->contact->tester_quality }}</textarea>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-1"> </div>
                <button class="btn-lg btn-link" type="button" onclick=" window.open('http://google.com','_blank' ,'toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1000,height=600')">

                    <span class="glyphicon glyphicon-envelope"></span> Send introduction email.
                </button>

                <button class="pull-right btn btn-app" style="margin-right: 5%; background:#dbac69;">Save L1</button>

            </div>

        </form>
    </div>
</div>

<script type="text/javascript">
    $('#birth').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
</script>
<script>
    function callAsterisk(contact_id, mobile_phone, station_id) {
        $.ajax({
            method: "GET",
            url: "{{ url('call/asterisk_call')}}",
            data: {
                contact_id: contact_id,
                mobile_phone: mobile_phone,
                station_id: station_id // user station
            },
            statusCode: {
                500: function () {
                    alert("error /500");

                },
                404: function () {
                    alert("page not found /404");

                }
            }
        }).done(function (data) {
            console.log(data);
        });
    }

    function callByphone3(station_id) {

        var mobile_phone = $("#phone3").val();
            this.callAsterisk('{{ $contact_id }}', mobile_phone, station_id);

    }

    function callByphone2(station_id) {

        var mobile_phone = $("#phone2").val();

            this.callAsterisk('{{ $contact_id }}', mobile_phone, station_id);
  
    }

    function callByphone1(station_id) {

        var mobile_phone = $("#phone1").val();

        this.callAsterisk('{{ $contact_id }}', mobile_phone, station_id);

    }
</script>
