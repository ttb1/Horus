@extends('layout')
@section('content')
<form action="{{ route('save_sale_group') }}" method="GET" >
    <label for="usr">Name group:</label>
    <input type="text" class="form-control" name="name" value="{{ $sale_group->name }}">
    <label for="usr">Description:</label>
    <input type="text" class="form-control" name="description" value="{{ $sale_group->description }}">
    <input type="hidden" name="id" value="{{ $sale_group->id }}">
    <input type="submit" class="btn" value="Save">
</form>
@endsection