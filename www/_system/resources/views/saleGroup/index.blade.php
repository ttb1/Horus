@extends('layout')
@section('content')

<form action="{{ route('add_sale_group') }}" method="GET">
    <input type="hidden" name="id" value="0">
    <input type="submit" class="btn" value="add group">
</form>
<form action="{{ route('user_sale_group') }}" method="GET">
    <input type="hidden" name="selectGroup1" value="0">
    <input type="hidden" name="selectGroup2" value="0">
    <input type="submit" class="btn" value="Edit">
</form>
<table class="table ">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Edit group</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($sale_group as $group)
        <tr>
            <td>
                {{$group->name}}
            </td>
            <td>
                {{$group->description}}
            </td>
            <td>
                <form action="{{ route('edit_sale_group') }}" method="GET">
                    <input type="hidden" name="id" value="{{$group->id}}">
                    <input type="submit" class="btn" value="Edit">
                </form>
            </td>
            <td>
                <form action="{{ route('remove_sale_group') }}" method="GET">
                    <input type="hidden" name="id" value="{{$group->id}}">
                    <input type="submit" class="btn" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection