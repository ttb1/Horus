import React, { Component } from "react";
import ExamInfomation from "./examp/ExamInformation";
import TesterInformation from "./examp/TesterInformation";

export default class ExampData extends React.Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this.setState({
      data: this.props.main
    });
  }

  render() {
    //  console.log(this);
    let data = this.props.main;

    return (
      <div className="card">
        <div className="card-header">L4 - L6</div>
        <div className="card-body">
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            {/* start sector */}
            <div className="col-md-12">
              <ExamInfomation contactid={data.contact_id} main={data} />

              <hr />
            </div>
            {/* end sector */}

            {/* start sector */}
            <div className="col-md-12">
              <TesterInformation contactid={data.contact_id} main={data}/>
              <hr />
            </div>
            {/* end sector */}

            {/* start sector */}
            <div className="col-md-12">
              <div className="form-group row">
                <div className="col-12">
                  <label htmlFor="sb100" className="control-label">
                    <strong>SB100 </strong>
                  </label>
                  <br />

                </div>

                <div className="col-3 border border-left-0 border-bottom-0 border-top-0">
                  <label htmlFor="sb100 Status" className="control-label">
                    <strong>Status </strong>
                  </label>{" "}
                  : Prepering
                  <br />
                  <button className="btn btn-grey">View</button>
                  <br />
                </div>
                <div className="col-9">
                 ss
                </div>
              </div>
              <hr />
            </div>
            {/* end sector */}

            <div className="col-md-12">
              <div className="form-group">
                <label htmlFor="note" className="control-label">
                  <strong>Note : </strong>
                </label>
                <textarea
                  name="note"
                  id="note"
                  rows="5"
                  className="form-control"
                  defaultValue="text"
                style={{ resize:"none" }}

                />
              </div>
              <button type="button" className="btn btn-sm btn-grey">
                Update
              </button>
              <hr />
            </div>

            {/* end */}
          </div>
        </div>
      </div>
    );
  }
}
