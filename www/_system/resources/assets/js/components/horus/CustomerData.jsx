import React, { Component } from "react";

export default class CustomerData extends React.Component {
  render() {
    return (
      <div className="card">
        <div className="card-header">
          <h2> Customer Data</h2>
        </div>
        <div className="card-body">
          <form action="#" method="get" className="form">
            {/* field set Quality */}
            <div
              className="row"
              style={{ marginTop: "-15px", marginBottom: "-15px" }}
            >
              <div className="col-md-6">
                <div className="form-group">
                  <label
                    htmlFor="contact_quality"
                    className="control-label col-12"
                  >
                    <strong>Contact Quality : </strong>
                    <select
                      name="contact_quality"
                      id="contact_quality"
                      className="select2-drop form-control"
                    >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </label>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <label
                    htmlFor="Call quality"
                    className="control-label col-12"
                  >
                    <strong>Call Quality : </strong>
                    <select
                      name="call quality"
                      id="call quality"
                      className="select2-drop form-control"
                    >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </label>
                </div>
              </div>
            </div>
            {/* field set Age adn Target */}

            <div
              className="row"
              style={{ marginTop: "-15px", marginBottom: "-15px" }}
            >
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="age" className="control-label col-12">
                    <strong>Age : </strong>
                    <select
                      name="age"
                      id="age"
                      className="select2-drop form-control"
                    >
                      <option value="1">15-20</option>
                      <option value="2">21-25</option>
                      <option value="3">26-30</option>
                      <option value="4">31-35</option>
                      <option value="5">36-40</option>
                      <option value="5">40-45</option>
                    </select>
                  </label>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="target" className="control-label col-12">
                    <strong>Target : </strong>
                    <select
                      name="target"
                      id="target"
                      className="select2-drop form-control"
                    >
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </label>
                </div>
              </div>
            </div>

            {/* field set Occupation and English level */}
            <div
              className="row"
              style={{ marginTop: "-15px", marginBottom: "-15px" }}
            >
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="occupation" className="control-label col-12">
                    <strong>Occupation : </strong>
                    <select
                      name="occupation"
                      id="occupation"
                      className="select2-drop form-control"
                    >
                      <option value="1">selected one</option>
                      <option value="2">Arts</option>
                      <option value="3">Business</option>
                      <option value="4">Law</option>
                      <option value="5">student</option>
                      <option value="5">other</option>
                    </select>
                  </label>
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <label
                    htmlFor="english_level"
                    className="control-label col-12"
                  >
                    <strong>English level : </strong>
                    <select
                      name="english_level"
                      id="english_level"
                      className="select2-drop form-control"
                    >
                      <option value="1">level 1</option>
                      <option value="2">level 2</option>
                      <option value="3">level 3</option>
                      <option value="4">level 4</option>
                      <option value="5">level 5</option>
                    </select>
                  </label>
                </div>
              </div>
            </div>

            {/* field set Line */}
            <div
              className="row"
              style={{ marginTop: "-15px", marginBottom: "-15px" }}
            >
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="line_id" className="control-label col-12">
                    <strong>Line id : </strong>
                    <input type="text" className="form-control" />
                  </label>
                </div>
              </div>
            </div>

            {/* field set Note */}
            <div
              className="row"
              style={{ marginTop: "-15px", marginBottom: "-15px" }}
            >
              <div className="col-md-12">
                <div className="form-group">
                  <label htmlFor="note" className="control-label col-12">
                    <strong>Note : </strong>
                    <textarea
                      name="note"
                      id="note"
                      rows="5"
                      className="form-control"
                      defaultValue="text"
                      style={{ resize: "none" }}
                    />
                  </label>
                </div>
                <hr />
                <button type="button" className="btn btn-sm btn-grey">
                  Update Prepering-send
                </button>
                <hr />
              </div>
            </div>

            {/* end */}
          </form>
        </div>
      </div>
    );
  }
}
