import React, { Component } from "react";

export default class CustomerInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      status: ""
    };
  }
  HandleUpdate(e) {
    this.setState({
      status: "Loading..."
    })
    let $this = this;
    axios
      .get("/api/sales_pipelinel2/" + this.props.main.level_contact.deal_id)
      .then(function(response) {
        // var get = JSON.parse(data);
       console.log(response);
        $this.setState({
          status: "complete"
        });
      })
      .catch(function(error) {
        // handle error
        $this.setState({
          status: "wrong update!!!"
        });
        console.log(error);
      });
  }
  render() {
    let data = this.props.main;
    return (
      <div className="card">
        <div className="card-header">
          <div className="row">
            <div className="col-md-6">
              <h2> Customer information</h2>
            </div>
            <div className="col-md-6 text-right">
              <legend style={{ fontFamily: "Times New Roman" }}>
                <h1>( {data.level_contact.level_contact} )</h1>
              </legend>
            </div>
          </div>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-sm-2 text-right">
              <label>
                <strong>Name : </strong>{" "}
              </label>
            </div>
            <div className="col-sm-6">
              {data.contact.firstname.value ? data.contact.firstname.value : ""}{" "}
              {data.contact.lastname.value ? data.contact.lastname.value : ""}{" "}
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2 text-right">
              <label>
                <strong>Tel : </strong>{" "}
              </label>
            </div>
            <div className="col-sm-6">
              {data.contact.phone.value ? data.contact.phone.value : "no phone"}
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2 text-right">
              <label>
                <strong>Email : </strong>{" "}
              </label>
            </div>
            <div className="col-sm-6">
              {data.contact.email.value ? data.contact.email.value : "no email"}
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2 text-right">
              <label>
                <strong>Create at: </strong>{" "}
              </label>
            </div>
            <div className="col-sm-6">
              {data.createdate ? data.createdate : "no data"}
            </div>
          </div>
          <div className="row">
            <div className="col-sm-2 text-right">
              <label>
                <strong>Last Contact: </strong>
              </label>
            </div>
            <div className="col-sm-6">
              <dd> 1.test </dd>
              <dd> 2.test </dd>
              <dd> 3.test </dd>
              <a href="#">read more</a>
            </div>
          </div>
          <hr />
          <button
            type="button"
            className="btn btn-sm btn-grey"
            onClick={this.HandleUpdate.bind(this)}
          >
            Update Contact to L2
          </button>{" "}
            {this.state.status}
          <hr />
        </div>
      </div>
    );
  }
}
