import React, { Component } from "react";

const CALL_STATUS = "Click to Call";

const CALL_DEFAULT = "btn btn-success";
const CALLING = "btn btn-danger";

class Call extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],

      call_status: CALL_STATUS,
      call_btn_status: false,
      call_btn_disabled: false,
      call_btn_css: CALL_DEFAULT,
      
      //call data
      station1: "",
      station2: "",
      contactid: "",
      mobilephone: "",
      call_id: "",
      call_id_display: "",
      engagement_id: "",

      selectedOption:"option1"
    };
  }

  componentDidMount(e) {
    // console.log(this);
    this.setState({
      station1: this.props.station1,
      station2: this.props.station2,
      contactid: this.props.contactid,
      mobilephone: this.props.mobilephone,
      stationid:this.props.station1,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    // console.log(nextProps, nextState);
    if (nextProps.mobilephone != nextState.mobilephone) {
      let $state = this;
      $state.setState({
        mobilephone: nextProps.mobilephone
      });
      //return true;
    }

    return true;
  }

  onClickToCall(station, e) {
  // console.log(station , e);
      let stationid = this.state.stationid;
      let contactid = this.state.contactid;
      let mobilephone = this.state.mobilephone;
      if (this.state.call_btn_status) {
        // if end call
        this.endCall(contactid, mobilephone, stationid);
        this.setState({
          call_status: CALL_STATUS,
          call_btn_status: false,
          call_btn_css: CALL_DEFAULT
        });
      } else {
        // if start call
        this.startCall(contactid, mobilephone, stationid);
        this.setState({
          call_status: this.state.stationid + " End Calling...",
          call_btn_status: true,
          call_btn_css: CALLING
        });
      }
    
  }

  startCall(contactid, mobilephone, stationid) {
    console.log( 'Start Call :',contactid, mobilephone, stationid);
    let $this = this;
    axios
      .get("/startcall", {
        params: {
          contact_id: contactid,
          mobile_phone: mobilephone,
          station_id: stationid // user station
        }
      })
      .then(function(response) {
        // var get = JSON.parse(data);
        console.log(response);
        $this.setState({
          call_id_display: "Call ID :" + response.data.result.data.call_id,
          call_id: response.data.result.data.call_id,
          engagement_id: response.data.add_engagment.engagement.id
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      });
  }
  endCall(contactid, mobilephone, stationid) {
    console.log('End Call',contactid, mobilephone, stationid);
    let $this = this;
    axios
      .get("/endCall", {
        params: {
          contact_id: contactid,
          mobile_phone: mobilephone,
          station_id: stationid,
          call_id: this.state.call_id,
          engagement_id: this.state.engagement_id
        }
      })
      .then(function(response) {
        // console.log(response);
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      });
    $this.setState({
      call_id_display: ""
    });
  }

  handleOptionChange(stationid , e){
    //console.log(stationid , e);
    this.setState({
      selectedOption: e.target.value,
      stationid: stationid
    });
  }

  //*************** render zone ***************\\
  render() {
    return (
      <div
        className="row"
        style={{ marginTop: "-15px", marginBottom: "-15px" }}
      >
        <div className="col-md-12">
          <label htmlFor="Call" className="control-label col-12">
            <input
              type="radio"
              value="option1"
              checked={this.state.selectedOption === 'option1'} 
              onChange={this.handleOptionChange.bind(this,this.state.station1)}
            />{" "}
            {this.props.station1}
          </label>
          <label htmlFor="Call" className="control-label col-12">
            <input
              type="radio"
              value="option2"
              checked={this.state.selectedOption === 'option2'}
              onChange={this.handleOptionChange.bind(this,this.state.station2 )}
            />{" "}
            {this.props.station2}
          </label>

          <hr />
          <div className="form-group">
            <label htmlFor="Call" className="control-label col-12">
              <button
                type="button"
                className={this.state.call_btn_css}
                onClick={this.onClickToCall.bind(this, this.state.stationid)}
                disabled={this.state.call_btn_disabled}
              >
                {this.state.call_status}
              </button>{" "}
            </label>
            <label htmlFor="call_id" className="control-label col-12">
              {this.state.call_id_display}
            </label>
          </div>
        </div>
      </div>
    );
  }
}

export default Call;
