import React from "react";
import Call from "../horus/Call";

class FormInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      phonenumber: "",
      get_error: ""
    };
  }
  componentDidMount(e) {
    let $this = this;
    axios
      .get("/api/contact_data/contact_get_by/" + this.props.main.props.phone)
      .then(function(response) {
        // handle success
        $this.setState({
          data: response.data,
          phonenumber: response.data.contact.phone.value
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
        $this.setState({
          get_error: "Please check data contact. / " + error
        });
      });
  }
  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  handleChangePhone(event) {
    this.setState({ phonenumber: event.target.value });
    //console.log(event.target.value);
  }

  //*************** render zone ***************\\
  render() {
    if (this.state.data.length == 0) {
      if (this.state.get_error != "") {
        return (
          <div>
            <strong>Error : </strong> this.state.get_error
          </div>
        );
      } else {
        return (
          <div>
            <strong>loading...</strong>
          </div>
        );
      }
    } else {
      return (
        <form action="#" method="get" className="form">
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="firstname" className="control-label col-12">
                  Firstname :{" "}
                  <input
                    type="text"
                    className="form-control"
                    defaultValue={
                      this.state.data.contact.firstname.value
                        ? this.state.data.contact.firstname.value
                        : ""
                    }
                  />
                </label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Lastname" className="control-label col-12">
                  Lastname :{" "}
                  <input
                    type="text"
                    className="form-control"
                    defaultValue={
                      this.state.data.contact.lastname.value
                        ? this.state.data.contact.lastname.value
                        : ""
                    }
                  />
                </label>
              </div>
            </div>
          </div>
          {/* *********************************************************************** */}
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Tel" className="control-label col-12">
                  Tel :{" "}
                  <input
                    type="text"
                    className="form-control"
                    onChange={this.handleChangePhone.bind(this)}
                    defaultValue={
                      this.state.data.contact.phone.value
                        ? this.state.data.contact.phone.value
                        : "no phone number"
                    }
                  />
                </label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Email" className="control-label col-12">
                  Email :{" "}
                  <input
                    type="email"
                    className="form-control"
                    defaultValue={
                      this.state.data.contact.email.value
                        ? this.state.data.contact.email.value
                        : ""
                    }
                    readOnly
                  />
                </label>
              </div>
            </div>
          </div>
          {/* *********************************************************************** */}
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Create at" className="control-label col-12">
                  Create at :{" "}
                  <input
                    type="text"
                    className="form-control"
                    value={
                      this.state.data.createdate
                        ? this.state.data.createdate
                        : ""
                    }
                    readOnly
                  />
                </label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Lastactivity" className="control-label col-12">
                  Lastactivity :{" "}
                  <input
                    type="text"
                    className="form-control"
                    value=""
                    readOnly
                  />
                </label>
              </div>
            </div>
          </div>
          {/* *********************************************************************** */}
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Call_quality" className="control-label col-12">
                  Call_quality :{" "}
                  <select
                    name="call_quality"
                    id="call_quality"
                    className="select2-drop form-control"
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="C3_quality" className="control-label col-12">
                  C3_quality :{" "}
                  <select
                    name="call_quality"
                    id="call_quality"
                    className="select2-drop form-control"
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </label>
              </div>
            </div>
          </div>
          <hr />
          {/* *********************************************************************** */}
          <div
            className="row"
            style={{ marginTop: "-15px", marginBottom: "-15px" }}
          >
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Owner" className="control-label col-12">
                  Owner :{" "}
                  <input
                    type="text"
                    className="form-control"
                    value={
                      this.state.data.get_owner.email
                        ? this.state.data.get_owner.email
                        : "No owner"
                    }
                    readOnly
                  />
                </label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="station1" className="control-label col-4">
                  station1 :{" "}
                  <input
                    type="text"
                    className="form-control"
                    value={
                      this.state.data.get_owner.station1
                        ? this.state.data.get_owner.station1
                        : "No set station !!"
                    }
                    readOnly
                  />
                </label>{" "}
                ||{" "}
                <label htmlFor="station2" className="control-label col-4">
                  station2 :{" "}
                  <input
                    type="text"
                    className="form-control"
                    value={
                      this.state.data.get_owner.station2
                        ? this.state.data.get_owner.station2
                        : "No set station !!"
                    }
                    readOnly
                  />
                </label>
              </div>
            </div>
          </div>
          {/* *********************************************************************** */}
          {/* <Call
            station1={this.state.data.get_owner.station1}
            station2={this.state.data.get_owner.station2}
            contactid={this.state.data.contact_id}
            mobilephone={this.state.phonenumber}
            formdata={this}
          /> */}
        </form>
      );
    }
  }
}

export default FormInfo;
