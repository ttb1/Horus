import React, { Component } from "react";

const STATUS_LOADING = "loading....";
const STATUS_PREPERING = "Prepering";
const STTAUS_READY = "ready";
class ExamInfomation extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      score_exam:[],
      contact_id: "",
      status_action: STATUS_PREPERING,
      user: "",
      password: "",
      status: "",
      exam_score_display: true
    };
  }

  componentDidMount() {
    // console.log(this.props );
    let $this = this;
    axios
      .get("/api/exam_data/get/" + this.props.contactid )
      .then(function(response) {
        console.log('componentDidMount ExamInfomation',response);
        if (response.data.exam == null) return;
        $this.setState({
          data: response.data.exam,
          user: response.data.exam.username,
          password: response.data.exam.password,
          status: response.data.exam.status,
          status_action: STTAUS_READY
        });
        if (response.data.score_exam == null) return
        $this.setState({
          score_exam :response.data.score_exam,
          exam_score_display: false
        });

      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleGenerate(e) {
    let deal_id = this.props.main.level_contact.deal_id;
    if (this.state.status == STATUS_LOADING) {
      alert("Please wait a minute !!");
      return;
    }
    let confirmMs = confirm("Get new user and pass ?");
    if (!confirmMs) return;
    this.setState({
      status_action: STATUS_LOADING
    });

    let $this = this;
    axios
      .get("/api/exam_genarate_pass/" + this.props.contactid +'/'+deal_id)
      .then(function(response) {
        // var get = JSON.parse(data);
       console.log(response);
        $this.setState({
          status_action: STTAUS_READY,
          user: response.data.exam.username,
          password: response.data.exam.password,
          status: response.data.exam.confirmed
        });
      })
      .catch(function(error) {
        // handle error
        $this.setState({
          status_action: STATUS_PREPERING
        });
        console.log(error);
      });
  }

  onOpenHistory(e) {
    window.open(
      "https://www.google.com",
      "_blank",
      "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1200%,height=600"
    );
  }
  render() {
    //console.log(this.props);

    return (
      <div className="form-group row">
        <div className="col-12">
          <label htmlFor="exam information" className="control-label">
            <strong>Exam information </strong>
          </label>
          <br />
        </div>

        <div className="col-3 border border-left-0 border-bottom-0 border-top-0">
          <label htmlFor="exam information Status" className="control-label">
            <strong>Status </strong>
          </label>{" "}
          : {this.state.status_action}
          <br />
          <button
            className="btn btn-grey"
            onClick={this.handleGenerate.bind(this)}
          >
            Generate
          </button>
          <br />
        </div>
        <div className="col-9">
          <table className="table table-sm">
            <thead>
              <tr>
                <th>username</th>
                <th>password</th>
                <th>status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.state.user}</td>
                <td>{this.state.password}</td>
                <td>{this.state.status}</td>
              </tr>
            </tbody>
          </table>
          <div className="col-12 text-right">
            <button
              className="pull-right btn btn-link"
              type="button"
              onClick={this.onOpenHistory.bind(this)}
            >
              view history
            </button>
          </div>
        </div>

        {/* SCORE SECTION */}
        <div className="col-12" hidden={this.state.exam_score_display} >
          <label htmlFor="exam Score" className="control-label">
            <strong>Exam Score </strong>
          </label>
          <table className="table table-bordered text-center">
            <thead>
              <tr>
                <th>date</th>
                <th>คำศัพท์</th>
                <th>ไวยกรณ์</th>
                <th>การฟัง</th>
                <th>การสะกด</th>
                <th>toeic</th>
                <th>topica_level</th>
                <th>รวม</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{this.state.score_exam.tested_at?this.state.score_exam.tested_at:''}</td>
                <td>{this.state.score_exam.s1?this.state.score_exam.s1:''}</td>
                <td>{this.state.score_exam.s2?this.state.score_exam.s2:''}</td>
                <td>{this.state.score_exam.s3?this.state.score_exam.s3:''}</td>
                <td>{this.state.score_exam.s4?this.state.score_exam.s4:''}</td>
                <td>{this.state.score_exam.toeic?this.state.score_exam.toeic:''}</td>
                <td>{this.state.score_exam.topica_level?this.state.score_exam.topica_level:''}</td>
                <td>{this.state.score_exam.total?this.state.score_exam.total:''}</td>
              </tr>
            </tbody>
          </table>
        </div>
        {/* SCORE SECTION */}
      </div>
    );
  }
}
export default ExamInfomation;
