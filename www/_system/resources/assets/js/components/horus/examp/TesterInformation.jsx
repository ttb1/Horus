import React, { Component } from "react";

const STATUS_LOADING = "loading....";
const STATUS_PREPERING = "Prepering";
const STTAUS_READY = "ready";

class TesterInformation extends React.Component {
  constructor() {
    super();
    this.state = {
      startDate: "",
      data: [],
      contact_id: "",
      status_action: STATUS_PREPERING,
      tester_status: [],
      time_schedule: [],
      tester_score: [],
      description: "",
      interviewstime: 1,
      interviewsdate: "",
      teacher: 1,
      schedulebtndisable: false,
      score_display: true,
      teacher_comment: "",
    };
  }

  componentWillMount() {
    //console.log('componentWillMount', this.props.main.level_contact.deal_id)
  }

  componentDidMount() {
    let $this = this;
    axios
      .get("/api/tester/get/" + this.props.contactid)
      .then(function(response) {
      // console.log(response);
        // console.log(response.data.interview_data.teacher_comment );
        // console.log(response.data.interview_data.date);
        if (response.data == null) return;
        $this.setState({
          data: response.data,
          contact_id: response.data.contactid,
          tester_status: response.data.tester_status,
          time_schedule: response.data.time_schedule,
          interviewsdate: response.data.interview_date,
        });
        if (response.data.interview_data != null) {
          $this.setState({
            interviewstime: response.data.interview_data.time_schedule_id,
            teacher: response.data.interview_data.teacher_type_id,
            teacher_comment: response.data.interview_data.teacher_comment
          });
          if (response.data.interview_data.teacher_status_id == 1) {
            $this.setState({
              schedulebtndisable: true,
              status_action: STTAUS_READY
            });
          }
        }
        if (response.data.tester_score != null) {
          $this.setState({
            tester_score: response.data.tester_score,
            score_display: false
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  onChangeTextarea(e) {
    // console.log(e.target.value)
    this.setState({
      description: e.target.value
    });
  }

  onChangeInterviewsdate(e) {
    this.setState({
      interviewsdate: e.target.value
    });
  }
  onChangeInterviewstime(e) {
    console.log(e.target.value);
    this.setState({
      interviewstime: e.target.value
    });
  }
  onChangeTeacher(e) {
    // console.log(e.target.value);
    this.setState({
      teacher: e.target.value
    });
  }
  onClickUpdate() {
    this.setState({
      status_action: STATUS_LOADING
    });
    let time_schedule = this.state.interviewstime;
    let date_schedule = this.state.interviewsdate;
    let description = this.state.description;
    let teacher = this.state.teacher;
    let $this = this;
    console.log(time_schedule, date_schedule, description, teacher);
    axios
      .get("/api/tester/add/" + this.props.contactid+'/'+this.props.main.level_contact.deal_id, {
        params: {
          time_schedule: time_schedule,
          date_schedule: date_schedule,
          description: description,
          teacher: teacher,
        }
      })
      .then(function(response) {
        console.log(response);
        if (response.data == null) return;

        $this.setState({
          status_action: response.data.response.msg
        });

        // $this.setState({
        //   status_action: STTAUS_READY
        // });
      })
      .catch(function(error) {
        console.error(error.response);
        $this.setState({
          status_action: error.response.statusText + " try again"
        });
      });
  }

  onOpenSchedule(e){
    window.open("http://testerdev.topicanative.asia/view_schedule_sale","_blank",
    "toolbar=yes,scrollbars=yes,resizable=yes,top=100,left=500,width=1200%,height=600");
  }

  render() {
    return (
      <div className="form-group row">
        <div className="col-12">
          <label htmlFor="interview schedule" className="control-label">
            <strong>Interview schedule </strong>
          </label>
          <br />
          <label htmlFor="interview schedule Status" className="control-label">
            <strong>Status </strong>
          </label>
          : {this.state.status_action}
          <br />
          <div className="form-inline">
            <div className="form-group">
              <label htmlFor="interview date" className="mr-sm-2">
                <strong> interview date : </strong>
              </label>
              <input
                type="text"
                className="form-control form-control-sm mr-sm-2"
                data-provide="datepicker"
                id="datepicker"
                name="email"
                autoComplete="off"
                defaultValue={this.state.interviewsdate}
                onSelect={this.onChangeInterviewsdate.bind(this)}
                disabled={this.state.schedulebtndisable}
              />
            </div>

            <div className="form-group">
              <label htmlFor="time" className="mr-sm-2">
                <strong> time : </strong>
              </label>
              <select
                className="form-control form-control-sm mr-sm-2"
                id="sel1"
                value={this.state.interviewstime}
                onChange={this.onChangeInterviewstime.bind(this)}
                disabled={this.state.schedulebtndisable}
              >
                {this.state.time_schedule
                  ? this.state.time_schedule.map((item, i) => (
                      <option value={item.id} key={i}>
                        {/* selected={ item.id==3?true:false } */}
                        {item.time_period}
                      </option>
                    ))
                  : ""}
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="teacher" className="mr-sm-2">
                <strong> teacher : </strong>
              </label>
              <select
                className="form-control form-control-sm mr-sm-2"
                value={this.state.teacher}
                onChange={this.onChangeTeacher.bind(this)}
                disabled={this.state.schedulebtndisable}
              >
                <option value="1">Native</option>
                <option value="2">Local</option>
              </select>
            </div>
            
            <div className="form-group">
                    <button type="button" onClick={this.onOpenSchedule.bind(this)} className="btn btn-link">view schedule</button>
            </div>
          </div>
          <div className="form-horizontal">
            <div className="form-group">
              <label htmlFor="description" className="">
                <strong> Description : </strong>
              </label>
              <textarea
                onChange={this.onChangeTextarea.bind(this)}
                className="form-control"
                style={{ resize: "none" }}
                disabled={this.state.schedulebtndisable}
              />
            </div>
          </div>
          {/* SCORE SECTION */}
          <div hidden={this.state.score_display}>
          <strong>Interview Score</strong>
            <table className="table table-bordered">
              <thead>
                <tr className="text-center">
                  <th>date</th>
                  <th>การเชื่อมโยง</th>
                  <th>คําศัพท์</th>
                  <th>ไวยากรณ์</th>
                  <th>การออกเสียง</th>
                  <th>ระดับการพูด</th>
                  <th>รวม</th>
                </tr>
              </thead>
              <tbody className="text-center">
                <tr>
                  <td>
                    {this.state.tester_score.interviewed_at
                      ? this.state.tester_score.interviewed_at
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.s1
                      ? this.state.tester_score.s1
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.s2
                      ? this.state.tester_score.s2
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.s3
                      ? this.state.tester_score.s3
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.s4
                      ? this.state.tester_score.s4
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.speaking_level
                      ? this.state.tester_score.speaking_level
                      : ""}
                  </td>
                  <td>
                    {this.state.tester_score.total
                      ? this.state.tester_score.total
                      : ""}
                  </td>
                </tr>
              </tbody>
            </table>
            <div className="form-horizontal">
              <div className="form-group">
                <label htmlFor="teacher_comment" className="">
                  <strong> Teacher comment : </strong>
                </label>
                <textarea
                  className="form-control"
                  style={{ resize: "none" }}
                  disabled={true}
                  value={
                    this.state.teacher_comment ? this.state.teacher_comment : ""
                  }
                />
              </div>
            </div>
          </div>
          {/* SCORE SECTION */}
          <button
            className="btn btn-grey"
            disabled={this.state.schedulebtndisable}
            onClick={this.onClickUpdate.bind(this)}
          >
            Update
          </button>
        </div>
      </div>
    );
  }
}

export default TesterInformation;
