import React, { Component } from "react";
import ReactDOM from "react-dom";
import Call from "./horus/Call";
import CustomerInfo from "./horus/CustomerInfo";
import CustomerData from "./horus/CustomerData";
import ExampData from "./horus/ExampData";

export default class Horus extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      phonenumber: "",
      level_contact:''
    };
  }

  componentDidMount(e) {
    let $this = this;
    axios
      .get("/api/contact_data/contact_get_by/" + this.props.phone)
      .then(function(response) {
        // handle success
        console.log('Horus',response.data);
        $this.setState({
          data: response.data,
          phonenumber: response.data.contact.phone.value,
          level_contact: response.data.level_contact
        });
        if (response.data.level_contact.level_contact == 'L4' || response.data.level_contact.level_contact == 'L5' || response.data.level_contact.level_contact == 'L6' || response.data.level_contact.level_contact == 'L6.5' ) {
          window.scrollTo( 0, 1200 );
        }
        // window.scrollTo( 0, 1120 );
      })
      .catch(function(error) {
        // handle error
        console.log(error);
        $this.setState({
          get_error: "Please check data contact. / " + error
        });
      });
  }

  render() {
  //  console.log("render", this.state.data);
    var data = this.state.data;
    if (this.state.data.length == 0) {
      return (
        <div className="container center">
          <br /> <legend>Loading...</legend>
        </div>
      );
    } else {
      return (
        <div className="row">
          {/* section 1 */}
          <div className="col-md-9">
            <div className="card">
              {/* <div className="card-header">
                <center>
                  <legend>
                    <h1>Horus</h1>
                  </legend>
                </center>
              </div> */}
              <div className="card-body">
                {/* Customer information section */}
                <CustomerInfo main={data} />
                <br/>
                {/* Customer data section 1 */}
                <CustomerData main={data}/>
                <br/>
                <ExampData main={data} />

              </div>
            </div>
          </div>
          {/* section 2 */}
          <div className="col-md-2">
            <div className="fixed">
              <div className="card">
                <div className="card-header">
                  <legend>Call : {this.state.phonenumber}</legend>
                </div>
                <div className="card-body">
                  <Call
                    station1={this.state.data.get_owner.station1}
                    station2={this.state.data.get_owner.station2}
                    contactid={this.state.data.contact_id}
                    mobilephone={this.state.phonenumber}
                    formdata={this}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

if (document.getElementById("horus")) {
  // console.log(document.getElementById("phone").value);
  let phone = document.getElementById("phone").value;
  ReactDOM.render(<Horus phone={phone} />, document.getElementById("horus"));
}
