import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class Hrcall extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      phonenumber: "",
      level_contact: '',
      Loading: true,
      call_status: 'Call',
      css_call: 'btn btn-lg bg-success',
      station_id: null,
      tel: null
    };
  }

  componentWillMount() {
    this.setState({
      Loading: true
    });
  }
  componentDidMount(e) {

    this.setState({
      Loading: false
    });
  }

  onCall(e) {
    if (this.state.station_id == null) {
      alert('please check you station-id !!!!');
      return
    }
    if (this.state.tel == null) {
      alert('please check you phone number !!!!');
      return
    }

    if (this.state.call_status == 'Calling....') {
      this.setState({
        call_status: 'Call',
        css_call: 'btn btn-lg bg-success',
      })
    } else {
      console.log('tel :' , this.state.tel)
      console.log('station :' , this.state.station_id)
      this.setState({
        call_status: 'Calling....',
        css_call: 'btn btn-lg bg-danger',
      })
      this.calling(this);
    }
  }

  calling(e){
    let $this = this;
    axios
      .get("api/startcall", {
        params: {
          phone: this.state.tel,
          station_id: this.state.station_id // user station
        }
      })
      .then(function(response) {
        // var get = JSON.parse(data);
        console.log(response);
     
      })
      .catch(function(error) {
        // handle error
        alert('something wrong !!! , Please try again (check you phone number or stationID)')
        $this.setState({
          call_status: 'Call',
          css_call: 'btn btn-lg bg-success',
        })
        console.log(error);
      });
  }

  handleStationChange(e) {
    //console.log(e.target.value);
    this.setState({
      station_id:e.target.value
    })
  }
  handleTelChange(e) {
    //console.log(e.target.value);
    this.setState({
      tel:e.target.value
    })
  }
  render() {
    if (this.state.Loading) {
      return (
        <div className="container">
          <center>
            <legend>Loading...</legend>
          </center>
        </div>
      );
    } else {
      return (
        <div className="container">

          <form className="form form-horizontal">
            <div className="form-group-sm">
              <legend>Station ID
              <input htmlFor="station id" placeholder="8001-8010" type="number" className="form-control"
                  onChange={this.handleStationChange.bind(this)}
                />
              </legend>
            </div>
            <div className="form-group-sm">
              <legend>phone
                <input type="tel" className="form-control"
                  onChange={this.handleTelChange.bind(this)} />
              </legend>
              <button type="button" onClick={this.onCall.bind(this)} className={this.state.css_call} >{this.state.call_status} </button>
            </div>
          </form>

        </div>
      );
    }
  }
}

if (document.getElementById("hrcall")) {
  ReactDOM.render(<Hrcall />, document.getElementById("hrcall"));
}
