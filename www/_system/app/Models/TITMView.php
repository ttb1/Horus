<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TITMView extends Model
{
    
    use SoftDeletes;
    
    protected $table = 'TITM_VIEW';
    
    protected $fillable = [
        'batch_name',
        'department',
        'receive',
        'refuse',
        't7',
        't14',
        't21',
        't30',
        't60',
        't90',
        't7_per',
        't14_per',
        't21_per',
        't30_per',
        't60_per',
        't90_per'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
