<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class NTLAdvisorRaw extends Model
{

    use SoftDeletes;
    
    protected $table = 'xx';
    
    protected $fillable = [
        "name",
        "level",
        "email",
        "telephone",
        "package",
        "purchase_date",
        "activate_status_and_date",
        "package_amout_and_expired_date",
        "advisor",
        "lms_date",
        "note",
        "contact_id",
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
 
    
}
