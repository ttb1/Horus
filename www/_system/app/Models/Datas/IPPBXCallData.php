<?php

namespace App\Models\Datas;

use App\Http\Controllers\CacheController;

class IPPBXCallData extends DataModel
{    
    public $message_code;
    public $agent_code;
    public $station_id;
    public $mobile_phone;
    public $datetime_response;
    public $call_id;
    public $start_time;
    public $end_time;
    public $duration;
    public $ringtime;
    public $link_down_record;
    public $status;
    public $error_code;
    public $error_desc;
    
    public function __construct($data = null, $is_strict = false) {
        
        parent::__construct($data, $is_strict);
        
    }
    
}
