<?php

namespace App\Models\Datas;

use App\Http\Controllers\CacheController;

class ResponseData extends DataModel
{    
    public $timestamp;
    
    public function __construct($data = null, $is_strict = false) {
        
        parent::__construct($data, $is_strict);
        
    }
    
}
