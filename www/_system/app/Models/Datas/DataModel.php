<?php

namespace App\Models\Datas;

class DataModel
{
    
    public function __construct(array $data = null, $is_strict = false){
        
        $this->updateData($data, $is_strict);
        
    }
    
    public function updateData(array $data = null, $is_strict = false) {
        
        $reflectionClass = new \ReflectionClass($this);

        foreach ($reflectionClass->getProperties() as $property) {
            $value = null;
            if(isset($data[$property->getName()]) || $is_strict) {
                $value = $data[$property->getName()];
            }
            $property->setAccessible(true);
            $property->setValue($this, $value);
        }
        
    }
    
    public function toArray() {
        return (array)$this;
    }
    
    public function toCollection() {
        return new \Illuminate\Support\Collection($this->toArray());
    }
    
}
