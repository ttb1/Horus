<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdvisorActivateRate extends Model
{
    use SoftDeletes;
    
    protected $table = 'advisor_activate_rate';
    
    protected $primaryKey = 'date';
    
    protected $fillable = [
        'date','active_hv','total_hv',
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];

    
}
