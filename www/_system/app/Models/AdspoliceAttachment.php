<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdspoliceAttachment extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'adspolice_attachment';
    
    protected $fillable = [
        'adspolice_id','url','path','workflow_name'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    
    public function adspolice()
    {
        return $this->belongsTo('App\Models\Adspolice');
    }
}
