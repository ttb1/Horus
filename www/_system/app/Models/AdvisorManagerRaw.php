<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdvisorManagerRaw extends Model
{
    use SoftDeletes;
    
    protected $table = 'advisor_manager_raw';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];

    
}
