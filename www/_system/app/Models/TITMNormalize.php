<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TITMNormalize extends Model
{
    
    protected $table = 'TITM_NORMALIZE';
    
    protected $fillable = [
        'issue_current_status_name',
        'TAT_The_First_Working_Day'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
