<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdvisorReportView extends Model
{
    use SoftDeletes;
    
    protected $table = 'advisor_report_view';
    
    protected $primaryKey = 'date';
    
    protected $fillable = [
        'date', 'new_account', 'expired', 'hv_activited',
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];

    
}
