<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdspoliceComment extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'adspolice_comment';
    
    protected $fillable = [
        'adspolice_id', 'comment', 'author_email'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    public function adspolice()
    {
        return $this->belongsTo('App\Models\Adspolice');
    }
    
    
}
