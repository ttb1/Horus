<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdspoliceCustomfield extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'adspolice_customfield';
    
    protected $fillable = [
        'adspolice_id', 'customfield', 'value'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    public function adspolice()
    {
        return $this->belongsTo('App\Models\Adspolice');
    }
    
    
}
