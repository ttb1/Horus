<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Adspolice extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'adspolice';
    
    protected $fillable = [
        'key_branding', 'key_marketing'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
    public function attachment()
    {
        return $this->hasMany('App\Models\AdspoliceAttachment');
    }
    
    public function customfield()
    {
        return $this->hasMany('App\Models\AdspoliceCustomfield');
    }
    public function comment()
    {
        return $this->hasMany('App\Models\AdspoliceComment');
    }
    
}
