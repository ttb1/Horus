<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AdvisorTransition extends Model
{
    use SoftDeletes;
    
    protected $table = 'advisor_transition';
    
    protected $fillable = [
        'lsm_id','status','date',
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];

    
}
