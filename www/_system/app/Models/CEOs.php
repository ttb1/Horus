<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class CEOs extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'ceos';
    
    protected $fillable = [
        'id', 'name', 'phone', 'email', 'age', 'link', 'file_cv', 'source'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
 
    
}
