<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_callhistories extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Callhistories';
    
    protected $primaryKey = 'CallHistoryId';

    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
    
 
    
}
