<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_phones extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Phones';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
