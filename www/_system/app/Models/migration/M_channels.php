<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_channels extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Channels';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
