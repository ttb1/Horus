<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_core_users extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Core_users';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
    
    public function core_useringroups() {
        return $this->hasOne('App\Models\migration\M_core_useringroups', 'UserId', 'UserID');
    }
    
}
