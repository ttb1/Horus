<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_levels extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Levels';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
