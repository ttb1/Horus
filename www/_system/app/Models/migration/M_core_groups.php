<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_core_groups extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Core_groups';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
    
    public function core_useringroups() {
        return $this->hasOne('App\Models\migration\M_core_useringroups', 'GroupID', 'GroupID');
    }
    
}
