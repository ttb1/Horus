<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_importexcels extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Importexcels';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
