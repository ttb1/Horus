<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_contacts extends Model
{

    protected $connection = 'sqlsrv';

    protected $table = 'Contacts';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
