<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_core_useringroups extends Model
{
    
    protected $connection = 'sqlsrv';

    protected $table = 'Core_useringroups';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
    
    public function core_users() {
        return $this->belongsTo('App\Models\migration\M_core_users');
    }
    public function core_groups() {
        return $this->belongsTo('App\Models\migration\M_core_groups');
    }
    
}
