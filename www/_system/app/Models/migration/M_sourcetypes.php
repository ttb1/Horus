<?php

namespace App\Models\migration;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_sourcetypes extends Model
{
    protected $connection = 'sqlsrv';

    protected $table = 'Sourcetypes';
    
    protected $fillable = [
    ];
    
    protected $hidden = [
    ];
  
    
}
