<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LogContactTransfer extends Model
{
    
    const FROM_RAW = 'from raw';
    const FROM_USER = 'from user';
    
    use SoftDeletes;
    
    protected $table = 'log_contact_transfer';
    
    protected $fillable = [
        'action', 'object', 'user_id', 'contact_id'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
