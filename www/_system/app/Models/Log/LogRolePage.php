<?php 

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LogRolePage extends Model
{
    
    use SoftDeletes;

    protected $table = 'log_role_page';

    protected $fillable = [
        'action', 'object', 'user_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];
}
?>