<?php 

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SGLogBox extends Model
{
    
    use SoftDeletes;

    protected $table = 'log_box';

    protected $fillable = [
        'box_id',
        'temperature',
        'relative_humidity',
        'sent_at'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];
    
    public function box()
    {
        return $this->belongsTo('App\Models\SGBox');
    }
    
}
?>