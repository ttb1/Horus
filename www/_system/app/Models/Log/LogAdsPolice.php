<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class LogAdsPolice extends Model
{

    
    use SoftDeletes;
    
    protected $table = 'log_adspolice';
    
    protected $fillable = [
        'action', 'object', 'data'
    ];
    
    protected $hidden = [
         'created_at', 'updated_at','deleted_at',
    ];
    
}
