<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class AdvisorController extends Controller
{
    public function index() {
        return 'Advisor Index';
    }
    
    public function activateRate(){
        
        $raw_data = \App\Models\AdvisorManagerRaw::where('DATE','01-10-2019')->get();
        foreach ($raw_data as $item)
        {
            // Active 
            if ( $item->STATUS_LAST != 'deactived')
            {
                $date_activity = Carbon::createFromFormat('m-d-Y', $item->STATUS_LAST)->format('Y-m-d');
                if ( $item->STATUS_LAST != 'deactived')
                {
                    $date_activity = Carbon::createFromFormat('m-d-Y', $item->STATUS_LAST)->format('Y-m-d');
                    $activate_rate = \App\Models\AdvisorActivateRate::where('date',$date_activity)->first();
                    if (is_null($activate_rate))
                    {
                        $activate_rate_new = new \App\Models\AdvisorActivateRate();
                        $activate_rate_new->date = $date_activity;
                        $activate_rate_new->active_hv = 1;
                        $activate_rate_new->save();
                        echo 'new ' .$date_activity . '<br>';
                    }
                    else
                    {
                        $activate_rate->active_hv = $activate_rate->active_hv + 1;
                        $activate_rate->save();
                        echo 'update ' .$date_activity . ' ' . $activate_rate->active_hv . '<br>';
                    } 
                }
            }
            
            //total
        }
        
        return ' activateRate ' ;
    }
    
    
    
    
    
    
    public function test(){
        $raw_data = \App\Models\AdvisorManagerRaw::where('DATE','01-10-2019')->get();
        foreach ($raw_data as $item){
            
            // new account 
            $date_purchase = Carbon::createFromFormat('m-d-Y', $item->PURCHASE_DATE)->format('Y-m-d');
            $report = \App\Models\AdvisorReportView::where('date',$date_purchase)->first();
            if (is_null($report))
            {
                $report_view = new \App\Models\AdvisorReportView();
                $report_view->date = $date_purchase;
                $report_view->new_account = 1;
                $report_view->save();
                echo 'new ' .$date_purchase . '<br>';
            }
            else
            {
                $report->new_account = $report->new_account + 1;
                $report->save();
                echo 'update ' .$date_purchase . ' ' . $report->new_account . '<br>';
            }
            
            // hv activity 
            if ( $item->STATUS_LAST != 'deactived')
            {
                $date_activity = Carbon::createFromFormat('m-d-Y', $item->STATUS_LAST)->format('Y-m-d');
                $report = \App\Models\AdvisorReportView::where('date',$date_activity)->first();
                if (is_null($report))
                {
                    $report_view = new \App\Models\AdvisorReportView();
                    $report_view->date = $date_activity;
                    $report_view->hv_activited = 1;
                    $report_view->save();
                    echo 'new ' .$date_activity . '<br>';
                }
                else
                {
                    $report->hv_activited = $report->hv_activited + 1;
                    $report->save();
                    echo 'update ' .$date_activity . ' ' . $report->hv_activited . '<br>';
                } 
            }
            
            //expired 
            $date_expired ;
            if ( strpos($item->PACKAGE_STUDY, 'THAI-TT') !== false)
            {
                // format date m-d-Y
                $date_expired = $item->EXPIRY_DATE;
            } 
            else if ( strpos($item->PACKAGE_STUDY, 'THAI-TC') !== false)
            {
                // format '12400/05-10-2019'
                $date_expired = substr($item->EXPIRY_DATE,strpos($item->EXPIRY_DATE, '/')+1);
            }
            if ( $date_expired != 'n/a' && !empty($date_expired) )
            {
                echo  $item->LSM_ID.' '.$date_expired .'<br>';
                try {
                    $date_expired = Carbon::createFromFormat('m-d-Y', $date_expired)->format('Y-m-d');
                    $report = \App\Models\AdvisorReportView::where('date',$date_expired)->first();
                    if (is_null($report))
                    {
                        $report_view = new \App\Models\AdvisorReportView();
                        $report_view->date = $date_expired;
                        $report_view->expired = 1;
                        $report_view->save();
                        echo 'new ' .$date_expired . '<br>';
                    }
                    else
                    {
                        $report->expired = $report->expired + 1;
                        $report->save();
                        echo 'update ' .$date_expired . ' ' . $report->expired . '<br>';
                    } 
                } catch (\Exception $ex) {
                    echo 'error';
                }
               

            }
            
                
            
            
        }
            
       
                
//        $collection_report = collect();
//        $collection_report->push(New \App\Models\AdvisorReportView(['expired'=>5]));
//        $collection_report->push(New \App\Models\AdvisorReportView(['expired'=>6]));
        
    }
    
    public function import() {
        return view('/advisor/import', []);
    }
    
    public function storeByImport(Request $request)
    {
        //dd($request->file('file'), $request->file('import'));
        
        //$path = $request->file('file')->store('');
        
        //$path = UploadController::upload($request);
        
        
        $path = $request->file('import')->store('');
        
        ini_set('max_execution_time', 300);
        
        $file = storage_path('app/' . $path);
        
        $file_type = IOFactory::identify($file);
        $reader = IOFactory::createReader($file_type);
        $reader->setReadDataOnly(true);
        $spreadsheets = $reader->load($file);
        $worksheet = new Worksheet();
        $worksheet = $spreadsheets->getActiveSheet();
        
        $row = $worksheet->getHighestDataRow()+1;
        
        for($i = 7 ; $i < $row ; $i++) {   
            // new account
            $advisor_transition = new \App\Models\AdvisorTransition([
                'lsm_id' => $worksheet->getCell('B'.$i)->getValue(),
                'status' => 'New Account',
                'date' => Carbon::createFromFormat('m-d-Y', $worksheet->getCell('J'.$i)->getValue())->format('Y-m-d'),

            ]);
            $advisor_transition->save();
            
            // Active
            if ($worksheet->getCell('K'.$i)->getValue() != 'deactived')
            {
                $advisor_transition = new \App\Models\AdvisorTransition([
                    'lsm_id' => $worksheet->getCell('B'.$i)->getValue(),
                    'status' => 'Active',
                    'date' => Carbon::createFromFormat('m-d-Y', $worksheet->getCell('K'.$i)->getValue())->format('Y-m-d'),
                ]);
                $advisor_transition->save();
            }
            
            
            // Expiry
            if ($worksheet->getCell('L'.$i)->getValue() != '' && $worksheet->getCell('L'.$i)->getValue() != 'n/a')
            {
                if (strpos($worksheet->getCell('I'.$i)->getValue(), 'THAI-TT') == false)
                {
                    try {
                        $advisor_transition = new \App\Models\AdvisorTransition([
                            'lsm_id' => $worksheet->getCell('B'.$i)->getValue(),
                            'status' => 'Expiry',
                            'date' => Carbon::createFromFormat('m-d-Y',substr($worksheet->getCell('L'.$i)->getValue(), strpos($worksheet->getCell('L'.$i)->getValue(),'/')+1))->format('Y-m-d'),
                        ]);
                    $advisor_transition->save();
                    } catch (\Exception $ex) {
                        echo 'LMS ID error Expiry:'.$worksheet->getCell('B'.$i)->getValue().'<br>';
                    }
                    
                }
                else if (strpos($worksheet->getCell('I'.$i)->getValue(), 'THAI-TC') == false)
                {
                    
                    try {
                        $advisor_transition = new \App\Models\AdvisorTransition([
                            'lsm_id' => $worksheet->getCell('B'.$i)->getValue(),
                            'status' => 'Expiry',
                            'date' => Carbon::createFromFormat('m-d-Y', $worksheet->getCell('L'.$i)->getValue())->format('Y-m-d'),
                        ]);
                        $advisor_transition->save(); 
                    } catch (\Exception $ex) {
                       echo 'LMS ID error Expiry:'.$worksheet->getCell('B'.$i)->getValue().'<br>';
                    }
                }
                
            }
            
            
//            $worksheet_data = [
//                "name" => $worksheet->getCell('C'.$i)->getValue(),
//                "level" => str_replace(' ', '', $worksheet->getCell('E'.$i)->getValue()),
//                "email" => str_replace(' ', '', $worksheet->getCell('F'.$i)->getValue()),
//                "telephone" => str_replace(' ', '', $worksheet->getCell('G'.$i)->getValue()),
//                "package" => str_replace(' ', '', $ดปกworksheet->getCell('I'.$i)->getValue()),
//                "purchase_date" => str_replace(' ', '', $worksheet->getCell('J'.$i)->getValue()),
//                "activate_status_and_date" => str_replace(' ', '', $worksheet->getCell('K'.$i)->getValue()),
//                "package_amout_and_expired_date" => str_replace(' ', '', $worksheet->getCell('L'.$i)->getValue()),
//                "advisor" => $worksheet->getCell('O'.$i)->getValue(),
//                "lms_date" => str_replace(' ', '', $worksheet->getCell('P'.$i)->getValue()),
//                "note" => $worksheet->getCell('Q'.$i)->getValue(),
//                "contact_id" => $worksheet->getCell('R'.$i)->getValue(),
//            ];
//            
//            dd(new \App\Models\NTLAdvisorRaw($worksheet_data));
            
        }
        
   
    }
    
}
