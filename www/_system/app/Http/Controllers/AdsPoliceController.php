<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Models\Datas\WorksheetData;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use Illuminate\Support\Facades\File;

use Carbon\Carbon;


interface iAdsPoliceController {
    
}

class AdsPoliceController extends Controller implements iAdsPoliceController
{
    const AMOUNT_ORDER_PER_PAGE = 15;
    const USERNAME = 'chatchai';
    const PASSWORD = 'topica123';
    
    const CREATE_USER_URL = 'http://jirabkk.topica.asia/rest/api/2/user';
    const ADD_USER_TO_GROUP_URL = 'http://jirabkk.topica.asia/rest/api/2/group/user?groupname=confluence-users';
    
    public function index( ) {
        return view('/adsPolice/import', []);
    }
    
    public function import(Request $request)
    {

        $path = UploadController::upload($request);
        
        //echo $path."<br>";
        
        if (empty($path)) {
            return redirect('ads_police')
            ->withErrors('Import fail, Please select file');
        }

        ini_set('max_execution_time', 300);
        
        $file = storage_path('app/' . $path);
        $reader = new CSV();
        $worksheet = $reader->load($file)->getActiveSheet();
        $row = $worksheet->getHighestRow()+1;
        
        for($i = 2 ; $i < $row ; $i++) {
            
            $worksheet_data = [
                "name" => str_replace(' ', '', $worksheet->getCell('B'.$i)->getValue()),
                "displayName" => str_replace(' ', '', $worksheet->getCell('C'.$i)->getValue()),
                "emailAddress" => str_replace(' ', '', $worksheet->getCell('D'.$i)->getValue()),
                "password" => str_replace(' ', '', $worksheet->getCell('E'.$i)->getValue())
            ];
            
            $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ]]);
            
            $log_ads_police = new \App\Models\Log\LogAdsPolice();
            $log_ads_police->action = 'Log';            
            $log_ads_police->data = json_encode($worksheet_data);
            
            //Add user
            try{
                $response = $client->post(self::CREATE_USER_URL, [
                    'auth' => [
                        self::USERNAME, 
                        self::PASSWORD
                    ],
                    'json' => $worksheet_data

                ]); 
                
                echo 'user: '.$worksheet_data['name'].' password: '.$worksheet_data['password']."<br>";
                
                //Add User to confluence-user group
                try{
                    $response = $client->post(self::ADD_USER_TO_GROUP_URL, [
                        'auth' => [
                            self::USERNAME, 
                            self::PASSWORD
                        ],
                        'json' => $worksheet_data

                    ]); 

                    $log_ads_police->object = 'complete';
                    $log_ads_police->save();

                } catch(ClientException  $exception) {

                    $log_ads_police->object = 'error_user_confluence';
                    $log_ads_police->save();

                    $response = $exception->getResponse();
                    echo $response->getBody()->getContents().'<br>';
                }            
            //echo $response->getBody()->getContents().'<br>';
                
            } catch(ClientException  $exception) {
                
                $log_ads_police->object = 'error_user_existed';
                $log_ads_police->save();
                
                $response = $exception->getResponse();
                echo $response->getBody()->getContents().'<br>';
            }
        }
    }
    
    public function show(Request $request)
    {
                
        if ( $request->from_date != '' && $request->to_date != '')
        {
            $to = Carbon::parse($request->to_date)->addDay(1);
            $from = Carbon::parse($request->from_date);
            $adspolice_list = \App\Models\Adspolice::whereBetween('created_at', [ $from , $to ])->paginate(self::AMOUNT_ORDER_PER_PAGE);;
        }
        else
        {
            $adspolice_list = \App\Models\Adspolice::paginate(self::AMOUNT_ORDER_PER_PAGE);;
        }
        return view('adsPolice.show', [
            'adspolice_list' => $adspolice_list,
            'old_request' => $request,
        ]);
    }
    
    public function showID($id)
    {
        $adspolice = \App\Models\Adspolice::find($id);
        return view('adsPolice.show_id',[
            'adspolice' => $adspolice,
        ]);
    }
    
    public function getAttachment(Request $request)
    {
        
        $filepath = public_path('/').$request->path;
        return \Illuminate\Support\Facades\Response::download($filepath);
    }


    
    public function test(){
        
        $adspolice_logs = \App\Models\Log\LogAdsPolice::wherein('action',[ 'Ads Police marketing' ])->get();
       
        foreach ($adspolice_logs as $adspolice_log)
        {
            $data = json_decode($adspolice_log->data);
            $transistion = $data->transition;
            $comment = $data->comment;
            $user = $data->user;
            $issue = $data->issue;
            $timestamp = $data->timestamp;
            
            
            
            
            

            $adspolice = new \App\Models\Adspolice();
            $adspolice->key_branding = $issue->key;
            $adspolice->save();  
            
            $customfield_array = collect();
            
            
            
            //customfield_10303 remaining time
            if(!empty($issue->fields->customfield_10303))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]remaining time',
                    'value' => $issue->fields->customfield_10303->ongoingCycle->remainingTime->friendly,
                ];
                $customfield_array->push($data);
            }
            dd($customfield_array);
            
            
            
            //customfield_11466 [Ads]Advertisement Product
            if(!empty($issue->fields->customfield_11466->value))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Advertisement Product',
                    'value' => $issue->fields->customfield_11466->value,
                ];
                $customfield_array->push($data);
            }
            //customfield_11111 [Ads]3P array
            if (!empty($issue->fields->customfield_11111))
            {
                foreach ($issue->fields->customfield_11111 as $item)
                {
                    $data = [
                        'adspolice_id' => $adspolice->id,
                        'customfield' => '[Ads]3P',
                        'value' => $item->value,
                    ];
                    $customfield_array->push($data);
                }
            }
            //customfield_11112 [Ads]MAC DOK SUN array
            if (!empty($issue->fields->customfield_11112))
            {
                foreach ($issue->fields->customfield_11112 as $item)
                {
                    $data = [
                        'adspolice_id' => $adspolice->id,
                        'customfield' => '[Ads]MAC DOK SUN',
                        'value' => $item->value,
                    ];
                    $customfield_array->push($data);
                }
            }
            //customfield_11113 [Ads]Advertisement Link : string
            if(!empty($issue->fields->customfield_11113))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Advertisement Link',
                    'value' => $issue->fields->customfield_11113,
                ];
                $customfield_array->push($data);
            }
            //customfield_11108 [Ads]Department
            if(!empty($issue->fields->customfield_11108->value))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                   'customfield' => '[Ads]Department',
                   'value' => $issue->fields->customfield_11108->value,
               ];
               $customfield_array->push($data);
            }
            //customfield_11114 [Ads]Description string
            if(!empty($issue->fields->customfield_11114))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Description',
                    'value' => $issue->fields->customfield_11114,
                ];
                $customfield_array->push($data);
            }
            //customfield_11105 [Ads]Email
            if(!empty($issue->fields->customfield_11105))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Firstname',
                    'value' => $issue->fields->customfield_11105,
                ];
                $customfield_array->push($data);
            }
            //customfield_11103 [Ads]Firstname
            if(!empty($issue->fields->customfield_11103))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Firstname',
                    'value' => $issue->fields->customfield_11103,
                ];
                $customfield_array->push($data);
            }
            //customfield_11104 [Ads]Lastname
            if(!empty($issue->fields->customfield_11104))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Firstname',
                    'value' => $issue->fields->customfield_11104,
                ];
                $customfield_array->push($data);
            }
            //customfield_11109 [Ads]Found Date 
            if(!empty($issue->fields->customfield_11109))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Found Date',
                    'value' => $issue->fields->customfield_11109,
                ];
                $customfield_array->push($data);
            }
            //customfield_11107 [Ads]Others Description 
            if(!empty($issue->fields->customfield_11107))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Others Description',
                    'value' => $issue->fields->customfield_11107,
                ];
                $customfield_array->push($data);
            }
            //customfield_11106 [Ads]Product : string
            if(!empty($issue->fields->customfield_11106->value))
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]Product',
                    'value' => $issue->fields->customfield_11106->value,
                ];
                $customfield_array->push($data);
            }
            
            
            dd($customfield_array);
            
            
         
            
            
            
            
            
            
            
            
            $adspolice_customfield = \App\Models\AdspoliceCustomfield::insert($customfield_array->toArray());
            
            //comment
            $comment_array = collect();
            foreach ($issue->fields->comment->comments as $comment)
            {
                $comment_array->push([
                    'adspolice_id' =>$adspolice->id,
                    'comment' => $comment->body,
                    'author_email' => $comment->author->emailAddress,
                ]);
            }
            $adspolice_comment = \App\Models\AdspoliceComment::insert($comment_array->toArray());
            
            
            $attachment_array = collect();
            foreach ($issue->fields->attachment as $attachment)
            {
                $attachment_model = new \App\Models\AdspoliceAttachment();
                $url = $attachment->content;
                $contents = file_get_contents($url);
                $name = substr($url, strrpos($url, '.') );
                $uuid =  (string) \Illuminate\Support\Str::uuid();
                $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ]]);            
                $response = $client->get($url, [
                    'auth' => [
                        self::USERNAME, 
                        self::PASSWORD
                    ]
                ]);         
                file_put_contents('img/'.$uuid .$name, $response->getBody());
                $attachment_array->push([
                    'path' =>  'img/'.$uuid .$name,
                    'url' => $url,
                    'adspolice_id' => $adspolice->id,
                    'workflow_name' => $transistion->workflowName,
                ]);
            }
            $attachment = \App\Models\AdspoliceAttachment::insert($attachment_array->toArray());
            


            
        }
        
        
        
        
       
        
        return 'test';
    }
}
