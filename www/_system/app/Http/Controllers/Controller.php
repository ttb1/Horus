<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public static $temp;
    
    public static function getResponseData() {
        
        $response_data = new \App\Models\Datas\ResponseData();
        $response_data->timestamp = Carbon::now()->timestamp;
        return $response_data;
        
    }
    
}
