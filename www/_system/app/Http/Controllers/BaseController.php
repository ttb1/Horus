<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    
    public static $key_list;
    public static $url_list;
    public static $url_id;
    public static $url;
    public static $url_api;
    
    const IS_DEV = true;
    
    public function __construct() {
        
        self::$key_list = [
            env('VN_SERVICE_KEY'),
            env('ROBINHOOD_SERVICE_KEY'),
            env('EXA_KEY')
        ];
        
        if(self::IS_DEV) {
            self::$url_list = [
            env('ROBINHOOD_LOCAL_URL')
        ];
        } else {
            self::$url_list = [
            env('ROBINHOOD_URL')
        ];
        }
        
        //return $this->url_id;
        self::$url_id = env('URL_ID');
        self::$url = self::$url_list[self::$url_id];
        self::$url_api = self::$url.'/api/';
        
    }
    
    protected static function noRequestId() {
        
        return [
            "status" => 0,
            'msg' => "No id"
        ];
    }
    
    protected static function noRecordId(int $id) {
        return [
            "status" => 0,
            "msg" => "No record id:".$id
        ];
    }
    
}

?>
