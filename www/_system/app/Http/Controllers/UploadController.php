<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;

class UploadController extends Controller {
    
    public static function upload(Request $request) {
        
        if($request->hasFile('import')) {
            
            /*
            
            $date = now();
            $year = $date->format('Y');
            $month = $date->format('m');            
            $store_path = 'public/'.$year.'/'.$month;
            
            $file = $request->file('import');
            $filename = $file->hashName();
            $filename = str_replace('bin', $file->getClientOriginalExtension(), $filename);
            
            $path = $file->storeAs($store_path, $filename);
            */
            /*
            $file = $request->file('import');
            $filename = storage_path().'/'.$file->hashName();
            $path = file_put_contents($filename, $file->); */
            
            $path = $request->file('import')->store('/public/');
            
            return $path;
            
        }

        
    }
    
}

?>
