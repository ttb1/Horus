<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\TITMNormalize;

use Carbon\Carbon;

class TITMAPIController extends Controller
{
    public function migrate(Request $request)
    {
        
        //$data = TITMNormalize::all();
        $batches = TITMNormalize::orderBy('ISSUE_STATUS')
                ->distinct()
                ->get(['TAT_The_First_Working_Day']);
        
        foreach ($batches as $batch ) {
            
            $batch_name = $batch->TAT_The_First_Working_Day;
            
            if(!empty($batch_name)) {            
                
                $departments = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                        ->distinct()
                        ->get(['TAT_Department']);
                
                foreach($departments as $department) {
                
                    $department_name = $department->TAT_Department;
                    
                    $refuse = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                            ->where('ISSUE_STATUS', 'LIKE', '%L1.3%')
                            ->where('TAT_Department', $department_name)
                            ->count();

                    $t = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                            ->where('TAT_Department', $department_name)
                            ->where('ISSUE_STATUS', 'NOT LIKE', '%L1.3%')
                            ->get()->count();

                    if($t > 0) {

                        $t7 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L1.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L2.3C%')
                                ->get()->count();

                        $t14 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L1.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L3.3C%')
                                ->get()->count();

                        $t21 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L2.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L4.3C%')
                                ->get()->count();

                        $t30 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L3.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L5.3C%')
                                ->get()->count();

                        $t60_1 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L4.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L6.3C%')
                                ->get()->count(); 
                        
                        $t60_2 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L5.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L6.3C%')
                                ->get()->count(); 
                        
                        $t60_3 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L6.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L6.3C%')
                                ->get()->count(); 
                        
                        $t60 = $t60_1 + $t60_2 + $t60_3;
                        
                        $t90 = TITMNormalize::where('TAT_The_First_Working_Day', $batch_name)
                                ->where('TAT_Department', $department_name)
                                ->where('ISSUE_STATUS', 'LIKE', '%L7.3B%')
                                //->orWhere('ISSUE_STATUS', 'LIKE', '%L7.3C%')
                                ->get()->count();

                        $t7_per = ((($t-$t7)/$t)*100);
                        $t14_per = ((($t-$t7-$t14)/$t)*100);
                        $t21_per = ((($t-$t7-$t14-$t21)/$t)*100);
                        $t30_per = ((($t-$t7-$t14-$t21-$t30)/$t)*100);
                        $t60_per = ((($t-$t7-$t14-$t21-$t30-$t60)/$t)*100);
                        $t90_per = ((($t-$t7-$t14-$t21-$t30-$t60-$t90)/$t)*100);

                        //dd(number_format($t7_per, 2, '.', ','));

                        $TITM_view = new \App\Models\TITMView([
                            'batch_name' => Carbon::parse($batch_name)->format('Y-m-d'),
                            'department' => $department_name,
                            'receive' => $t,
                            'refuse' => $refuse,
                            't7' => ($t-$t7),
                            't14' => ($t-$t7-$t14),
                            't21' => ($t-$t7-$t14-$t21),
                            't30' => ($t-$t7-$t14-$t21-$t30),
                            't60' => ($t-$t7-$t14-$t21-$t30-$t60),
                            't90' => ($t-$t7-$t14-$t21-$t30-$t60-$t90),
                            't7_per' => number_format($t7_per, 2, '.', ','),
                            't14_per' => number_format($t14_per, 2, '.', ','),
                            't21_per' => number_format($t21_per, 2, '.', ','),
                            't30_per' => number_format($t30_per, 2, '.', ','),
                            't60_per' => number_format($t60_per, 2, '.', ','),
                            't90_per' => number_format($t90_per, 2, '.', ',')
                        ]);

                        //dd($TITM_view);

                        $TITM_view->save();
                    //}
                
                    } else {
                        
                        $TITM_view = new \App\Models\TITMView([
                            'batch_name' => Carbon::parse($batch_name)->format('Y-m-d'),
                            'department' => $department_name,
                            'receive' => 0,
                            'refuse' => $refuse,
                            't7' => 0,
                            't14' => 0,
                            't21' => 0,
                            't30' => 0,
                            't60' => 0,
                            't90' => 0,
                            't7_per' => 0,
                            't14_per' => 0,
                            't21_per' => 0,
                            't30_per' => 0,
                            't60_per' => 0,
                            't90_per' => 0
                        ]);
                        
                        $TITM_view->save();
                        
                        echo $batch_name."<br>";
                    }
                }
                
                //echo ($t/$t)."<br>";
                
                //echo (($t-$t7)/$t)."<br>";
                
                /*
                echo $batch->TAT_The_First_Working_Day.'<br>';
                echo count($issues)."<br>";
                echo $t."<br>";
                echo ($t-$t7)."<br>"; 
                echo ($t-$t7-$t14)."<br>"; 
                echo ($t-$t7-$t14-$t21)."<br>";
                echo ($t-$t7-$t14-$t21-$t30)."<br>";
                echo ($t-$t7-$t14-$t21-$t30-$t60)."<br>";*/
                
                /*foreach ($issues as $issue) {
                    
                    
                    
                    echo $issue->ISSUE_STATUS."<br>";
                    
                }*/
                
                //dd($t);
                
                //dd($data->where('TAT_The_First_Working_Day', $batch->TAT_The_First_Working_Day));
                
            }
        }
        
        //dd(count($data));
        
    }
    
}

