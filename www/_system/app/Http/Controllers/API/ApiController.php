<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Controller\API\CallController;
use App\Models\Datas\IPPBXCallData;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Faker\Provider\Uuid;



interface iApiController {
    public static function validateKey(Request $request);
    public function test();
}

class ApiController extends BaseController implements iApiController
{
    
    public function __construct() {        
        parent::__construct();        
    }
    
    public static function validateKey(Request $request) {
        
        $data = new Collection();
        $data->status = false;
        
        if(is_null($request->token)) {
            $data->msg = "No key";
            return $data;
        }
        
        foreach(self::$key_list as $key => $value) {
            if($value == $request->token) {
                $data->status = true;
                $data->msg = $key;
                return $data;
            }
        }
        
        $data->msg = 'Invalid key';
        
        return $data;
        
    }
    
    public static function checkKey(Request $request) {
        
        foreach(self::$key_list as $key => $value) {
            if($value == $request->token) {
                return 1;
            }
        }
        
        return 0;
        
    }
    
    public function test() {
        
        return self::$url;        
        
        /*
        $callData = [
            "message_code"=> "003",
            "agent_code"=> "admin_native",
            "station_id"=> 8001,
            "mobile_phone"=> 891281313,
            "datetime_response"=> Carbon::today()->toDateTimeString(),
            "call_id"=> Uuid::uuid(),
            "start_time"=> null,
            "end_time"=> null,
            "duration"=> null,
            "ringtime"=> null,
            "link_down_record"=> null,
            "status"=> null,
            "error_code"=> "0",
            "error_desc"=> ""
        ];

        return CallController::callAsterisk(new IPPBXCallData($callData));
        */
        
        $book_data = [
            "AccountTVTS" => "admin_tpe",
            "TenTVTS" => "Sale name",
            "hocVienID" => 1,
            "hoTenHocVien" => "Student Name",
            "dienThoaiHV" => "02623864645",
            "ngayHenPV" => '11-05-2018',
            "kieuGV" => 1,
            "ghiChuLichHen" => 'description',
            "khungGioID" => 7,
            "half" => 1,
            "key" => "SSeKfm7RXCJZxnFUleFsPf63o2ymZ93fWuCmvCjq"
        ];
        
        //return ExamAPIController::getToeic(100);
        
    }
}

?>
