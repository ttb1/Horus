<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use Carbon\Carbon;
use \Faker;

class NTLAdvisorAPIController extends Controller
{
    
    public function sampleSheet2() {
        
        $faker = Faker\Factory::create();
        
        $total_days = 28;
        $total_customer = 30;
        
        //$customers = new \Illuminate\Support\Collection();
        
        $customers = [];
        
        //30 days
        for($i = 0 ; $i<$total_days ; $i++) {
            
            $date = Carbon::create(2019, 1, 1);
            $date->addDay($i);
            
            for($j = 0 ; $j < $total_customer ; $j++) {
                
                if(count($customers) < $total_customer) {
                
                    $purchase_date = $faker->dateTimeBetween($startDate = '-1 year', $endDate = 'now', $timezone = null);


                    //echo $purchase_date->format('m-d-Y')."<br>";

                    //$status = 'RUNNING'.'ACTIVED'.'DEACTIVED'.'SUSPENDED';

                    // 1.Deactivated => STATUS_LAST = 'deactivated'
                    // 2.'ACTIVED' -> STATUS_LAST = date
                    // 3. 'SUSPENDED' -> 

                    //PACKAGE_STUDY TT TC

                    $customer = new \App\Models\AdvisorManagerRaw([
                        'LSM_ID' => 1,
                        'HV' => $faker->name, 
                        'SEX' => 1,
                        'CLASS' => 1,
                        'EMAIL' => 1,
                        'TELEPHONE' => 1,
                        'PACKAGE_NUMBER' => 1,
                        'PACKAGE_STUDY' => 1,
                        'PURCHASE_DATE' => $date->format('m-d-Y'),
                        'STATUS_LAST' => 1,
                        'EXPIRY_DATE' => 1,
                        'STATUS' => 1 ,
                        'LEVEL' => 1,
                        'ADVISOR' => 1,
                        'LMS_DATE' => 1,
                        'NOTE' => 1,
                        'CONTACT_ID' => 1,
                        'DATE' => 1
                    ]);

                    $customers[$j] = $customer;
                    
                } else {
                    
                }
                
                echo count($customers)." ".$i."<br>";
                
            }
            
            //echo $date."<br>";
            
        }
        
    }
    
    
}

