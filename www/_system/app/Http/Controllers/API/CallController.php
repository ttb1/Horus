<?php

namespace App\Http\Controller\API;

use App\Http\Controllers\BaseController;

use App\Models\Datas\IPPBXGetData;
use App\Models\Datas\IPPBXCallData;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

interface iCallController {
    public static function callVOIP(IPPBXCallData $call_data);
    public static function getVOIPRecord(IPPBXGetData $get_data);
}

class CallController extends BaseController implements iCallController
{
    
    const ASTERISK_URL = 'http://103.231.188.233:8000/api/call/';
    const GET_RECORD_URL = 'http://103.231.188.233:8000/api/getrecord/';
    
    public static function callVOIP(IPPBXCallData $call_data)
    {
        try{
            
            $client = new Client();
            $response = $client->post(self::ASTERISK_URL, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'SOAPAction'=>""
                ],
                'json' => $call_data,
                'connect_timeout' => 10
            ]);

        } catch(ClientException  $exception) {
            $response = $exception->getResponse();
            $responseBodyAsString = $response->getBody();
        }
   
        return $response->getBody();
    }

    public static function getVOIPRecord(IPPBXGetData $get_data)
    {
        $client = new Client();
        
        try{
            $response = $client->get(self::GET_RECORD_URL.$get_data->call_id , [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'SOAPAction'=>""
                ],
            ]);    
        } catch(ClientException  $exception) {
            $response = $exception->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
        }
        
        return $response->getBody();
    }
}