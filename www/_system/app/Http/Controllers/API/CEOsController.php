<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;


class CEOsController extends Controller
{
    public function setData(\Illuminate\Http\Request $request)
    {
        $log = new \App\Models\Log\LogAdsPolice();
        $log->action = '22ceos';
        $log->object = 'complete';
        $log->data = json_encode($request->all());
        $log->save();
        
        
        $CEOs = new \App\Models\CEOs($request->all());
        $CEOs->save();
    }
    public function test()
    {
        return view('test');
    }
}

