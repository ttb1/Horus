<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\SGBox;
use App\Models\Log\SGLogBox;

interface iSGLogAPIController {
    
    public static function create(Request $request);
    public static function get(Request $request);
    public static function getByBoxId(Request $request);
    
}

class SGLogAPIController extends Controller implements iSGLogAPIController
{
    
    public static function test() {
        return 'test';
    }
    
    public static function create(Request $request) {
        
        $sg_log_box = new SGLogBox($request->toArray());
        $sg_log_box->save();
        
        $response_data = self::getResponseData();
        $response_data->data = $sg_log_box;
        
        return response()->json($response_data);
        
    }
    
     public static function get(Request $request) {
        
        $sg_log_box = SGLogBox::where('id', $request->id)->first();
        $response_data = self::getResponseData();
        $response_data->data = $sg_log_box;
        
        return response()->json($response_data);
        
    }
    
    public static function getByBoxId(Request $request) {
        
        $sg_box = SGBox::where('id', $request->box_id)->first();
        
        $response_data = self::getResponseData();
        $response_data->data = [
            "box" => $sg_box,
            "log" => $sg_box->log
        ];
        
        return response()->json($response_data);
        
    }
    
}