<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\API\ApiController;

use Illuminate\Http\Request;

interface iContactController
{
    public static function validateContact(Request $request);
}

class ContactController extends BaseController implements iContactController
{
    
    const CONTACT_VALIDATE = 'contact/validate';
    
    public static function validateContact(Request $request) {
        
        $validate = ApiController::validateKey($request);
        
        if(!$validate->status) {
            return $validate->msg;
        }
        
        if(is_null($request->id)) {            
            return self::noRequestId();
        }
        
        //return $validate->msg;
        
        //Call Sale CRM, Robinhood        
        $link = self::$url_api.self::CONTACT_VALIDATE;
        
        return $link;
        
        $client = new Client();
        $response = $client->post($link, [
            'json' => $request
        ]);
        
        return $reponse;
        
    }
    
}