<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\SGBox;

interface iSGAPIController {
    
    public static function create(Request $request);
    public static function update(Request $request);
    public static function get(Request $request);
    
}

class SGAPIController extends Controller implements iSGAPIController
{
    
    public static function test() {
        return 'test';
    }
    
    public static function create(Request $request) {
        
        $existed_box = SGBox::where('name', $request->name)->first();
        
        $response_data = self::getResponseData();
        
        
        if(is_null($existed_box)) {

            $sg_box = new SGBox($request->toArray());
            $sg_box->save();
            
            $response_data->data = [
                "box" => $sg_box
            ];
            
            $response_data->message = 'Create SG Id: '.$sg_box->id;
            
        } else {
            $response_data->message = 'SG Box Name: '.$request->name.' is existed';
        }
        
        return response()->json($response_data);
        
    }
    
    public static function update(Request $request) {
        
        $sg_box = SGBox::where('name', $request->name)->first();
        
        $response_data = self::getResponseData();
        $response_data->data = [
            "box" => $sg_box
        ];
        
        if(!is_null($sg_box)) {
            
            $sg_box->fill($request->toArray());
            $sg_box->save();
            
            $response_data->message = 'updated';
            
            $sg_box_log = new \App\Models\Log\SGLogBox($sg_box->toArray());
            $sg_box_log->box_id = $sg_box->id;
            $sg_box_log->save();
            
            $response_data->data['log'] = $sg_box_log;
            
        } else {
            $response_data->message = 'SG Box Id: '.$request->id.' / Name: '.$request->name.' is not existed';
        }
        
        return response()->json($response_data);
        
    }
    
    public static function get(Request $request) {
        
        $sg_box = SGBox::where('id', $request->id)->first();
        
        $response_data = self::getResponseData();
        $response_data->data = [
            "box" => $sg_box,
            "log" => $sg_box->log
        ];
        
        return response()->json($response_data);
        
    }
    
}