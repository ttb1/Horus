<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class APIAdsPoliceController extends Controller
{
    
    const USERNAME = 'chatchai';
    const PASSWORD = 'topica123';
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    public function branding(\Illuminate\Http\Request $request)
    {
        $log = new \App\Models\Log\LogAdsPolice();
        $log->action = 'Ads Police Branding';
        $log->data = json_encode($request->json()->all()) ;
        $log->save();
        //return $log;
        
        $data = json_encode($request->json()->all());
        $data = json_decode($data);
        $transistion = $data->transition;
        $comment = $data->comment;
        $user = $data->user;
        $issue = $data->issue;
        $timestamp = $data->timestamp;

        $adspolice = new \App\Models\Adspolice();
        $adspolice->key_branding = $issue->key;
        $adspolice->save();    
        
        $customfield_array = collect();
            
            
         //customfield_11466 [Ads]Advertisement Product
        if(!empty($issue->fields->customfield_11466->value))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Advertisement Product',
                'value' => $issue->fields->customfield_11466->value,
            ];
            $customfield_array->push($data);
        }
        //customfield_11111 [Ads]3P array
        if (!empty($issue->fields->customfield_11111))
        {
            foreach ($issue->fields->customfield_11111 as $item)
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]3P',
                    'value' => $item->value,
                ];
                $customfield_array->push($data);
            }
        }
        //customfield_11112 [Ads]MAC DOK SUN array
        if (!empty($issue->fields->customfield_11112))
        {
            foreach ($issue->fields->customfield_11112 as $item)
            {
                $data = [
                    'adspolice_id' => $adspolice->id,
                    'customfield' => '[Ads]MAC DOK SUN',
                    'value' => $item->value,
                ];
                $customfield_array->push($data);
            }
        }
        //customfield_11113 [Ads]Advertisement Link : string
        if(!empty($issue->fields->customfield_11113))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Advertisement Link',
                'value' => $issue->fields->customfield_11113,
            ];
            $customfield_array->push($data);
        }
        //customfield_11108 [Ads]Department
        if(!empty($issue->fields->customfield_11108->value))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
               'customfield' => '[Ads]Department',
               'value' => $issue->fields->customfield_11108->value,
           ];
           $customfield_array->push($data);
        }
        //customfield_11114 [Ads]Description string
        if(!empty($issue->fields->customfield_11114))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Description',
                'value' => $issue->fields->customfield_11114,
            ];
            $customfield_array->push($data);
        }
        //customfield_11105 [Ads]Email
        if(!empty($issue->fields->customfield_11105))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Email',
                'value' => $issue->fields->customfield_11105,
            ];
            $customfield_array->push($data);
        }
        //customfield_11103 [Ads]Firstname
        if(!empty($issue->fields->customfield_11103))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Firstname',
                'value' => $issue->fields->customfield_11103,
            ];
            $customfield_array->push($data);
        }
        //customfield_11104 [Ads]Lastname
        if(!empty($issue->fields->customfield_11104))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Lastname',
                'value' => $issue->fields->customfield_11104,
            ];
            $customfield_array->push($data);
        }
        //customfield_11109 [Ads]Found Date 
        if(!empty($issue->fields->customfield_11109))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Found Date',
                'value' => $issue->fields->customfield_11109,
            ];
            $customfield_array->push($data);
        }
        //customfield_11107 [Ads]Others Description 
        if(!empty($issue->fields->customfield_11107))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Others Description',
                'value' => $issue->fields->customfield_11107,
            ];
            $customfield_array->push($data);
        }
        //customfield_11106 [Ads]Product : string
        if(!empty($issue->fields->customfield_11106->value))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]Product',
                'value' => $issue->fields->customfield_11106->value,
            ];
            $customfield_array->push($data);
        }
        //add customfield to db
        $adspolice_customfield = \App\Models\AdspoliceCustomfield::insert($customfield_array->toArray());
        
        //comment
        $comment_array = collect();
        foreach ($issue->fields->comment->comments as $comment)
        {
            $comment_array->push([
                'adspolice_id' =>$adspolice->id,
                'comment' => $comment->body,
                'author_email' => $comment->author->emailAddress,
            ]);
        }
        $adspolice_comment = \App\Models\AdspoliceComment::insert($comment_array->toArray());
  
        //attachment
        $attachment_array = collect();
        foreach ($issue->fields->attachment as $attachment)
        {
            $attachment_model = new \App\Models\AdspoliceAttachment();
            $url = $attachment->content;
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '.') );
            $uuid =  (string) \Illuminate\Support\Str::uuid();
            $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ]]);            
            $response = $client->get($url, [
                'auth' => [
                    self::USERNAME, 
                    self::PASSWORD
                ]
            ]);         
            file_put_contents('img/'.$uuid .$name, $response->getBody());
            $attachment_array->push([
                'path' =>  'img/'.$uuid .$name,
                'url' => $url,
                'adspolice_id' => $adspolice->id,
                'workflow_name' => $transistion->workflowName,
            ]);
        }
        $attachment = \App\Models\AdspoliceAttachment::insert($attachment_array->toArray());
            

            
            
            
    }
    
    public function marketing(\Illuminate\Http\Request $request)
    {
        $log = new \App\Models\Log\LogAdsPolice();
        $log->action = 'Ads Police marketing';
        $log->data = json_encode($request->json()->all()) ;
        $log->save();
        //return $log;
        
        $data = json_encode($request->json()->all());
        $data = json_decode($data);
        $transistion = $data->transition;
        $comment = $data->comment;
        $user = $data->user;
        $issue = $data->issue;
        $timestamp = $data->timestamp;

        $adspolice = \App\Models\Adspolice::where('key_branding',$issue->fields->issuelinks[0]->outwardIssue->key)->first();
        $adspolice->key_marketing = $issue->key;
        $adspolice->save();
        
        $customfield_array = collect();
        //customfield_10303 remaining time
        if(!empty($issue->fields->customfield_10303))
        {
            $data = [
                'adspolice_id' => $adspolice->id,
                'customfield' => '[Ads]remaining time',
                'value' => $issue->fields->customfield_10303->ongoingCycle->remainingTime->friendly,
            ];
            $customfield_array->push($data);
        }
        $adspolice_customfield = \App\Models\AdspoliceCustomfield::insert($customfield_array->toArray());
        //comment
        $comment_array = collect();
        foreach ($issue->fields->comment->comments as $comment)
        {
            $comment_array->push([
                'adspolice_id' =>$adspolice->id,
                'comment' => $comment->body,
                'author_email' => $comment->author->emailAddress,
            ]);
        }
        $adspolice_comment = \App\Models\AdspoliceComment::insert($comment_array->toArray());
  
        //attachment
        $attachment_array = collect();
        foreach ($issue->fields->attachment as $attachment)
        {
            $attachment_model = new \App\Models\AdspoliceAttachment();
            $url = $attachment->content;
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '.') );
            $uuid =  (string) \Illuminate\Support\Str::uuid();
            $client = new Client(['headers' => [ 'Content-Type' => 'application/json' ]]);            
            $response = $client->get($url, [
                'auth' => [
                    self::USERNAME, 
                    self::PASSWORD
                ]
            ]);         
            file_put_contents('img/'.$uuid .$name, $response->getBody());
            $attachment_array->push([
                'path' =>  'img/'.$uuid .$name,
                'url' => $url,
                'adspolice_id' => $adspolice->id,
                'workflow_name' => $transistion->workflowName,
            ]);
        }
        $attachment = \App\Models\AdspoliceAttachment::insert($attachment_array->toArray());
            

            
            
            
    }
}