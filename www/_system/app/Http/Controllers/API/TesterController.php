<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;

use Illuminate\Support\Collection;
use Illuminate\Http\Request;
//use Symfony\Component\HttpFoundation\Request;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

interface iTesterController {
    
    public static function addSchedule(Request $request);
    public static function changeSchedule(Request $request);
    public static function cancelSchedule(Request $request);
    public static function getSB100(Request $request);
    public static function getSB100Topica(Request $request);
    public static function getSB100Casec(Request $request);

    public static function add(Request $request);
    public static function update(Request $request);
    public static function remove(Request $request);
    public static function get(Request $request);
    public static function getAll();
    
}

class TesterController extends BaseController implements iTesterController
{
    
    const ACCOUNT_TVTS = 'admin_tpe';
    const SCHEDULE_ADD = 'save_contact/add';
    const SCHEDULE_UPDATE = 'save_contact/change_schedule';
    const SCHEDULE_CANCEL = 'save_contact/cancel_schedule';
    
    const TESTER_SCORE_ADD = 'tester/score/set';
    const TESTER_SCORE_UPDATE = 'tester/score/update';

    public static $tester_url;
    public static $key;
    public static $auth;
    
    public function __construct() {        
        
        parent::__construct();   
        
        self::$key = env('TESTER_KEY');
        self::$auth = [
            env('TESTER_USERNAME'),
            env('TESTER_PASSWORD')
        ];
        self::$tester_url = env('TESTER_URL');
        
    }
    
    //TO TESTER    
    public static function addSchedule(Request $request)
    {
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        $client = new Client();
        
        $link = self::$tester_url.self::SCHEDULE_ADD;
        
        $request->request->add([
            'key' => self::$key
        ]);
        
        $response = $client->post($link, [
            'auth' => self::$auth,
            'json' => $request->toArray()
        ]);
        
        $data = [
            'data'=> $request->getContent(),
            'response' => json_decode($response->getBody())
        ];
        
        return json_encode($data);
        
    }

    public static function  changeSchedule(Request $request){
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        $client = new Client();
        
        $link = self::$tester_url.self::SCHEDULE_UPDATE;
        
        $request->request->add([
            'key' => self::$key
        ]);
        
        $response = $client->post($link, [
            'auth' => self::$auth,
            'json' => $request->toArray()
            
        ]);
        
        $data = [
            'data'=> $request->getContent(),
            'response' => $response
        ];
        
        return json_encode($data);
        
    }

    
    public static function cancelSchedule(Request $request){
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        $client = new Client();
        
        $link = self::$tester_url.self::SCHEDULE_CANCEL;
        
        $request->request->add([
            'key' => self::$key
        ]);
        
        $response = $client->post($link, [
            'auth' => self::$auth,
            'json' => $request->toArray()
            
        ]);
        
        $data = [
            'data'=> $request->getContent(),
            'response' => $response
        ];
        
        return json_encode($data);
        
    }
    
    public static function getSB100(Request $request){
        return;
    }
    public static function getSB100Topica(Request $request){
        return;
    }
    public static function getSB100Casec(Request $request){
        return;
    }
    
    //FROM TESTER
    public static function add(Request $request) {
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        //call Robinhood
        $link = self::$url_api.self::TESTER_SCORE_ADD;
        
        //return $link;
        
        $client = new Client();
        $response = $client->post($link, [
            RequestOptions::JSON => $request->toArray()
        ]);
        
        return $response->getBody();
        
    }
    
    public static function update(Request $request) {
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        //call Robinhood
        $link = self::$url_api.self::TESTER_SCORE_UPDATE;
        
        //return $link;
        
        $client = new Client();
        $response = $client->post($link, [
            RequestOptions::JSON => $request->toArray()
        ]);
        
        return $response->getBody();
        
    }
    
    public static function remove(Request $request) {
        
        return 'remove';
        
    }
    
    public static function get(Request $request) {
        
        return 'get';
        
    }
    
    public static function getAll() {
        return 'get all';
    }
    
    //
    public function oldAddSchedule() {
        if (isset($_GET['fullname'])) {
            $fullname = $_GET['fullname'];
        }
        if (isset($_GET['contactID'])){
            $contactID = $_GET['contactID'];
        }
        if (isset($_GET['time_schedule'])){
            $time_schedule = $_GET['time_schedule'];
        }
        if (isset($_GET['date_schedule'])){
            $date_schedule = $_GET['date_schedule'];
        }

        if (isset($_GET['teacher'])){
            $teacher = $_GET['teacher'];
        }
        if (isset($_GET['description'])){
            $description = $_GET['description'];
        }
        

        $object = [       
            'level_id' => 3,
            'fullname'=> $fullname ,
            'contactID'=>$contactID,
            'time_schedule'=> $time_schedule,
            'date_schedule'=> $date_schedule,
            'teacher' => $teacher,
            'description' => $description
        ];
    }
    
}