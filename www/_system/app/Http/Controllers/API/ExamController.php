<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

interface iExamController {
    
    //TO EXAM
    public static function generateUser(Request $request);
    
    //FROM EXAM
    public static function add(Request $request);
    public static function update(Request $request);
    public static function remove(Request $request);
    public static function get(Request $request);
    public static function getAll();
    public static function getToeic(int $total_score);
    
}

class ExamController extends BaseController implements iExamController
{
    
    const EXAM_SCORE_ADD = 'exam/score/set';
    const EXAM_SCORE_UPDATE = 'exam/score/update';
    
    public function __construct() {
        parent::__construct();
        
    }
    
    //TO Exam
    public static function generateUser(Request $request) {

        $validate = ApiController::validateKey($request);
        
        $exam_url = env('EXAM_URL');
        $ws_token = '758542e8ddd2b4b26d1ffac07837c1fe';
        
        $link = $exam_url.'?wstoken='.$ws_token.'&contactid=' . $request->contact_id . '&wsfunction=local_create_usertl&moodlewsrestformat=json';
        
        $client = new Client();
        $response = $client->get($link);
        $exam = json_decode($response->getBody());
        
        $data = [
            'contact_id' => $request->contact_id,
            'exam' => $exam,
            'link' => $link
        ];
        
        return $data;
        
    }
    
    //FROM Exam
    public static function add(Request $request) {
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        //call Robinhood
        $link = self::$url_api.self::EXAM_SCORE_ADD;
        
        //return $link;
        
        $client = new Client();
        $response = $client->post($link, [
            RequestOptions::JSON => $request->toArray()
        ]);
        
        return $response->getBody();
        
    }
    
    public static function update(Request $request) {
        
        $validate = ApiController::validateKey($request);
        if(!$validate->status) {
            return $validate->msg;
        }
        
        //call Robinhood
        $link = $this->url_api.self::EXAM_SCORE_UPDATE;
        $client = new Client();
        $response = $client->post($link, [
            'json' => $request
        ]);
        
        return $reponse;
        
    }
    
    public static function remove(Request $request) {
        
        return 'remove';
        
    }
    
    public static function get(Request $request) {
        
        return 'get';
        
    }
    
    public static function getAll() {
        return 'get all';
    }
    
    public static function getToeic(int $total_score) {
        
        
        //Fomula ((50-(Ax1-Cx1))/50)*(Bx1-Bx2)+Bx2
        $topica_toeic_x1 = \App\Models\TopicaTOEICLevel::where('value2', '<=', $total_score)
              ->first();
        $topica_toeic_x2 = \App\Models\TopicaTOEICLevel::where('id', '>', $topica_toeic_x1->id)
                ->first();
        
        $toeic_score = ((50-($topica_toeic_x1->value1-$total_score))/50)*($topica_toeic_x1->value2-$topica_toeic_x2->value2)+$topica_toeic_x2->value2;
        
        return $toeic_score;
        
    }
    
}