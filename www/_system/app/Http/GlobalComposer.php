<?php

namespace App\Http;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

use App\Models\UserRole;

use App\Http\Controllers\CacheController;

class GlobalComposer {
    
    protected $role_id;
    protected $menu_data = null;
    protected $is_auth = false;

    protected $username;

    protected $count = 0;
    protected $gen_count = 0;
    
    public function compose(View $view) {
        
        $this->is_auth = Auth::check();
        
        if($this->is_auth) {
            
            if(is_null($this->menu_data)) {            
                $user = Auth::user();
                $this->role_id = UserRole::where('user_id', '=', $user->id)
                        ->first()->role_id;                
                $menu_data = cache(CacheController::ROLE_PAGE.$this->role_id);                
                $this->menu_data = $menu_data;
                $this->gen_count++; 
            } 
            
            $this->username = Auth::user()->name;    
        }
        
        $this->count++;
        $view->with([ 
                'menu_data' => json_encode($this->menu_data),
                'role_id' => $this->role_id,
                'is_auth' => $this->is_auth,
                'count' => $this->count,
                'gen_count' => $this->gen_count,
                'username' => $this->username
                ]);
        
        /*
        $user = Auth::user();
        $user_role = UserRole::where('user_id', '=', $user->id)
                    ->first();      
        $menu_data = cache(AdminController::ROLE_PAGE.$user_role->id);
        
        View::share('user_id', $user->id);
        View::share('role_id', $user_role->id);
        View::share('menu_data', $menu_data);
        */
        
        
        
    }
    
}
