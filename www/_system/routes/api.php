<?php

use Illuminate\Http\Request;
use App\Article;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//TEST
Route::get('test', 'API\ApiController@test');

//Check key
Route::post('key/check', 'API\APIController@checkKey');

//Validate contact
Route::post('contact/validate', 'API\ContactController@validateContact');

//Call
Route::post('voip/call', 'API\CallController@callVOIP');
Route::post('voip/record/get', 'API\CallController@getVOIPRecord');

//EXAM
Route::post('exam/user/generate', 'API\ExamController@generateUser');
Route::post('exam_score/add', 'API\ExamController@add');
Route::post('exam_score/update', 'API\ExamController@update');

//TESTER
Route::post('schedule/add', 'API\TesterController@addSchedule');
Route::post('schedule/change', 'API\TesterController@changeSchedule');
Route::post('schedule/cancel', 'API\TesterController@cancelSchedule');

//Route::post('receive/sb', '');
//Route::post('receive/sbtopica', '');
//Route::post('receive/casec', '');

Route::post('tester_score/add', 'API\TesterController@add');
Route::post('tester_score/update', 'API\TesterController@update');


//hr call
Route::get('startcall' ,'API\HrCall@call' );


//Smart Greenhouse
Route::get('sg', 'API\SGAPIController@test');
Route::post('sg/box/create', 'API\SGAPIController@create');
Route::post('sg/box/update', 'API\SGAPIController@update');
Route::get('sg/box/get', 'API\SGAPIController@get');

Route::post('sg/log/create', 'API\SGLogAPIController@create');
Route::get('sg/log/get', 'API\SGLogAPIController@get');
Route::get('sg/log/getByBoxId', 'API\SGLogAPIController@getByBoxId');




















/*
Route::get('/user/findGET/{id}', 'UserController@findUserAPIGET');
Route::post('/user/findPOST', 'UserController@findUserAPIPOST');
Route::get('/users', 'UserController@getAllUserFromAPI');

// Route::get('ajaxBookingschedule' , 'Api/TesterController@bookingSchedule');
Route::get('ajaxBookingschedule' , 'API/TesterController@bookingSchedule');

Route::get('call', 'API\ApiController@call');
Route::get('importContact', 'API\ApiController@importContact');
Route::get('importOwner', 'API\ApiController@importOwner');
*/
/*
Route::post('tester_score/add', 'API\TesterAPIController@add');
Route::post('tester_score/update', 'API\TesterAPIController@update');
Route::post('tester_score/remove', 'API\TesterAPIController@remove');
Route::post('tester_score/get', 'API\TesterAPIController@get');
Route::post('tester_score/all', 'API\TesterAPIController@getAll');

Route::post('exam_score/add', 'API\ExamAPIController@add');
Route::post('exam_score/update', 'API\ExamAPIController@update');
Route::post('exam_score/remove', 'API\ExamAPIController@remove');
Route::post('exam_score/get', 'API\ExamAPIController@get');
Route::post('exam_score/all', 'API\ExamAPIController@getAll');

Route::get('exam', ['as' => 'exam', 'uses' => 'API\ExamController@test']);

*/

/*
Route::get('contact_data/get' , 'API\ContactAPIController@get');
Route::get('contact_data/contact_get_by/{phone}' , 'API\ContactAPIController@contactGetBy');
Route::get('exam_genarate_pass' , 'API\ContactAPIController@generateUser');
Route::get('exam_data/get/{contact_id}' , 'API\ContactAPIController@examDataGet');
Route::get('tester/get/{contact_id}' , 'API\ContactAPIController@testerDataGet');
Route::get('tester/add/{contact_id}/{deal_id}' , 'API\ContactAPIController@testerAdd');
Route::get('sales_pipelinel2/{deal_id}' , 'API\ContactAPIController@salesPipelineL2');
*/

// cronJob 
Route::get('importContactCron', 'API\ApiController@importContactWithCornJob');
Route::get('updateContact', 'API\ApiController@updateContact');


// Ads Police API
Route::post('webhook/branding', 'API\APIAdsPoliceController@branding');
Route::post('webhook/marketing', 'API\APIAdsPoliceController@marketing');

// 22CEOs
Route::get('webhook/ceos', 'API\CEOsController@setData');

Route::get('titm/migrate', 'API\TITMAPIController@migrate');
Route::get('ntla/sample/sheet2', 'API\NTLAdvisorAPIController@sampleSheet2');