<?php
use App\Http\Middleware\CheckAge;
//Auth
Route::get('/', function () {
    Auth::logout();
    return view('home');
})->name('login') ;

Route::post('auth/login', ['as' => 'auth_login', 'uses' => 'Auth\AuthController@login']) ;
Route::get('auth/logout', ['as' => 'auth_logout', 'uses' => 'Auth\AuthController@logout']);

//Cache
Route::get('cache', 'CacheController@cache');
Route::get('cache/list', 'CacheController@index');
Route::get('cache/check', 'CacheController@check');
Route::get('cache/test', 'CacheController@test');


//User
Route::get('user', ['as' => 'user_index', 'uses' => 'UserController@index']);
Route::get('user/add', ['as' => 'user_add', 'uses' => 'UserController@add']);
Route::post('user/create', ['as' => 'user_create', 'uses' => 'UserController@create']);
Route::get('user/edit/{id}', ['as' => 'user_edit', 'uses' => 'UserController@edit']);
Route::post('user/update', ['as' => 'user_update', 'uses' => 'UserController@update']);
Route::post('user/remove', ['as' => 'user_remove', 'uses' => 'UserController@remove']);
Route::get('user/profile/{id}', ['as' => 'user_profile', 'uses' => 'UserController@profile']);

//KPI
Route::get('user/kpi', ['as' => 'user_kpi', 'uses' => 'KPIController@index']);
Route::get('user/kpi/add', ['as' => 'user_kpi_add', 'uses' => 'KPIController@add']);
Route::post('user/kpicreate', ['as' => 'create_kpi', 'uses' => 'KPIController@create']);
Route::get('user/kpiedit/{id}', ['as' => 'kpi_edit', 'uses' => 'KPIController@edit']);
Route::get('kpiremove/{id}', ['as' => 'kpi_remove', 'uses' => 'KPIController@remove']);
Route::post('user/kpiupdate', ['as' => 'update_kpi', 'uses' => 'KPIController@update']);

//Page
Route::get('page', ['as' => 'page_index', 'uses' => 'PageController@index']);
Route::get('page/add', ['as' => 'page_add', 'uses' => 'PageController@add']);
Route::post('page/create', ['as' => 'page_create', 'uses' => 'PageController@create']);
Route::get('page/edit/{id}', ['as' => 'page_edit', 'uses' => 'PageController@edit']);
Route::post('page/update', ['as' => 'page_update', 'uses' => 'PageController@update']);
Route::post('page/remove', ['as' => 'page_remove', 'uses' => 'PageController@remove']);
Route::get('page/rolepage',['as' => 'page_role', 'uses' => 'RolePageController@rolepage'] );
Route::post('page/updateRolePage',['as' => 'update_role_page', 'uses' => 'RolePageController@updateRolePage'] );
Route::get('pageselect', ['as' => 'pageselect', 'uses' => 'RolePageController@pageselect']); // Ajax


//Contact
Route::get('contact', ['as' => 'contact_index', 'uses' => 'ContactController@index'])->Middleware('auth')  ;
Route::post('contact/import', ['as' => 'contact_import', 'uses' => 'ContactController@import']);
Route::get('contact/add', ['as' => 'contact_add', 'uses' => 'ContactController@add']);
Route::post('contact/create', ['as' => 'contact_create', 'uses' => 'ContactController@create']);
Route::get('contact/edit/{id}', ['as' => 'contact_edit', 'uses' => 'ContactController@edit']);
Route::post('contact/update', ['as' => 'contact_update', 'uses' => 'ContactController@update']);
Route::post('contact/remove', ['as' => 'contact_remove', 'uses' => 'ContactController@remove']);
Route::get('contact/profile/{id}', ['as' => 'contact_profile', 'uses' => 'ContactController@profile']);
Route::get('contact/transfer', ['as' => 'contact_transfer', 'uses' => 'ContactController@transfer']);
Route::post('contact/transfer/user', ['as' => 'contact_transfer_to_user', 'uses' => 'ContactController@transferToUser']);
Route::get('contact/importContact', ['as' => 'contact_import_contact', 'uses' => 'ContactController@importContact']);
Route::get('contact/test', ['as' => 'contact_test', 'uses' => 'ContactController@test']);


//Sale Gourp 
Route::get('salegroup', ['as' => 'sale_group', 'uses' => 'SaleGroupController@index']);
Route::get('salegroup/add', ['as' => 'add_sale_group', 'uses' => 'SaleGroupController@addOrEdit']);
Route::get('salegroup/edit', ['as' => 'edit_sale_group', 'uses' => 'SaleGroupController@addOrEdit']);
Route::get('salegroup/save', ['as' => 'save_sale_group', 'uses' => 'SaleGroupController@save']);
Route::get('salegroup/remove', ['as' => 'remove_sale_group', 'uses' => 'SaleGroupController@remove']);

//User Sale Group
Route::get('salegroup/usersalegroup', ['as' => 'user_sale_group', 'uses' => 'UserSaleGroupController@index']);

//Report
Route::get('report', ['as' => 'report_index', 'uses' => 'ReportController@index']);
Route::get('report/qttt', ['as' => 'report_qttt', 'uses' => 'ReportController@reportQTTT']);
Route::get('report/split', ['as' => 'report_split', 'uses' => 'ReportController@reportSplit']);
Route::get('report/css', ['as' => 'report_css', 'uses' => 'ReportController@reportCSS']);
Route::get('report/chartqttt' , ['as', 'chart_qttt', 'uses' => 'ReportController@reportChartQttt']);
Route::get('report/qttt_quantity' , ['as', 'qttt_quantity', 'uses' => 'ReportController@qttt_quantity']);
Route::get('report/handover' , ['as', 'report_handover', 'uses' => 'ReportController@handover']);



//Report Marketing
Route::get('reportmarketing', [ 'as' => 'report_marketing' ,'uses' => 'ReportMarketingController@index' ]);
Route::get('reportmarketing/viewbysource', [ 'as' => 'marketing_report' ,'uses' => 'ReportMarketingController@marketingreport' ]);
Route::get('reportmarketing/heatmap', [ 'as' => 'marketing_heatmap' ,'uses' => 'ReportMarketingController@marketingReportHeatmap' ]);
Route::get('reportmarketing/overviewheatmap', [ 'as' => 'marketing_report_overview_heatmap' ,'uses' => 'ReportMarketingController@marketingReportOverviewheatmap' ]);
Route::get('reportmarketing/overview', [ 'as' => 'marketing_over_view' ,'uses' => 'ReportMarketingController@marketingReportOverview' ]);

//Upload
Route::post('upload', ['as' => 'upload', 'uses' => 'UploadController@upload']);

//API
Route::post('api/users', 'UserController@getAllUserFromWebRoute');

//3rd Party
Route::get('auth/google', 'Auth\AuthController@redirectToProvider');
Route::get('auth/google/callback', 'Auth\AuthController@handleProviderCallback');

//Call History
Route::get('callHistory', ['as' => 'call_history_index', 'uses' => 'CallHistoryController@index']);
Route::get('callHistory/add', ['as' => 'call_history_add', 'uses' => 'CallHistoryController@add']);
Route::post('callHistory/create', ['as' => 'call_history_create', 'uses' => 'CallHistoryController@create']);
Route::get('callHistory/generate', ['as' => 'call_history_generate', 'uses' => 'CallHistoryController@generate']);
Route::get('callHistory/shuffleDate', ['as' => 'call_history_shuffle_date', 'uses' => 'CallHistoryController@shuffleDate']);
Route::get('callHistory/generateComplete', ['as' => 'call_history_generate_complete', 'uses' => 'CallHistoryController@completeGenerate']);

//Others
Route::get('info', function () {
    return view('/info');
});

Route::get('layout', function () {
    return view('layout');
});
Route::get('chartqtttmock', function () {
    return view('chartqtttmock');
});

//Ajax
Route::post('ajaxPOST', ['as' => 'ajaxPOST', 'uses' => 'CallHistoryController@ajaxPOST']);
Route::get('ajaxGET', ['as' => 'ajaxGET', 'uses' => 'CallHistoryController@ajaxGET']);

//Chart
Route::get('chart', ['as' => 'chart', 'uses' => 'ChartController@index']);

//saledashboard
Route::get('saledashboard' , ['as' => 'sale_dashboard','uses' => 'SaleDashbordController@index' ])->Middleware('auth') ;


//migration 
Route::get('migration', ['as' => 'm_index', 'uses' => 'MigrationController@index']);
Route::get('migration/usergroup', ['as' => 'm_core_users', 'uses' => 'MigrationController@usergroup']);
Route::get('migration/users', ['as' => 'm_users', 'uses' => 'MigrationController@users']);
Route::get('migration/callhistory/{offset}',['as' => 'm_call_history', 'uses' => 'MigrationController@call_history']);
Route::get('migration/levels', ['as' => 'm_levels', 'uses' => 'MigrationController@levels']);
Route::get('migration/contacts/{offset}', ['as' => 'm_contacts', 'uses' => 'MigrationController@contacts']);
Route::get('migration/phone/{offset}', ['as' => 'm_phone', 'uses' => 'MigrationController@phone']);
Route::get('migration/sourcetypes', ['as' => 'm_sourcetypes', 'uses' => 'MigrationController@sourcetypes']);
Route::get('migration/channels', ['as' => 'm_channels', 'uses' => 'MigrationController@channels']);
Route::get('migration/importexcels', ['as' => 'm_importexcels', 'uses' => 'MigrationController@importexcels']);
Route::get('migration/sqlserver', ['as' => 'm_sqlserver', 'uses' => 'MigrationController@sqlserver']);
Route::get('migration/email/{offset}', ['as' => 'm_sqlserver', 'uses' => 'MigrationController@email']);


//redis 
Route::get('redis', ['as' => 'redis', 'uses' => 'RedisController@index']);
Route::get('redisCache', ['as' => 'redisCache', 'uses' => 'RedisController@redisCache']);

// ContactStatusController
Route::get('contactstatus/{contact_id}/{version}', ['as' => 'ContactStatus', 'uses' => 'ContactStatusController@index'])->Middleware('auth');
Route::post('ContactStatus/updateContactStatus', ['as' => 'updateContactStatus', 'uses' => 'ContactStatusController@updateContactStatus']);
Route::post('ContactStatus/updateContact', ['as' => 'updateContact', 'uses' => 'ContactStatusController@updateContact']);
Route::get('contactstatus/dummy/{contact_id}/{flag}/{version}', ['as' => 'ContactStatus_dummy', 'uses' => 'ContactStatusController@dummy_test']);

// Asterisk
Route::get('call/asterisk_call', 'Sale\L1Controller@asteriskCall');
Route::get('call/getRecord/{call_id}', 'Sale\L1Controller@asteriskGetRecord');


//Levecontroller
Route::post('ContactStatus/l1update', ['as' => 'l1update', 'uses' => 'Sale\L1Controller@updateUserContact']);
Route::get('genaratetestuser', 'Sale\L3To6Controller@genarateTestUser');
//


Route::get('bookingScheduleAdd' , 'API\TesterController@bookingScheduleAdd');
Route::get('bookingScheduleChange' , 'API\TesterController@bookingScheduleChange');
Route::get('bookingScheduleCancel' , 'API\TesterController@bookingScheduleCancel');

Route::get('redis/generate/{size}', ['as' => 'redis_generate', 'uses' => 'redisController@generate']);
Route::get('redis/getCallhistoryLv/{lv}', ['as' => 'redis_getCallhistoryLv', 'uses' => 'redisController@getCallhistoryLv']);

//mail
Route::get('testMailSend', ['as' => 'testMailSend', 'uses' => 'MailController@testMailSend']);
Route::get('mail', ['as' => 'mail', 'uses' => 'MailController@index']);
Route::get('mailadd', ['as' => 'mailadd', 'uses' => 'MailController@add']);
Route::get('mail_template_edit/{id}', ['as' => 'mail_template_edit', 'uses' => 'MailController@edit']);
Route::get('mail_delete/{id}', ['as' => 'mail_delete', 'uses' => 'MailController@remove']);
Route::post('mail_teamplate_create', ['as' => 'mail_teamplate_create', 'uses' => 'MailController@create']);
Route::post('mail_teamplate_update', ['as' => 'mail_teamplate_update', 'uses' => 'MailController@update']);

Route::get('mail_view/{id}', ['as' => 'mail_view', 'uses' => 'MailController@mailView']);
Route::post('mail_teamplate_send',['as'=> 'mail_teamplate_send' , 'uses'=> 'MailController@mailTeamplateSend'  ] );

Route::get('mail_l3/{contact_id}/{type}', ['as' => 'mail_l3', 'uses' => 'Sale\L3To6Controller@mailL3']);
Route::get('mail_l2/{contact_id}/{type}', ['as' => 'mail_l2', 'uses' => 'Sale\L2Controller@mailL2']);
Route::post('mail_l3_send',['as'=> 'mail_l3_send' , 'uses'=> 'MailController@mailL3Send'  ] );

Route::get('mailformat',['as'=> 'mail_format' , 'uses'=> 'MailController@mailFormat'  ] );
Route::post('img_mail', ['as' => 'img_mail', 'uses'=>'MailController@imgMail' ]);


//TEST
Route::get('test', ['as' => 'test', 'uses' => 'TestController@index']);


//generate handover metadata 
Route::get('makeHandoverMetadata', ['as' => 'makeHandoverMetadata', 'uses' => 'CacheController@makeHandoverMetadata']);


Route::get( 'contact_page/{phone}', 'HubspotController@index');
Route::get('horus/{phone}', 'HubspotController@reactUi');
Route::get( 'endCall','HubspotController@endCall');
Route::get( 'startcall','HubspotController@startCall');
//engagementsController 
Route::get('engagements', ['as' => 'engagements', 'uses' => 'api\EngagementsController@index']);

//webhook
Route::get('webhook', ['as' => 'webhook', 'uses' => 'webhook\HubspotController@index']);
Route::get('webhook/hubspot', ['as' => 'webhook_hubspot', 'uses' => 'webhook\HubspotController@hook']);

// create deal 
Route::get('autocreatedeal', ['as' => 'webhook', 'uses' => 'HubSpot\HSDealController@test']);


//horus redis controller 
Route::get('horusRedis', 'Horus\HorusRedisController@index');

// sale predict
Route::get('salePredict',  'SalePredict\ImportController@index');


Route::get('hr-call', function () {
    return view('hrCall/index');
});

Route::get('ads_police', ['as' => 'ads_police', 'uses' => 'AdsPoliceController@index']);
Route::post('ads_police/import', ['as' => 'ads_police_import', 'uses' => 'AdsPoliceController@import']);
Route::get('ads_police/test', ['as' => 'ads_police_test', 'uses' => 'AdsPoliceController@test']);
Route::get('ads_police/show', ['as' => 'ads_police_show', 'uses' => 'AdsPoliceController@show']);
Route::get('ads_police/show/{id}', ['as' => 'ads_police_show_id', 'uses' => 'AdsPoliceController@showID']);
Route::get('ads_police/download', ['as' => 'ads_police_get_attachment', 'uses' => 'AdsPoliceController@getAttachment']);

Route::get('ad/test', ['as' => 'ads_police', 'uses' => 'API\CEOsController@test']);

// Advisor 
Route::get('advisor', ['as' => 'advisor_index', 'uses' => 'AdvisorController@index']);
Route::get('advisor/activate_rate', ['as' => 'advisor_test', 'uses' => 'AdvisorController@activateRate']);
Route::get('advisor/import', ['as' => 'advisor_import', 'uses' => 'AdvisorController@import']);
Route::post('advisor/storeByImport', ['as' => 'advisor_store_by_import', 'uses' => 'AdvisorController@storeByImport']);

Route::get('advisor/test', ['as' => 'advisor_test', 'uses' => 'AdvisorController@test']);